!/ ------------------------------------------------------------------- /
      MODULE W3TRIAMD
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |       F. Ardhuin and A. Roland    |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          26-Jan-2014|
!/                  +-----------------------------------+
!/
!/    15-Mar-2007 : Origination.                        ( version 3.13 )
!/    25-Aug-2011 : Modification of boundary treatment  ( version 4.04 )
!/    30-Aug-2012 : Automatic detection of open BC      ( version 4.08 )
!/    02-Sep-2012 : Clean up of open BC for UG grids    ( version 4.08 )
!/    14-Oct-2013 : Correction  of latitude factor      ( version 4.12 )
!/    26-Jan-2014 : Correction  interpolation weights   ( version 4.18 )
!/    21-Apr-2016 : New algorithm to detect boundary    ( version 5.12 ) 
!/
!
!  1. Purpose :
!
!      Reads triangle and unstructured grid information
!
!  2. Method :
!
!     Look for namelist with name NAME in unit NDS and read if found.
!
!  3. Parameters :
!
!
!  4. Subroutines used :
!
!      Name               Type  Module   Description
!     ------------------------------------------------------------------------------------
!      READTRI            Subr. Internal Read unstructured grid data from .grd .tri formatted files.
!      READMSH            Subr.   Id.    Read unstructured grid data from MSH format
!      COUNT              Subr. Internal Count connection.
!      SPATIAL_GRID       Subr.   Id.    Calculate surfaces.
!      NVECTRI            Subr.   Id.    Define cell normals and angles and edge length
!      COORDMAX           Subr.   Id.    Calculate  useful grid elements
!      AREA_SI            Subr.   Id.    Define Connections
!     ------------------------------------------------------------------------------------
!
!
!  5. Called by :
!
!     Program in which it is contained.
!
!  6. Error messages :
!
!  7. Remarks :
!     The only point index which is needed is IX and NX stands for the total number of grid point.
!     IY and NY are not needed anymore, they are set to 1 in the unstructured case
!     Some noticeable arrays are:
!                     XYB    : give the 2D coordinates of all grid points
!                     TRIGP  : give the vertices of each triangle
!  8. Structure :
!
!  9. Switches :
!       !/PR3   : Enables unstructured meshes (temporary, will be replace by Unstructured switch)
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
PUBLIC

!C
        integer :: node_num
        integer :: dim_num
        integer :: triangle_order
        integer :: triangle_num
        integer :: bound_edge_num
        integer :: bound_num
!C        
        logical,save, allocatable :: edge_boundary(:)
        logical,save, allocatable :: node_boundary(:)
        integer,save, allocatable :: edge_nums(:)
        integer,save, allocatable :: boundary_node_index(:)
!C        
        integer,save, allocatable :: triangle_node(:,:)
        integer,save, allocatable :: edge(:,:)
        integer,save, allocatable :: edge_index(:,:)
        integer,save, allocatable :: iobp_aux(:)

        INTEGER, SAVE                      :: N_OUTSIDE_BOUNDARY
        INTEGER, SAVE, ALLOCATABLE         :: OUTSIDE_BOUNDARY(:)
        real (kind = 8), save, allocatable :: node_xy(:,:)
        real (kind = 8), save, allocatable :: edge_angle(:,:)

CONTAINS

!/ -------------------------------------------------------------------/
      SUBROUTINE READMSH(NDS,FNAME) 
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           F. Ardhuin              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          06-Jun-2018|
!/                  +-----------------------------------+
!/
!/    15-Feb-2008 : Origination.                        ( version 3.13 )
!/    25-Aug-2011 : Change of method for IOBPD          ( version 4.04 )
!/    06-Jun-2018 : Add DEBUGINIT/PDLIB/DEBUGSTP/DEBUGSETIOBP   
!/                                                      ( version 6.04 )
!/
!
!  1. Purpose :
!
!      Reads triangle and unstructured grid information from GMSH files
!      Calls the subroutines needed to compute grid connectivity
!
!  2. Method :
!
!     Look for namelist with name NAME in unit NDS and read if found.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       NDS     Int.   I   Data set number used for search.
!       NAME    C*4    I   Name of namelist.
!       STATUS  C*20   O   Status at end of routine,
!                            '(default values)  ' if no namelist found.
!                            '(user def. values)' if namelist read.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!      Name               Type  Module   Description
!     ------------------------------------------------------------------------------------
!      NEXTLN             Subr.
!      COUNT              Subr. Internal Count connection.
!      SPATIAL_GRID       Subr.   Id.    Calculate surfaces.
!      NVECTRI            Subr.   Id.    Define cell normals and angles and edge length
!      COORDMAX           Subr.   Id.    Calculate  useful grid elements
!      AREA_SI            Subr.   Id.    Define Connections
!     ----------------------------------------------------------------
!
!
!
!  5. Called by :
!      Name      Type  Module   Description
!     ----------------------------------------------------------------
!      W3GRID    Prog.          Model configuration program
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!     The only point index which is needed is IX and NX stands for the total number of grid point.
!     IY and NY are not needed anymore, they are set to 1 in the unstructured case
!     Some noticeable arrays are:
!                     XYB    : give the 2D coordinates of all grid points
!                     TRIGP  : give the vertices of each triangle
!     GMSH file gives too much information that is not necessarily required so data processing is needed (data sort and nesting).
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /

      USE W3GDATMD
      USE W3SERVMD, ONLY: NEXTLN, EXTCDE
!
      IMPLICIT NONE
!/
!/ Parameter list
!/
      INTEGER, INTENT(IN)                :: NDS
      CHARACTER(60), INTENT(IN)          :: FNAME
!/
!/ local parameters 
!/      
      INTEGER                            :: i,j,k, NODES, NELTS, ID, KID
      INTEGER                            :: ID1, ID2, KID1, ITMP(3)
      INTEGER                            :: I1, I2, I3
      INTEGER(KIND=4)                    :: Ind,eltype,ntag, INode
      CHARACTER                          :: COMSTR*1, SPACE*1 = ' ', CELS*64
      REAL, ALLOCATABLE                  :: TAGS(:)
      CHARACTER(LEN=64), ALLOCATABLE     :: ELS(:)
      CHARACTER(LEN=120)                 :: LINE
      CHARACTER(LEN=50)                  :: CHTMP
      CHARACTER(LEN=10)                  :: A, B, C
      INTEGER,ALLOCATABLE                :: NELS(:), TRIGPTMP1(:,:), TRIGPTMP2(:,:)
      INTEGER(KIND=4),ALLOCATABLE        :: IFOUND(:), VERTEX(:), BOUNDTMP(:)
      DOUBLE PRECISION, ALLOCATABLE      :: XYBTMP1(:,:),XYBTMP2(:,:)
      REAL                               :: z
      INTEGER                            :: NDSO,NDSE,NDST
      LOGICAL                            :: LPDLIB
!/DEBUGINIT     WRITE(740+IAPROC,*) 'Beginning of READMSH routine'
!/DEBUGINIT     FLUSH(740+IAPROC)

! IO setup

      NDSO = 10
      NDSE = 6
      NDST = 6

!

      OPEN(NDS,FILE = FNAME,STATUS='old')
      READ (NDS,'(A)') COMSTR
      IF (COMSTR.EQ.' ') COMSTR = '$'
      CALL NEXTLN(COMSTR, NDS, NDSE)
      READ(NDS,*) i,j,k
      CALL NEXTLN(COMSTR, NDS, NDSE)
      LPDLIB = .FALSE.
!/PDLIB LPDLIB = .TRUE.
!
! read number of nodes and nodes from Gmsh files
!
      READ(NDS,*) NODES
      ALLOCATE(XYBTMP1(NODES,3))
      DO I= 1, NODES
        READ(NDS,*) j, XYBTMP1(I,1), XYBTMP1(I,2), XYBTMP1(I,3)
        END DO
!
! read number of elements and elements from Gmsh files
!
      ALLOCATE(BOUNDTMP(NODES))
      N_OUTSIDE_BOUNDARY = 0
      CALL NEXTLN(COMSTR, NDS, NDSE)
      READ(NDS,*) NELTS
      ALLOCATE(TRIGPTMP1(NELTS, 3))
      J = 0 
      DO I= 1, NELTS
        READ(NDS,'(A100)') LINE
        READ(LINE,*) Ind,eltype,ntag
        ALLOCATE(TAGS(ntag))
        SELECT CASE (eltype) 
!
! eltype = 15 : boundary points  (this is used to make the difference 
!                                between the outside polygon and islands)
!
        CASE(15)
          READ(LINE,*) Ind,eltype,ntag,TAGS,INODE
          N_OUTSIDE_BOUNDARY = N_OUTSIDE_BOUNDARY +1
          BOUNDTMP(N_OUTSIDE_BOUNDARY)=INODE
!
! eltype = 2 : triangles
!
        CASE (2)
          J = J + 1
          READ(LINE,*)  Ind,eltype,ntag,tags,ITMP
          TRIGPTMP1(J,1:3) = ITMP
          END SELECT

        DEALLOCATE(TAGS)
        END DO
!
! organizes the grid data structure
!
      ALLOCATE(OUTSIDE_BOUNDARY(N_OUTSIDE_BOUNDARY))
      OUTSIDE_BOUNDARY(:)=BOUNDTMP(1:N_OUTSIDE_BOUNDARY)
      NTRI = J 
  
      ALLOCATE(IFOUND(NODES))
 
      IFOUND = 0
!
! Verifies that the nodes are used in at least one triangle
! 
      DO K = 1, NTRI
        I1 = TRIGPTMP1(K,1)
        I2 = TRIGPTMP1(K,2)
        I3 = TRIGPTMP1(K,3)
    
        IFOUND(I1)= IFOUND(I1) + 1
        IFOUND(I2)= IFOUND(I2) + 1
        IFOUND(I3)= IFOUND(I3) + 1
        END DO
 
      J = 0
 
      ALLOCATE(TRIGPTMP2(NTRI,3),VERTEX(NODES),XYBTMP2(NODES,3)) 
      VERTEX(:)=0
      XYBTMP2 = 0

      DO I = 1, NODES
        IF( IFOUND(I) .GT. 0) THEN
          J = J+1
          XYBTMP2(J,:) = XYBTMP1(I,:) 
          VERTEX(I) = J
          END IF
        END DO
!
! Number of nodes after clean up 
! 
      NX = J  
!
      DO I = 1, NTRI
        I1 = TRIGPTMP1(I,1)
        I2 = TRIGPTMP1(I,2)
        I3 = TRIGPTMP1(I,3)
        TRIGPTMP2(I,1) = VERTEX(I1)
        TRIGPTMP2(I,2) = VERTEX(I2)
        TRIGPTMP2(I,3) = VERTEX(I3)
        END DO   
!
      DEALLOCATE(XYBTMP1, IFOUND,TRIGPTMP1)
      DEALLOCATE(VERTEX)
!
!count points connections to allocate array in W3DIMUG 
!
      CALL COUNT(TRIGPTMP2)
!/DEBUGINIT     WRITE(*,*) 'Call W3DIMUG from READMSH'
!/DEBUGINIT     WRITE(740+IAPROC,*) 'Call W3DIMUG from READMSH'
!/DEBUGINIT     FLUSH(740+IAPROC)
      CALL W3DIMUG ( 1, NTRI, NX, COUNTOT, NNZ, NDSE, NDST ) 

!
! fills arrays
!
      DO I = 1, NX
        XYB(I,1) = XYBTMP2(I,1) 
        XYB(I,2) = XYBTMP2(I,2) 
        XYB(I,3) = XYBTMP2(I,3)
        END DO
!
!/DEBUGSTP    WRITE(740,*) 'Writing XYB(:,3)'
!/DEBUGSTP    DO I=1,NX
!/DEBUGSTP      WRITE(740,*) 'I,XYB(I,3)=', I, XYB(I,3)
!/DEBUGSTP    END DO
!/DEBUGSTP    FLUSH(740)
!
      DO I=1, NTRI
        ITMP = TRIGPTMP2(I,:)
        TRIGP(I,:) = ITMP 
        END DO   
!   
      DEALLOCATE(TRIGPTMP2,XYBTMP2)   
!
! call the various routines which define the point spotting strategy
!
      CALL SPATIAL_GRID    
      CALL NVECTRI
      CALL COORDMAX
!
!READMSH is only called by ww3_grid, thus the grid index is always 1. 
!
      CALL AREA_SI
!
      CLOSE(NDS)
      END SUBROUTINE READMSH
!/----------------------------------------------------------------------
      SUBROUTINE SPATIAL_GRID
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |      A. Roland  (BGS IT&E GbmH)   |
!/                  |      F. Ardhuin (IFREMER)         | 
!/                  |                        FORTRAN 90 |
!/                  | Last update :          31-Aug-2011|
!/                  +-----------------------------------+
!/
!/    15-May-2007 : Origination: adjustment from the WWM code       ( version 3.13 )
!/    31-Aug-2011 : Simplfies the cross products                    ( version 4.05 )
!/
!
!  1. Purpose :
!
!      Calculates triangle areas and reorders the triangles to have them 
!      oriented counterclockwise 
!
!  2. Method :
!
!     The triangle surface calculation is based on cross product.
!
!  3. Parameters :
!
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      READTRI    Subr. Internal  Unstructured mesh definition.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!     
!     This part of code is adapted from the WWM wave model develop at the Darmstadt University
!     (Aaron Roland)
!
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /   
         USE W3GDATMD  
!/S      USE W3SERVMD, ONLY: STRACE
         IMPLICIT NONE
!	 
!local parameters	 
! 
         REAL              :: TL1, TL2, TL3, TMPTRIGP
         INTEGER           :: I1, I2, I3
         INTEGER           :: K
!/S        INTEGER                      ::  IENT = 0          
!/ ------------------------------------------------------------------- /   
!/S      CALL STRACE (IENT, 'SPATIAL_GRID')   

                DO K = 1, NTRI
               
                   I1 = TRIGP(K,1)
                   I2 = TRIGP(K,2)
                   I3 = TRIGP(K,3)
!
! cross product of edge-vector  (orientated anticlockwise)
!                                   
                   TRIA(K) = REAL( (XYB(I2,2)-XYB(I1,2))      &     !  (Y2-Y1)
                                  *(XYB(I1,1)-XYB(I3,1))      &     ! *(X1-X3)
                                  +(XYB(I3,2)-XYB(I1,2))      &     !  (Y3-Y1)*(X2-X1)
                                  *(XYB(I2,1)-XYB(I1,1))      )*0.5
!
! test on negative triangle area, which means that the orientiation is not as assumed to be anticw. 
! therefore we swap the nodes !!! 
!      
                  IF (TRIA(K) .lt. TINY(1.)) THEN
         TMPTRIGP = TRIGP(K,2)
         TRIGP(K,2) = TRIGP(K,3)
         TRIGP(K,3) = TMPTRIGP
         I2 = TRIGP(K,2)
         I3 = TRIGP(K,3)
         TRIA(K) = -1.d0*TRIA(K)
         STOP 'WRONG TRIANGLE' 
         END IF    
       END DO
     END SUBROUTINE
!/--------------------------------------------------------------------/
!
!/----------------------------------------------------------------------------  
    SUBROUTINE NVECTRI
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           A. Roland               |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          15-May-2008|
!/                  +-----------------------------------+
!/
!/    15-May-2007 : Origination: adjustment from the WWM code       ( version 3.13 )
!/
!
!  1. Purpose :
!
!      Calculate cell tools: inward normal, angles and length of edges.
!
!  2. Method :
!      To get inward pointing normals, triangle are glanced through anti-clockwisely
!     
!
!  3. Parameters :
!
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      READTRI    Subr. Internal  Unstructured mesh definition.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!     
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /       
    USE W3GDATMD
!/S      USE W3SERVMD, ONLY: STRACE

         IMPLICIT NONE
!
!local parameter 
!	 
         INTEGER :: IP, IE
         INTEGER :: I1, I2, I3, I11, I22, I33
         REAL*8    :: P1(2), P2(2), P3(2)
         REAL*8    :: R1(2), R2(2), R3(2)
         REAL*8    :: N1(2), N2(2), N3(2)
	 REAL*8    :: TMP(3)
	 REAL*8    :: TMPINV(3)
!/S        INTEGER                      ::  IENT = 0 	 
!/ ------------------------------------------------------------------- /   
!/S      CALL STRACE (IENT, 'NVECTRI')   

    
         DO IE = 1, NTRI
!
! vertices
!
            I1 = TRIGP(IE,1)
            I2 = TRIGP(IE,2)
            I3 = TRIGP(IE,3)
                           
            P1(1) = XYB(I1,1)
            P1(2) = XYB(I1,2)
            P2(1) = XYB(I2,1)
            P2(2) = XYB(I2,2)
            P3(1) = XYB(I3,1)
            P3(2) = XYB(I3,2)
!
! I1 -> I2, I2 -> I3, I3 -> I1 (anticlockwise orientation is preserved)
!                    
            R1 = P3-P2
            R2 = P1-P3
            R3 = P2-P1
        
            N1(1) = (-R1(2))
            N1(2) = ( R1(1))
            N2(1) = (-R2(2))
            N2(2) = ( R2(1))
            N3(1) = (-R3(2))
            N3(2) = ( R3(1))  
!
! edges length
!            
            LLEN(IE,1) = DSQRT(R1(1)**2+R1(2)**2)
            LLEN(IE,2) = DSQRT(R2(1)**2+R2(2)**2)
            LLEN(IE,3) = DSQRT(R3(1)**2+R3(2)**2)   
!
! inward normal used for propagation (not normalized)
!            
            IEN(IE,1) = N1(1)
            IEN(IE,2) = N1(2)
            IEN(IE,3) = N2(1)
            IEN(IE,4) = N2(2)
            IEN(IE,5) = N3(1)
            IEN(IE,6) = N3(2)
    
         END DO
 
     END SUBROUTINE
!/------------------------------------------------------------------------

      SUBROUTINE COUNT(TRIGPTEMP)      
     
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           A. Roland               | 
!/                  |           F. Ardhuin              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          15-May-2008|
!/                  +-----------------------------------+
!/
!/    15-May-2007 : Origination.                        ( version 3.13 )
!/
!      
!  1. Purpose :
!
!      Calculate global and maximum number of connection for array allocations .
!
!  2. Method :
!     
!  3. Parameters :
!     Parameter list
!     ----------------------------------------------------------------
!       NTRI         Int.   I   Total number of triangle.
!       TRIGPTEMP    Int    I   Temporary array of triangle vertices
!       COUNTRI      Int    O   Maximum number of connected triangle 
!                               for a given points
!       COUNTOT      Int    O   Global number of triangle connection 
!                               for the whole grid.   	
!     ----------------------------------------------------------------
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      READTRI    Subr. Internal  Unstructured mesh definition.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!     
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /  
        USE W3GDATMD
!/S      USE W3SERVMD, ONLY: STRACE
        IMPLICIT NONE

     
!/ parameter list

   INTEGER,INTENT(IN) :: TRIGPTEMP(:,:)
!/ ------------------------------------------------------------------- /   
!/ local parameter

   INTEGER               :: CONN(NX)
   INTEGER               :: COUNTER, IP, IE, I, J, N(3)
!/S        INTEGER                      ::  IENT = 0    
!/------------------------------------------------------------------------

!/S      CALL STRACE (IENT, 'COUNT')

COUNTRI=0
COUNTOT=0
CONN(:)= 0

!
!calculate the number of connected triangles for a given point.
!

DO IE = 1,NTRI
   N(:) = 0.
   N(1) = TRIGPTEMP(IE,1)
   N(2) = TRIGPTEMP(IE,2)
   N(3) = TRIGPTEMP(IE,3)
   CONN(N(1)) = CONN(N(1)) + 1
   CONN(N(2)) = CONN(N(2)) + 1
   CONN(N(3)) = CONN(N(3)) + 1
ENDDO
 
 COUNTRI = MAXVAL(CONN)
! 
! calculate the global number of connections available through the mesh
!
J=0
 DO  IP=1,NX
   DO I=1,CONN(IP)
      J=J+1
   ENDDO
 ENDDO
 COUNTOT=J  

END SUBROUTINE

!/----------------------------------------------------------------------------  
      SUBROUTINE COORDMAX
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           F. Ardhuin              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          15-May-2008|
!/                  +-----------------------------------+
!/
!/    15-May-2007 : Origination.                        ( version 3.13 )
!/
!  1. Purpose :
!
!      Calculate first point and last point coordinates, and minimum and maximum edge length.
!
!  2. Method :
!     
!  3. Parameters :
!
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      READTRI    Subr. Internal  Unstructured mesh definition.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!     
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- / 
          USE W3GDATMD
!/S      USE W3SERVMD, ONLY: STRACE
          IMPLICIT NONE
!/S        INTEGER                      ::  IENT = 0  
            
  
!/S      CALL STRACE (IENT, 'COORDMAX') 
!     
! maximum of coordinates s
!
    MAXX = MAXVAL(XYB(:,1))
    MAXY = MAXVAL(XYB(:,2))
! 
! minimum of coordinates 
!
    X0 = MINVAL(XYB(:,1))
    Y0 = MINVAL(XYB(:,2))
! 
!maximum and minimum length of edges
!
    DXYMAX = MAXVAL(LLEN(:,:))
    SX = MINVAL(LLEN(:,:))
    SY = SX
! 
 END SUBROUTINE
!-------------------------------------------------------------------------
  SUBROUTINE AREA_SI
!/ ------------------------------------------------------------------- 
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           A. Roland               |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          23-Aug-2011|
!/                  +-----------------------------------+
!/
!/    15-May-2007 : Origination: adjustment from the WWM code       ( version 3.13 )
!/    23-Aug-2011 : Removes double entries in VNEIGH                ( version 4.04 )
!/  
!
!  1. Purpose :
!
!      Define optimized connection arrays (points and triangles) for spatial propagation schemes.
!
!  2. Method :
!     
!  3. Parameters :
!
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      READTRI    Subr. Internal  Unstructured mesh definition.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!      
!     The storage is optimize especially considering the iterative solver used.
!     The schemes used are vertex-centered, a point has to be considered within its
!     median dual cell. For a given point, the surface of the dual cell is one third
!     of the sum of the surface of connected triangles. 
!     This routine is from WWM developped in Darmstadt(Aaron Roland) 
!     
!  8. Structure :
!
!  9. Switches :
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /      

        USE W3GDATMD
!/S      USE W3SERVMD, ONLY: STRACE
         IMPLICIT NONE
!/ input 

         !INTEGER, INTENT(INOUT) :: NNZ
         !INTEGER, INTENT(INOUT) :: JAA(:),IAA(:),POSI(:)

!/ local parameters   

         INTEGER :: COUNTER,ifound,alreadyfound
         INTEGER :: I, J, K, II
         INTEGER :: IP, IE, POS, POS_I, POS_J, POS_K, IP_I, IP_J, IP_K
         INTEGER :: I1, I2, I3, IP2, CHILF(NX)
         INTEGER :: TMP(NX), CELLVERTEX(NX,COUNTRI,2)
         INTEGER :: COUNT_MAX
         DOUBLE PRECISION   :: TRIA03
         INTEGER, ALLOCATABLE :: PTABLE(:,:)

!/S        INTEGER                      ::  IENT = 0    
!/ ------------------------------------------------------------------- /      

!/S      CALL STRACE (IENT, 'AREA_SI')

         WRITE(*,'("+TRACE......",A)') 'COMPUTE SI, TRIA und CCON'

         SI(:) = 0.D0
!
         CCON(:) = 0     ! Number of connected Elements
         DO IE = 1 , NTRI 
           I1 = TRIGP(IE,1)
           I2 = TRIGP(IE,2)
           I3 = TRIGP(IE,3)
           CCON(I1) = CCON(I1) + 1
           CCON(I2) = CCON(I2) + 1
           CCON(I3) = CCON(I3) + 1
           TRIA03 = 1./3. * TRIA(IE)
           SI(I1) = SI(I1) + TRIA03
           SI(I2) = SI(I2) + TRIA03
           SI(I3) = SI(I3) + TRIA03
           ENDDO

         CELLVERTEX(:,:,:) = 0 ! Stores for each node the Elementnumbers of the connected Elements
                               ! and the Position of the position of the Node in the Element Index

         WRITE(*,'("+TRACE......",A)') 'COMPUTE CELLVERTEX'

         CHILF             = 0

         DO IE = 1, NTRI 
           DO J=1,3
             I = TRIGP(IE,J)!INE(J,IE)
             CHILF(I) = CHILF(I)+1
             CELLVERTEX(I,CHILF(I),1) = IE
             CELLVERTEX(I,CHILF(I),2) = J
           END DO
         ENDDO

         WRITE(*,'("+TRACE......",A)') 'COMPUTE IE_CELL and POS_CELL'
!
! Second step in storage, the initial 3D array CELLVERTEX, is transformed in a 1D array
! the global index is J . From now, all the computation step based on these arrays must
! abide by the conservation of the 2 loop algorithm (points + connected triangles)
! AR: I will change this now to pointers in order to omit fix loop structure for the LTS stuff ...
!	   
         INDEX_CELL(1)=1
         J = 0
         DO IP = 1, NX
           DO I = 1, CCON(IP)
             J = J + 1
             IE_CELL(J)  = CELLVERTEX(IP,I,1)
             POS_CELL(J) = CELLVERTEX(IP,I,2) 
           END DO
           INDEX_CELL(IP+1)=J+1
         END DO

         IF (.NOT. FSNIMP) RETURN

         J = 0
         DO IP = 1, NX 
           DO I = 1, CCON(IP)
             J = J + 1
           END DO
         END DO

         COUNT_MAX = J

           ALLOCATE(PTABLE(COUNT_MAX,7))

           J = 0
           PTABLE(:,:) = 0.
           DO IP = 1, NX 
             DO I = 1, CCON(IP)
               J = J + 1
               IE    = IE_CELL(J)
               POS   = POS_CELL(J)
               I1 = TRIGP(IE,1)
               I2 = TRIGP(IE,2)
               I3 = TRIGP(IE,3)
               IF (POS == 1) THEN
                 POS_J = 2
                 POS_K = 3
               ELSE IF (POS == 2) THEN
                 POS_J = 3
                 POS_K = 1
               ELSE
                 POS_J = 1
                 POS_K = 2
               END IF
               IP_I = IP
               IP_J = TRIGP(IE,POS_J)
               IP_K = TRIGP(IE,POS_K)
               PTABLE(J,1) = IP_I
               PTABLE(J,2) = IP_J
               PTABLE(J,3) = IP_K
               PTABLE(J,4) = POS
               PTABLE(J,5) = POS_J
               PTABLE(J,6) = POS_K
               PTABLE(J,7) = IE
             END DO
           END DO

!           WRITE(*,'("+TRACE......",A)') 'SET UP SPARSE MATRIX POINTER ... COUNT NONZERO ENTRY'

           J = 0
           K = 0
           DO IP = 1, NX 
             TMP(:) = 0
             DO I = 1, CCON(IP)
               J = J + 1
               IP_J  = PTABLE(J,2)
               IP_K  = PTABLE(J,3)
               POS   = PTABLE(J,4)
               TMP(IP)   = 1
               TMP(IP_J) = 1
               TMP(IP_K) = 1
            END DO
            K = K + SUM(TMP)
          END DO

          NNZ = K

!          WRITE(*,'("+TRACE......",A)') 'SET UP SPARSE MATRIX POINTER ... SETUP POINTER'

           J = 0
           K = 0
           IAA(1) = 1
           JAA    = 0
           DO IP = 1, NX ! Run through all rows 
             TMP(:)=0
             DO I = 1, CCON(IP)         ! Check how many entries there are ...
               J = J + 1                ! this is the same J index as in IE_CELL
               IP_J  = PTABLE(J,2)
               IP_K  = PTABLE(J,3)
               TMP(IP)   = 1
               TMP(IP_J) = 1
               TMP(IP_K) = 1
             END DO
             DO I = 1, NX               ! Run through all columns 
               IF (TMP(I) .GT. 0) THEN  ! this is true only for the connected points
                 K = K + 1              
                 JAA(K) = I
               END IF
             END DO
             IAA(IP + 1) = K + 1    
           END DO

           POSI = 0
           J = 0
           DO IP = 1, NX  
             DO I = 1, CCON(IP)
               J = J + 1
               IP_J  = PTABLE(J,2)
               IP_K  = PTABLE(J,3)
               DO K = IAA(IP), IAA(IP+1) - 1
                 IF (IP   == JAA(K)) POSI(1,J)  = K
                 IF (IP_J == JAA(K)) POSI(2,J)  = K
                 IF (IP_K == JAA(K)) POSI(3,J)  = K
                 IF (K == 0) THEN
                  WRITE(*,*) 'ERROR IN AREA_SI K .EQ. 0'
                  STOP
                 END IF
               END DO
            END DO
          END DO

          DEALLOCATE(PTABLE)

       END SUBROUTINE
!/ ------------------------------------------------------------------- /
     SUBROUTINE IS_IN_UNGRID(TRIGP, XYB, NX, NTRI, XTIN, YTIN, ITOUT, IS, JS, RW)

!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |      Mathieu Dutour Sikiric, IRB  |
!/                  |                 Aron Roland, Z&P  |
!/                  |             Fabrice Ardhuin       |
!/                  |                        FORTRAN 90 |
!/                  | Last update :          26-Jan-2014|
!/                  +-----------------------------------+
!/
!/ Adapted from other subroutine 
!/    15-Oct-2007 : Origination.                        ( version 3.13 )
!/    21-Sep-2012 : Uses same interpolation as regular  ( version 4.08 )
!/    26-Jan-2014 : Correcting bug in RW                ( version 4.18 )
!/
!  1. Purpose :
!
!      Determine whether a point is inside or outside an unstructured grid, 
!      and returns index of triangle and interpolation weights 
!      This is the analogue for triangles of the FUNCTION W3GRMP
!
!  2. Method :
!
!     Using barycentric coordinates defined as the ratio of triangle algebric areas 
!     which are positive or negative. 
!     Computes the 3 interpolation weights for each triangle until they are all positive
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       NTRI    Int.   I   Number of triangles.
!       XTIN    Real   I   X-coordinate of target point.
!       YTIN    Real   I   Y-coordinate of target point.
!       IS,JS   I.A.   O   (I,J) indices of vertices of enclosing grid cell.
!       RW      R.A.   O   Array of interpolation weights.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!     None
!
!  5. Called by :
!
!     WMGLOW, W3IOPP, WMIOPP, WW3_GINT
!
!  6. Error messages :
!
!     - Error checks on previous setting of variable.
!
!  7. Remarks :
!
!  8. Structure :
!
!  9. Switches :
!
!     !/S    Enable subroutine tracing.
!     !/T    Enable test output
!
! 10. Source code :
!


!  2. Method :
!
!     Using barycentric coordinates. Each coefficient depends on the mass of its related point in the interpolation.
!
!  3. Parameters :
!
!  4. Subroutines used :
!
!  5. Called by :
!
!       Name      Type  Module   Description
!     ----------------------------------------------------------------
!      W3IOPP    Subr. Internal  Preprocessing of point output.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!  7. Remarks :
!
!      This subroutine is adjusted from CREST code (Fabrice Ardhuin)
!      For a given output point, the algorithm enable to glance through all the triangles 
!      to find the one the point belong to, and then make interpolation.
!
!  8. Structure :
!
!  9. Switches :
!
!       !/LLG   Spherical grid.
!       !/XYG   Carthesian grid.
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
!      USE W3GDATMD
      USE W3SERVMD, ONLY: EXTCDE
!/S      USE W3SERVMD, ONLY: STRACE
      IMPLICIT NONE

!/ ------------------------------------------------------------------- /
! Parameter list

     INTEGER, INTENT(IN)            :: NTRI, NX
     INTEGER, INTENT(IN)            :: TRIGP(:,:)
     DOUBLE PRECISION, INTENT(IN)   :: XYB(:,:)
     REAL   , INTENT(IN)            :: XTIN, YTIN
     INTEGER, INTENT(OUT)           :: ITOUT
     INTEGER, INTENT(OUT)           :: IS(4), JS(4)
     REAL, INTENT(OUT)              :: RW(4)
!/ ------------------------------------------------------------------- /
!local parameters

     DOUBLE PRECISION             :: x1, x2, x3
     DOUBLE PRECISION             :: y1, y2, y3
     DOUBLE PRECISION             :: s1, s2, s3, sg1, sg2, sg3
     INTEGER                      :: ITRI
     INTEGER                      :: I1, I2, I3
     INTEGER                      :: nbFound
     INTEGER                      :: NDSO, NDSE
!/S     INTEGER                      ::  IENT = 0
!/S     CALL STRACE (IENT, 'IS_IN_UNGRID')


! IO setup

NDSO = 10
NDSE = 6

!
     itout = 0
     nbFound=0
     ITRI = 0 
     DO WHILE (nbFound.EQ.0.AND.ITRI.LT.NTRI)
       ITRI = ITRI +1 
       I1=TRIGP(ITRI,1)
       I2=TRIGP(ITRI,2)
       I3=TRIGP(ITRI,3)
! coordinates of the first vertex A
       x1=XYB(I1,1)
       y1=XYB(I1,2)
! coordinates of the 2nd vertex B
       x2=XYB(I2,1)
       y2=XYB(I2,2)
!coordinates of the 3rd vertex C
       x3=XYB(I3,1)
       y3=XYB(I3,2)
!with M = (XTIN,YTIN) the target point ... 
!vector product of AB and AC
       sg3=(y3-y1)*(x2-x1)-(x3-x1)*(y2-y1)
!vector product of AB and AM
       s3=(YTIN-y1)*(x2-x1)-(XTIN-x1)*(y2-y1)
!vector product of BC and BA
       sg1=(y1-y2)*(x3-x2)-(x1-x2)*(y3-y2)
!vector product of BC and BM
       s1=(YTIN-y2)*(x3-x2)-(XTIN-x2)*(y3-y2)
!vector product of CA and CB
       sg2=(y2-y3)*(x1-x3)-(x2-x3)*(y1-y3)
!vector product of CA and CM
       s2=(YTIN-y3)*(x1-x3)-(XTIN-x3)*(y1-y3)
       IF ((s1*sg1.GE.0).AND.(s2*sg2.GE.0).AND.(s3*sg3.GE.0)) THEN
         itout=ITRI
         nbFound=nbFound+1
         IS(1)=I1
         IS(2)=I2
         IS(3)=I3
         IS(4)=1
         JS(:)=1
         RW(1)=s1/sg1
         RW(2)=s2/sg2
         RW(3)=1.-RW(1)-RW(2)  !s3/sg3
         RW(4)=0.
       END IF
     ENDDO
     END SUBROUTINE IS_IN_UNGRID




END MODULE W3TRIAMD

