!/ ------------------------------------------------------------------- /
      MODULE W3NMLSTATMD
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         14-Dec-2018 |
!/                  +-----------------------------------+
!/
!/    For updates see subroutines.
!/
!  1. Purpose :
!
!     Manages namelists from configuration file ww3_stat.nml for ww3_stat program
!

!/ ------------------------------------------------------------------- /

  ! module defaults
  IMPLICIT NONE

  PUBLIC

  ! match structure
  TYPE NML_STAT_T
    CHARACTER(1024)             :: TRKFILELIST
    CHARACTER(1024)             :: TRKCOORDLIST
    CHARACTER(1024)             :: OBSVARLIST
    CHARACTER(1024)             :: MODVARLIST
    LOGICAL                     :: TRKLON360

    CHARACTER(1024)             :: DIR_OUT
    CHARACTER(256)              :: OUTPREFIX
    INTEGER                     :: NCTYPE

    CHARACTER(15)               :: TIMESTART
    CHARACTER(15)               :: TIMESTOP

    DOUBLE PRECISION            :: LON_MIN
    DOUBLE PRECISION            :: LON_MAX
    DOUBLE PRECISION            :: LAT_MIN
    DOUBLE PRECISION            :: LAT_MAX

    DOUBLE PRECISION            :: DX
    DOUBLE PRECISION            :: DY
    INTEGER                     :: NAVGMIN
    CHARACTER(3)                :: NORM

  END TYPE NML_STAT_T


  ! miscellaneous
  CHARACTER(256)                :: MSG
  INTEGER                       :: NDSN




  CONTAINS
!/ ------------------------------------------------------------------- /


  SUBROUTINE W3NMLSTAT (NDSI, INFILE, NML_STAT, IERR)

!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         12-Dec-2018 |
!/                  +-----------------------------------+
!/

    IMPLICIT NONE

    INTEGER, INTENT(IN)                         :: NDSI
    CHARACTER*(*), INTENT(IN)                   :: INFILE
    TYPE(NML_STAT_T), INTENT(INOUT)            :: NML_STAT
    INTEGER, INTENT(OUT)                        :: IERR

    INTEGER                                     :: NDSE

    IERR = 0
    NDSE = 6

    ! open namelist log file
    NDSN = 3
    OPEN (NDSN, file=TRIM(INFILE)//'.log', form='formatted', iostat=IERR)
       IF (IERR.NE.0) THEN
      WRITE (NDSE,'(A)') 'ERROR: open full nml file '//TRIM(INFILE)//'.log failed'
      RETURN
    END IF

    ! open input file
    OPEN (NDSI, file=TRIM(INFILE), form='formatted', status='old', iostat=IERR)
    IF (IERR.NE.0) THEN
      WRITE (NDSE,'(A)') 'ERROR: open input file '//TRIM(INFILE)//' failed'
      RETURN
    END IF

    ! read match namelist
    CALL READ_STAT_NML (NDSI, NML_STAT)
    CALL REPORT_STAT_NML (NML_STAT)

    ! close namelist files
    CLOSE (NDSI)
    CLOSE (NDSN)

  END SUBROUTINE W3NMLSTAT

!/ ------------------------------------------------------------------- /


  SUBROUTINE READ_STAT_NML (NDSI, NML_STAT)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         12-Dec-2018 |
!/                  +-----------------------------------+
!/

    USE W3SERVMD, ONLY: EXTCDE


    IMPLICIT NONE

    INTEGER, INTENT(IN)                 :: NDSI
    TYPE(NML_STAT_T), INTENT(INOUT)    :: NML_STAT

    ! locals
    INTEGER                   :: IERR, NDSE
    TYPE(NML_STAT_T) :: STAT
    NAMELIST /STAT_NML/ STAT


    IERR = 0
    NDSE = 6

    ! set default values for match structure
    STAT%TRKFILELIST  = 'track.list'
    STAT%TRKCOORDLIST = 'lat lon'
    STAT%OBSVARLIST   = 'swh wind_speed'
    STAT%MODVARLIST   = 'hs wnd'
    STAT%TRKLON360    = .FALSE.

    STAT%DIR_OUT      = 'unset'
    STAT%OUTPREFIX    = 'alti_'
    STAT%NCTYPE       = 4

    STAT%TIMESTART    = '19680101 000000'
    STAT%TIMESTOP     = '19681231 000000'

    STAT%LON_MIN      = -180
    STAT%LON_MAX      =  179.5
    STAT%LAT_MIN      =  -90
    STAT%LAT_MAX      =   90

    STAT%DX           =   0.5
    STAT%DY           =   0.5
    STAT%NAVGMIN      =   6
    STAT%NORM         = 'OBS'
 

    ! read match namelist
    REWIND (NDSI)
    READ (NDSI, nml=STAT_NML, iostat=IERR, iomsg=MSG)
    IF (IERR.GT.0) THEN
      WRITE (NDSE,'(A,/A)') &
        'ERROR: READ_STAT_NML: namelist read error', &
        'ERROR: '//TRIM(MSG)
      CALL EXTCDE (1)
    END IF

    ! save namelist
    NML_STAT = STAT

  END SUBROUTINE READ_STAT_NML

!/ ------------------------------------------------------------------- /



!/ ------------------------------------------------------------------- /

  SUBROUTINE REPORT_STAT_NML (NML_STAT)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         12-Dec-2018 |
!/                  +-----------------------------------+
!/
    IMPLICIT NONE
    TYPE(NML_STAT_T), INTENT(IN) :: NML_STAT

      WRITE (MSG,'(A)') 'STAT % '
      WRITE (NDSN,'(A)')
      WRITE (NDSN,10) TRIM(MSG),'TRKFILELIST     = ', TRIM(NML_STAT%TRKFILELIST)
      WRITE (NDSN,10) TRIM(MSG),'TRKCOORDLIST      = ', TRIM(NML_STAT%TRKCOORDLIST)
      WRITE (NDSN,10) TRIM(MSG),'OBSVARLIST      = ', TRIM(NML_STAT%OBSVARLIST)
      WRITE (NDSN,10) TRIM(MSG),'MODVARLIST      = ', TRIM(NML_STAT%MODVARLIST)
      WRITE (NDSN,13) TRIM(MSG),'TRKLON360       = ', NML_STAT%TRKLON360

      WRITE (NDSN,10) TRIM(MSG),'DIR_OUT         = ', TRIM(NML_STAT%DIR_OUT)
      WRITE (NDSN,10) TRIM(MSG),'OUTPREFIX       = ', TRIM(NML_STAT%OUTPREFIX)
      WRITE (NDSN,11) TRIM(MSG),'NCTYPE          = ', NML_STAT%NCTYPE

      WRITE (NDSN,10) TRIM(MSG),'TIMESTART       = ', TRIM(NML_STAT%TIMESTART)
      WRITE (NDSN,10) TRIM(MSG),'TIMESTOP        = ', TRIM(NML_STAT%TIMESTOP)

      WRITE (NDSN,14) TRIM(MSG),'LON_MIN         = ', NML_STAT%LON_MIN
      WRITE (NDSN,14) TRIM(MSG),'LON_MAX         = ', NML_STAT%LON_MAX
      WRITE (NDSN,14) TRIM(MSG),'LAT_MIN         = ', NML_STAT%LAT_MIN
      WRITE (NDSN,14) TRIM(MSG),'LAT_MAX         = ', NML_STAT%LAT_MAX

      WRITE (NDSN,14) TRIM(MSG),'DX              = ', NML_STAT%DX
      WRITE (NDSN,14) TRIM(MSG),'DY              = ', NML_STAT%DY
      WRITE (NDSN,11) TRIM(MSG),'NAVGMIN         = ', NML_STAT%NAVGMIN
      WRITE (NDSN,10) TRIM(MSG),'NORM            = ', NML_STAT%NORM


10  FORMAT (A,2X,A,A)
11  FORMAT (A,2X,A,I8)
13  FORMAT (A,2X,A,L1)
14  FORMAT (A,2X,A,F8.2)

  END SUBROUTINE REPORT_STAT_NML

!/ ------------------------------------------------------------------- /





END MODULE W3NMLSTATMD

!/ ------------------------------------------------------------------- /

