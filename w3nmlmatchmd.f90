!/ ------------------------------------------------------------------- /
      MODULE W3NMLMATCHMD
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         02-Sep-2021 |
!/                  +-----------------------------------+
!/
!/    For updates see subroutines.
!/
!  1. Purpose :
!
!     Manages namelists from configuration file ww3_match.nml for ww3_match program
!

!/ ------------------------------------------------------------------- /

  ! module defaults
  IMPLICIT NONE

  PUBLIC

  ! match structure
  TYPE NML_MATCH_T
    CHARACTER(1024)             :: OBSFILELIST
    CHARACTER(1024)             :: OBSTIME
    CHARACTER(1024)             :: OBSCOORDLIST
    CHARACTER(1024)             :: OBSVARLIST
    CHARACTER(1024)             :: OBSFLAGLIST
    CHARACTER(1024)             :: OBSFLAGCOMP
    CHARACTER(1024)             :: OBSFLAGVALUE
    LOGICAL                     :: OBSSAMEFILE
    LOGICAL                     :: OBSLON360

    CHARACTER(1024)             :: MODFILELIST
    CHARACTER(1024)             :: MODTIME
    CHARACTER(1024)             :: MODVARLIST
    CHARACTER(1024)             :: MODFLAGLIST
    CHARACTER(1024)             :: MODFLAGCOMP
    CHARACTER(1024)             :: MODFLAGVALUE
    LOGICAL                     :: MODSAMEFILE
    LOGICAL                     :: MODLON360

    CHARACTER(1024)             :: DIR_OUT
    CHARACTER(256)              :: OUTPREFIX
    CHARACTER(3)                :: OUTTIMEUNITS
    CHARACTER(3)                :: FILESPLIT
    INTEGER                     :: NCTYPE

    CHARACTER(15)               :: TIMESTART
    CHARACTER(15)               :: TIMESTOP
!    INTEGER                     :: TIMESPLIT
    CHARACTER(6)                :: INTERPSPACE
    LOGICAL                     :: INTERPTIME
    INTEGER                     :: MAXLANDPOINTS
    LOGICAL                     :: ALLTIMEINDEX

    CHARACTER(4)                :: CLOS
    REAL                        :: LON_MIN
    REAL                        :: LON_MAX
    REAL                        :: LAT_MIN
    REAL                        :: LAT_MAX

  END TYPE NML_MATCH_T


  ! miscellaneous
  CHARACTER(256)                :: MSG
  INTEGER                       :: NDSN




  CONTAINS
!/ ------------------------------------------------------------------- /


  SUBROUTINE W3NMLMATCH (NDSI, INFILE, NML_MATCH, IERR)

!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         12-Dec-2018 |
!/                  +-----------------------------------+
!/

    IMPLICIT NONE

    INTEGER, INTENT(IN)                         :: NDSI
    CHARACTER*(*), INTENT(IN)                   :: INFILE
    TYPE(NML_MATCH_T), INTENT(INOUT)            :: NML_MATCH
    INTEGER, INTENT(OUT)                        :: IERR

    INTEGER                                     :: NDSE

    IERR = 0
    NDSE = 6

    ! open namelist log file
    NDSN = 3
    OPEN (NDSN, file=TRIM(INFILE)//'.log', form='formatted', iostat=IERR)
       IF (IERR.NE.0) THEN
      WRITE (NDSE,'(A)') 'ERROR: open full nml file '//TRIM(INFILE)//'.log failed'
      RETURN
    END IF

    ! open input file
    OPEN (NDSI, file=TRIM(INFILE), form='formatted', status='old', iostat=IERR)
    IF (IERR.NE.0) THEN
      WRITE (NDSE,'(A)') 'ERROR: open input file '//TRIM(INFILE)//' failed'
      RETURN
    END IF

    ! read match namelist
    CALL READ_MATCH_NML (NDSI, NML_MATCH)
    CALL REPORT_MATCH_NML (NML_MATCH)

    ! close namelist files
    CLOSE (NDSI)
    CLOSE (NDSN)

  END SUBROUTINE W3NMLMATCH

!/ ------------------------------------------------------------------- /


  SUBROUTINE READ_MATCH_NML (NDSI, NML_MATCH)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                                   |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         02-Sep-2021 |
!/                  +-----------------------------------+
!/

    USE W3SERVMD, ONLY: EXTCDE


    IMPLICIT NONE

    INTEGER, INTENT(IN)                 :: NDSI
    TYPE(NML_MATCH_T), INTENT(INOUT)    :: NML_MATCH

    ! locals
    INTEGER                   :: IERR, NDSE
    TYPE(NML_MATCH_T) :: MATCH
    NAMELIST /MATCH_NML/ MATCH


    IERR = 0
    NDSE = 6

    ! set default values for match structure
    MATCH%OBSFILELIST  = 'obs.list'
    MATCH%OBSTIME      = 'time'
    MATCH%OBSCOORDLIST = 'lat lon'
    MATCH%OBSVARLIST   = 'swh wind_speed'
    MATCH%OBSFLAGLIST  = 'swh_quality'
    MATCH%OBSFLAGCOMP  = '.LT.'
    MATCH%OBSFLAGVALUE = '3'
    MATCH%OBSSAMEFILE  = .TRUE.
    MATCH%OBSLON360    = .FALSE.

    MATCH%MODFILELIST  = 'mod.list'
    MATCH%MODTIME      = 'time'
    MATCH%MODFLAGLIST  = 'dpt'
    MATCH%MODFLAGCOMP  = '.LT.'
    MATCH%MODFLAGVALUE = '0'
    MATCH%MODSAMEFILE  = .TRUE.
    MATCH%MODVARLIST   = 'hs uwnd vwnd'
    MATCH%MODLON360    = .FALSE.

    MATCH%DIR_OUT      = 'unset'
    MATCH%OUTPREFIX    = 'alti_'
    MATCH%OUTTIMEUNITS = 'MOD'
    MATCH%FILESPLIT    = 'NOT'
    MATCH%NCTYPE       = 4

    MATCH%TIMESTART    = '19680101 000000'
    MATCH%TIMESTOP     = '19681231 000000'
!    MATCH%TIMESPLIT    = 6
    MATCH%INTERPSPACE  = 'bilinr'
    MATCH%INTERPTIME   = .TRUE.
    MATCH%MAXLANDPOINTS = 3
    MATCH%ALLTIMEINDEX  = .FALSE.

    MATCH%CLOS         = 'SMPL'
    MATCH%LON_MIN      = -180
    MATCH%LON_MAX      =  180
    MATCH%LAT_MIN      =  -90
    MATCH%LAT_MAX      =   90
 

    ! read match namelist
    REWIND (NDSI)
    READ (NDSI, nml=MATCH_NML, iostat=IERR, iomsg=MSG)
    IF (IERR.GT.0) THEN
      WRITE (NDSE,'(A,/A)') &
        'ERROR: READ_MATCH_NML: namelist read error', &
        'ERROR: '//TRIM(MSG)
      CALL EXTCDE (1)
    END IF

    ! save namelist
    NML_MATCH = MATCH

  END SUBROUTINE READ_MATCH_NML

!/ ------------------------------------------------------------------- /



!/ ------------------------------------------------------------------- /

  SUBROUTINE REPORT_MATCH_NML (NML_MATCH)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           M. Accensi              |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         02-Sep-2021 |
!/                  +-----------------------------------+
!/
    IMPLICIT NONE
    TYPE(NML_MATCH_T), INTENT(IN) :: NML_MATCH

      WRITE (MSG,'(A)') 'MATCH % '
      WRITE (NDSN,'(A)')
      WRITE (NDSN,10) TRIM(MSG),'OBSFILELIST     = ', TRIM(NML_MATCH%OBSFILELIST)
      WRITE (NDSN,10) TRIM(MSG),'OBSTIME         = ', TRIM(NML_MATCH%OBSTIME)
      WRITE (NDSN,10) TRIM(MSG),'OBSCOORDLIST    = ', TRIM(NML_MATCH%OBSCOORDLIST)
      WRITE (NDSN,10) TRIM(MSG),'OBSVARLIST      = ', TRIM(NML_MATCH%OBSVARLIST)
      WRITE (NDSN,10) TRIM(MSG),'OBSFLAGLIST     = ', TRIM(NML_MATCH%OBSFLAGLIST)
      WRITE (NDSN,10) TRIM(MSG),'OBSFLAGCOMP     = ', TRIM(NML_MATCH%OBSFLAGCOMP)
      WRITE (NDSN,10) TRIM(MSG),'OBSFLAGVALUE    = ', TRIM(NML_MATCH%OBSFLAGVALUE)
      WRITE (NDSN,13) TRIM(MSG),'OBSSAMEFILE     = ', NML_MATCH%OBSSAMEFILE
      WRITE (NDSN,13) TRIM(MSG),'OBSLON360       = ', NML_MATCH%OBSLON360


      WRITE (NDSN,10) TRIM(MSG),'MODFILELIST     = ', TRIM(NML_MATCH%MODFILELIST)
      WRITE (NDSN,10) TRIM(MSG),'MODTIME         = ', TRIM(NML_MATCH%MODTIME)
      WRITE (NDSN,10) TRIM(MSG),'MODVARLIST      = ', TRIM(NML_MATCH%MODVARLIST)
      WRITE (NDSN,10) TRIM(MSG),'MODFLAGLIST     = ', TRIM(NML_MATCH%MODFLAGLIST)
      WRITE (NDSN,10) TRIM(MSG),'MODFLAGCOMP     = ', TRIM(NML_MATCH%MODFLAGCOMP)
      WRITE (NDSN,10) TRIM(MSG),'MODFLAGVALUE    = ', TRIM(NML_MATCH%MODFLAGVALUE)
      WRITE (NDSN,13) TRIM(MSG),'MODSAMEFILE     = ', NML_MATCH%MODSAMEFILE
      WRITE (NDSN,13) TRIM(MSG),'MODLON360       = ', NML_MATCH%MODLON360


      WRITE (NDSN,10) TRIM(MSG),'DIR_OUT         = ', TRIM(NML_MATCH%DIR_OUT)
      WRITE (NDSN,10) TRIM(MSG),'OUTPREFIX       = ', TRIM(NML_MATCH%OUTPREFIX)
      WRITE (NDSN,10) TRIM(MSG),'OUTTIMEUNITS    = ', TRIM(NML_MATCH%OUTTIMEUNITS)
      WRITE (NDSN,10) TRIM(MSG),'FILESPLIT       = ', TRIM(NML_MATCH%FILESPLIT)
      WRITE (NDSN,11) TRIM(MSG),'NCTYPE          = ', NML_MATCH%NCTYPE

      WRITE (NDSN,10) TRIM(MSG),'TIMESTART       = ', TRIM(NML_MATCH%TIMESTART)
      WRITE (NDSN,10) TRIM(MSG),'TIMESTOP        = ', TRIM(NML_MATCH%TIMESTOP)
!      WRITE (NDSN,11) TRIM(MSG),'TIMESPLIT       = ', NML_MATCH%TIMESPLIT
      WRITE (NDSN,10) TRIM(MSG),'INTERPSPACE     = ', TRIM(NML_MATCH%INTERPSPACE)
      WRITE (NDSN,13) TRIM(MSG),'INTERPTIME      = ', NML_MATCH%INTERPTIME
      WRITE (NDSN,11) TRIM(MSG),'MAXLANDPOINTS   = ', NML_MATCH%MAXLANDPOINTS
      WRITE (NDSN,13) TRIM(MSG),'ALLTIMEINDEX    = ', NML_MATCH%ALLTIMEINDEX

      WRITE (NDSN,10) TRIM(MSG),'CLOS            = ', TRIM(NML_MATCH%CLOS)
      WRITE (NDSN,14) TRIM(MSG),'LON_MIN         = ', NML_MATCH%LON_MIN
      WRITE (NDSN,14) TRIM(MSG),'LON_MAX         = ', NML_MATCH%LON_MAX
      WRITE (NDSN,14) TRIM(MSG),'LAT_MIN         = ', NML_MATCH%LAT_MIN
      WRITE (NDSN,14) TRIM(MSG),'LAT_MAX         = ', NML_MATCH%LAT_MAX


10  FORMAT (A,2X,A,A)
11  FORMAT (A,2X,A,I8)
13  FORMAT (A,2X,A,L1)
14  FORMAT (A,2X,A,F8.2)

  END SUBROUTINE REPORT_MATCH_NML

!/ ------------------------------------------------------------------- /





END MODULE W3NMLMATCHMD

!/ ------------------------------------------------------------------- /

