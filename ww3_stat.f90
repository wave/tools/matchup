
!/ ------------------------------------------------------------------- /
      PROGRAM W3STAT
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |     M. Accensi & F. Ardhuin       |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         18-Sep-2023 |
!/                  +-----------------------------------+
!/
!
!/    16-Oct-2014 : Adaptation from ALTI_WAVE_TRACKAVG
!/    02-Nov-2014 : Debugged to work with gnu
!/    30-Oct-2015 : Modify format of output (1 file per sat.)
!/    21-Feb-2019 : Fully rebuilt
!/    21-Jan-2020 : correct SI formula
!/    27-Jan-2020 : add pearson correlation coefficient, normalized bias
!/                  and HH indicator (Hanna and Heinold 1985)
!/    31-Mar-2020 : add percentile98
!/    01-Apr-2020 : add valmax, model value corresponding to obs max value
!/    02-Apr-2020 : add valp98, model value corresponding to obs p98 value
!/    07-May-2020 : add min
!/    30-Oct-2020 : improve memory usage
!/    06-Nov-2020 : improve computation time
!/    21-Jan-2021 : remove output file if no valid data
!/    16-Mar-2021 : correct a bug in bias computation
!/    02-Sep-2021 : add FillValue management for NF90_INT type
!/    28-Jul-2023 : force stat output variable to Nf90_FLOAT type
!/    18-Sep-2023 : use NORM to compute MOD-OBS and normalize by OBS, or opposite
!/    18-Sep-2023 : add sdd for standard deviation of the difference
!
!  1. Purpose :
!
!     Averages data along the track into regular grid based on latitude bins
!     
!
!  2. Method :
!
!  3. Parameters :
!
!/ ------------------------------------------------------------------- /
  USE W3SERVMD
  USE W3TIMEMD
  USE W3NMLSTATMD
  USE NETCDF
!
  IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
  TYPE(NML_STAT_T)                 :: NML_STAT

!/T  INTEGER(KIND=2)                  :: varshort

  INTEGER                          :: iret, ncid, ncido, nctype, istart
  INTEGER                          :: NDSI, NDSE, NDSO, NDSL, NDSM, IERR
  INTEGER                          :: ivar, ntimetrk, navgmin, NLON, NLAT, indlon, indlat, itrk
  INTEGER                          :: nvartrk, nvarobs, nvarmod, nvartrkout, nvarout, ivarout
  INTEGER                          :: i, varidtmp, itime, ilat, ilon, ncoordtrk, ivartmp
  INTEGER                          :: J, NTRKFILE, IFILE, ncoords
  INTEGER                          :: nstats, n2stats, maxcellcount, it

  INTEGER                          :: dimln(2), dimid(2), varid(50), indimid(3)
  INTEGER                          :: TIMESTART(2), TIMESTOP(2)
  INTEGER                          :: DATE(8), STARTDATE(8), STOPDATE(8), TRKREFDATE(8)

  INTEGER, ALLOCATABLE             :: trkvartype(:), outvartype(:), ntimetrklist(:)
  INTEGER, ALLOCATABLE             :: indmean(:), indcount(:), indmin(:), indmax(:)
  INTEGER, ALLOCATABLE             :: indrms(:), indrmse(:), indnrmse(:), indsdd(:)
  INTEGER, ALLOCATABLE             :: indsi(:), indbias(:), indnbias(:), indcor(:)
  INTEGER, ALLOCATABLE             :: indhh(:), indvalmax(:), indvalp98(:), indp98(:)
  INTEGER, ALLOCATABLE             :: countsort(:), indexorig(:,:)
  INTEGER, ALLOCATABLE             :: TIMEINDEX(:,:,:), COUNTTIMEINDEX(:,:)

  DOUBLE PRECISION                 :: LON_MIN, LON_MAX, LAT_MIN, LAT_MAX, DX, DY

  REAL, ALLOCATABLE                :: outscalefac(:)
  REAL, ALLOCATABLE                :: trkoffset(:), trkscalefac(:), outoffset(:)
  REAL, ALLOCATABLE                :: trkvalidmin(:), trkvalidmax(:)
  REAL, ALLOCATABLE                :: outvalidmin(:), outvalidmax(:)

  DOUBLE PRECISION                 :: TRKREFJULDAY
  DOUBLE PRECISION                 :: STARTJULDAY, STOPJULDAY, OUTREFJULDAY
  DOUBLE PRECISION                 :: TMPSTDMOD, TMPSTDOBS

  DOUBLE PRECISION, ALLOCATABLE    :: OUTLATVAL(:), OUTLONVAL(:)
  DOUBLE PRECISION, ALLOCATABLE    :: trkvarval(:,:), TRKJULDAY(:)
  DOUBLE PRECISION, ALLOCATABLE    :: outvarval(:,:,:), trkfillval(:), outfillval(:)
  DOUBLE PRECISION, ALLOCATABLE    :: TMPSCATMOD(:,:,:), TMPSCATOBS(:,:,:)
  DOUBLE PRECISION, ALLOCATABLE    :: ORIGVARVAL(:,:), SORTVARVAL(:,:)

  CHARACTER*1024                   :: DIR_OUT, outncfile
  CHARACTER*1024                   :: TRKCOORDLIST, TRKVARLIST, OBSVARLIST, MODVARLIST, TRKFILE, TRKFILELIST
  CHARACTER*50                     :: calendar
  CHARACTER*128                    :: alltrkvarstr(14), allobsvarstr(6), allmodvarstr(6), alltrkcoordstr(2)
  CHARACTER*128                    :: trktimeunits
  CHARACTER(256)                   :: OUTPREFIX
  CHARACTER*3                      :: NORM

  CHARACTER*128, ALLOCATABLE       :: outunits(:), outlongname(:), outstdname(:) 
  CHARACTER*128, ALLOCATABLE       :: trkunits(:), trklongname(:), trkstdname(:)
  CHARACTER*128, ALLOCATABLE       :: outvarstr(:)

  LOGICAL                          :: filefound, FIRST
  LOGICAL                          :: TRKLON360

!/
!/ ------------------------------------------------------------------- /
!/


!==========================================================
! 1. Process input
!==========================================================


!---------------------------------------------------------------
! 1.a IO set-up
!---------------------------------------------------------------

  NDSI=10
  NDSL=12
  NDSM=13
  NDSE=6
  NDSO=6


!---------------------------------------------------------------
! 1.b Read namelist
!---------------------------------------------------------------

  CALL W3NMLSTAT (NDSI, 'ww3_stat.nml', NML_STAT, IERR)
  TRKFILELIST = NML_STAT%TRKFILELIST
  TRKCOORDLIST= NML_STAT%TRKCOORDLIST
  OBSVARLIST  = NML_STAT%OBSVARLIST
  MODVARLIST  = NML_STAT%MODVARLIST
  TRKLON360   = NML_STAT%TRKLON360

  DIR_OUT     = NML_STAT%DIR_OUT
  OUTPREFIX   = NML_STAT%OUTPREFIX
  NCTYPE      = NML_STAT%NCTYPE

  READ (NML_STAT%TIMESTART,*) TIMESTART
  READ (NML_STAT%TIMESTOP,*) TIMESTOP

  LON_MIN     = NML_STAT%LON_MIN
  LON_MAX     = NML_STAT%LON_MAX
  LAT_MIN     = NML_STAT%LAT_MIN
  LAT_MAX     = NML_STAT%LAT_MAX

  DX          = NML_STAT%DX
  DY          = NML_STAT%DY
  NAVGMIN     = NML_STAT%NAVGMIN
  NORM        = NML_STAT%NORM

  NLON = CEILING((LON_MAX - LON_MIN) / DX)
  NLAT = CEILING((LAT_MAX - LAT_MIN) / DY)

  ! add one extra index for exact modulo
  !IF(MOD((LON_MAX - LON_MIN),DX) .EQ. 0) NLON=NLON+1
  !IF(MOD((LAT_MAX - LAT_MIN),DY) .EQ. 0) NLAT=NLAT+1

  ! add one extra index for zero-crossing dimension
  ! or force indice to 1 if global stats on 1D array
  IF((NLON>1) .AND. (LON_MIN<0 .AND. LON_MAX>0)) NLON=NLON+1
  IF((NLAT>1) .AND. (LAT_MIN<0 .AND. LAT_MAX>0)) NLAT=NLAT+1

  WRITE(*,*) 'NLON : ', NLON
  WRITE(*,*) 'NLAT : ', NLAT

  ! number of input trk file
  NTRKFILE = 0
  OPEN(NDSL,FILE=TRIM(TRKFILELIST),STATUS='OLD',ERR=809,IOSTAT=IERR)
  REWIND (NDSL)
  DO
    READ (NDSL,*,END=400,ERR=802)
    NTRKFILE = NTRKFILE + 1
  END DO
  400 CONTINUE
  REWIND (NDSL)

  ALLOCATE(ntimetrklist(NTRKFILE))


  ! output variable names from trk
  nvarobs=0
  nvarmod=0
  ncoordtrk=0
  nvartrk=0
  WRITE(TRKVARLIST,'(5A)') TRIM(OBSVARLIST), ' ', TRIM(MODVARLIST), ' ', TRIM(TRKCOORDLIST)
  CALL STRSPLIT(MODVARLIST,allmodvarstr,nvarmod)
  CALL STRSPLIT(OBSVARLIST,allobsvarstr,nvarobs)
  CALL STRSPLIT(TRKCOORDLIST,alltrkcoordstr,ncoordtrk)
  CALL STRSPLIT(TRKVARLIST,alltrkvarstr,nvartrk)

  nvartrkout=nvarobs+nvarmod ! substract lat and lon from track
  ilat=nvartrk-1
  ilon=nvartrk
  write(*,*) 'latitude from nml : ', alltrkvarstr(ilat)
  write(*,*) 'longitude from nml : ', alltrkvarstr(ilon)

  ALLOCATE(indmean(nvartrkout))
  ALLOCATE(indcount(nvartrkout))
  ALLOCATE(indmin(nvartrkout))
  ALLOCATE(indmax(nvartrkout))
  ALLOCATE(indrms(nvartrkout))
  ALLOCATE(indp98(nvartrkout))

  IF (NVARMOD.GT.0) THEN
    ALLOCATE(indvalmax(nvarmod))
    ALLOCATE(indvalp98(nvarmod))
    ALLOCATE(indrmse(nvarmod))
    ALLOCATE(indnrmse(nvarmod))
    ALLOCATE(indsdd(nvarmod))
    ALLOCATE(indsi(nvarmod))
    ALLOCATE(indhh(nvarmod))
    ALLOCATE(indbias(nvarmod))
    ALLOCATE(indnbias(nvarmod))
    ALLOCATE(indcor(nvarmod))
  END IF


  ! start and stop time
  CALL T2D(TIMESTART,STARTDATE,IERR)
  CALL D2J(STARTDATE,STARTJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)') 'input file start time : ', STARTDATE(:)
  CALL T2D(TIMESTOP,STOPDATE,IERR)
  CALL D2J(STOPDATE,STOPJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)') 'input file stop  time : ', STOPDATE(:)


!==========================================================
!
! 2. Read the first trk file to setup the refdate & attributes
!
!==========================================================

  WRITE(NDSO,'(/A)') 'Open trk file : '
  READ (NDSL,'(A1024)',END=801,ERR=802) TRKFILE
  WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(TRKFILE)
  iret = nf90_open(TRKFILE, NF90_NOWRITE, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! get the time dimension
  iret=nf90_inq_dimid(ncid,'time',indimid(1))
  IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'mes',indimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_inquire_dimension(ncid,indimid(1),len=ntimetrk)

  ! get the trk reference time
  DATE(:)=0
  iret=nf90_inq_varid(ncid,'time', varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_att(ncid,varidtmp,"calendar",calendar)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(NDSE,1028)
  ELSE IF ((INDEX(calendar, "standard").EQ.0) .AND. &
           (INDEX(calendar, "gregorian").EQ.0)) THEN
    WRITE(NDSE,1029)
    STOP
  END IF

  IRET=NF90_GET_ATT(NCID,VARIDTMP,'units',TRKTIMEUNITS)
  CALL U2D(TRKTIMEUNITS,TRKREFDATE,IERR)
  CALL D2J(TRKREFDATE,TRKREFJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)')  'trk reference time : ', TRKREFDATE(:)

  ! allocate attributes arrays
  allocate(trkvartype(nvartrk))
  allocate(trklongname(nvartrk))
  allocate(trkstdname(nvartrk))
  allocate(trkoffset(nvartrk))
  allocate(trkscalefac(nvartrk))
  allocate(trkunits(nvartrk))
  allocate(trkfillval(nvartrk))
  allocate(trkvalidmin(nvartrk))
  allocate(trkvalidmax(nvartrk))
  

  ! Allocate attributes arrays
  ncoords=2 ! latitude, longitude
  nstats=6 ! for each 'trk' : mean, count, min, max, rms, p98
  n2stats=0
  IF (nvarmod.GT.0) THEN
    IF(nvarobs.NE.nvarmod) THEN
      WRITE(NDSE,*) '[ERROR] nvarmod not equal to nvarobs'
      STOP
    END IF
    n2stats=10 ! for each 'mod VS obs' : valmax, valp98, rmse, nrmse, sdd, si, hh, bias, nbias, cor
  END IF

  ALLOCATE(outvartype(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outoffset(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outscalefac(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outunits(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outlongname(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outstdname(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outvalidmin(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outvalidmax(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outfillval(ncoords + nvartrkout*nstats + nvarmod*n2stats))
  ALLOCATE(outvarstr(ncoords + nvartrkout*nstats + nvarmod*n2stats))


  ! output variable names
  nvarout=ncoords ! latitude,longitude
  outvarstr(1)='latitude'
  outvarstr(2)='longitude'

  ! get 'trk' attributes (obs and mod variables)
  DO ivar=1,nvartrkout

    ! inq variable
    write(NDSO,'(2A)') 'variable : ', trim(alltrkvarstr(ivar))
    iret=nf90_inq_varid(ncid, trim(alltrkvarstr(ivar)), varid(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_variable(ncid,varid(ivar),xtype=trkvartype(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! process attributes 
    iret=nf90_get_att(ncid,varid(ivar),'add_offset',trkoffset(ivar))
    if (iret.NE.0) trkoffset(ivar)=0.0
    iret=nf90_get_att(ncid,varid(ivar),'scale_factor',trkscalefac(ivar))
    if (iret.NE.0) trkscalefac(ivar)=1.0
    iret=nf90_get_att(ncid,varid(ivar),'_FillValue',trkfillval(ivar))
    if (iret.NE.0) THEN
      if(trkvartype(ivar).EQ.NF90_SHORT) trkfillval(ivar)=NF90_FILL_SHORT
      if(trkvartype(ivar).EQ.NF90_INT) trkfillval(ivar)=NF90_FILL_INT
      if(trkvartype(ivar).EQ.NF90_FLOAT) trkfillval(ivar)=NF90_FILL_FLOAT
      if(trkvartype(ivar).EQ.NF90_DOUBLE) trkfillval(ivar)=NF90_FILL_DOUBLE
    END IF
    iret=nf90_get_att(ncid,varid(ivar),'units',trkunits(ivar))
    if (iret.NE.0) trkunits(ivar)='unknown'
    iret=nf90_get_att(ncid,varid(ivar),'long_name',trklongname(ivar))
    if (iret.NE.0) trklongname(ivar)=alltrkvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'standard_name',trkstdname(ivar))
    if (iret.NE.0) trkstdname(ivar)=alltrkvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'valid_min',trkvalidmin(ivar))
    if (iret.NE.0) trkvalidmin(ivar)=-1000
    iret=nf90_get_att(ncid,varid(ivar),'valid_max',trkvalidmax(ivar))
    if (iret.NE.0) trkvalidmin(ivar)=1000


!-----------
! mean
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indmean(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'mean : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'mean_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'mean of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'mean_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=trkvalidmin(ivar)
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! count
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indcount(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'count : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'count_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_INT
    outoffset(nvarout)=0.
    outscalefac(nvarout)=1.
    outfillval(nvarout)=NF90_FILL_INT
    outunits(nvarout)=''
    WRITE(outlongname(nvarout),'(2A)') 'count of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'count_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=ntimetrk

!-----------
! min
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indmin(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'min : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'min_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'minimum of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'min_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! max
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indmax(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'max : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'max_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'maximum of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'max_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! rms
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indrms(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'rms : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'rms_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'root mean square of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'rms_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! p98
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indp98(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'p98 : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'p98_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'percentile 98 of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'p98_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)
  END DO


  ! get 'obs VS mod' attributes
  IF (nvarmod.GT.0) THEN
    DO ivar=1,nvarmod

!-----------
! valmax
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indvalmax(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'valmax : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'valmax_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'model value for maximum obs value of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'valmax_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! valp98
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indvalp98(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'valp98 : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'valp98_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=trkvartype(ivar)
    outoffset(nvarout)=trkoffset(ivar)
    outscalefac(nvarout)=trkscalefac(ivar)
    outfillval(nvarout)=trkfillval(ivar)
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'model value for percentile98 obs value of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'valp98_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! rmse
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indrmse(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'rmse : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'rmse_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'root mean square error of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'rmse_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-trkvalidmax(ivar)
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! nrmse
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indnrmse(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'nrmse : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'nrmse_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)='%'
    WRITE(outlongname(nvarout),'(2A)') 'normalized root mean square error of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'nrmse_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-100
    outvalidmax(nvarout)=100

!-----------
! sdd
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indsdd(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'sdd : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'sdd_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'standard deviation of the difference of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'sdd_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-trkvalidmax(ivar)
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! si
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indsi(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'si : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'si_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)='%'
    WRITE(outlongname(nvarout),'(2A)') 'scatter index of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'si_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=0
    outvalidmax(nvarout)=100

!-----------
! hh
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indhh(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'hh : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'hh_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)='%'
    WRITE(outlongname(nvarout),'(2A)') 'Hanna and Heinold indicator of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'hh_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-100
    outvalidmax(nvarout)=100

!-----------
! bias
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indbias(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'bias : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'bias_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)=trkunits(ivar)
    WRITE(outlongname(nvarout),'(2A)') 'bias of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'bias_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-trkvalidmax(ivar)
    outvalidmax(nvarout)=trkvalidmax(ivar)

!-----------
! nbias
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indnbias(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'nbias : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'nbias_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)='%'
    WRITE(outlongname(nvarout),'(2A)') 'normalized bias of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'nbias_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-100
    outvalidmax(nvarout)=100

!-----------
! cor
!-----------

    ! increment output variable index
    nvarout=nvarout+1
    indcor(ivar)=nvarout

    ! output name, attributes
    write(NDSO,'(2A)') 'cor : ', trim(alltrkvarstr(ivar))
    WRITE(outvarstr(nvarout),'(2A)') 'cor_',TRIM(alltrkvarstr(ivar))
    outvartype(nvarout)=NF90_FLOAT
    outoffset(nvarout)=0
    outscalefac(nvarout)=1
    outfillval(nvarout)=NF90_FILL_FLOAT
    outunits(nvarout)=''
    WRITE(outlongname(nvarout),'(2A)') 'pearson correlation coefficient of ',TRIM(alltrkvarstr(ivar))
    WRITE(outstdname(nvarout),'(2A)') 'cor_',TRIM(alltrkvarstr(ivar))
    outvalidmin(nvarout)=-1
    outvalidmax(nvarout)=1


    END DO
  END IF



!==========================================================
!
! 3. Create output file
!
!==========================================================

  ! Check if corresponding stat file exists
  WRITE(outncfile,'(4A)') trim(DIR_OUT),'/', TRIM(OUTPREFIX), 'stat.nc'
  WRITE(6,'(/A)') "Defining stat file :"
  WRITE(6,'(A)') trim(adjustl(adjustr(outncfile)))
  inquire(file=trim(outncfile), exist=filefound)
  IF(filefound) THEN
    WRITE(NDSO,'(3A)') '[WARNING] There was already a stat file ', TRIM(OUTNCFILE), ' in the folder: it will be deleted.'
    open(30, file=trim(outncfile), form='unformatted')
    close(30, status='delete') 
  END IF

  ! create output altimeter file 
  write(NDSO,'(/A)') 'Creating stat file : '
  WRITE(NDSO,'(A)') trim(outncfile)
  dimln(1)=NLON
  dimln(2)=NLAT
  CALL W3CRNC(trim(outncfile), NCTYPE, NORM, dimln, dimid, varid, ncido)


  ! define the variables in track file
  write(NDSO,'(/A)') 'Defining the output variables : '
  DO ivar=ncoords+1,nvarout ! skip latitude, longitude
    write(NDSO,'(A)') trim(outvarstr(ivar))
    iret=nf90_redef(ncido)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! use outvarstr to define the name of variables in track file
    iret=nf90_def_var(ncido, trim(outvarstr(ivar)), outvartype(ivar), (/dimid(1),dimid(2)/), varid(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'units',outunits(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'long_name',outlongname(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'standard_name',outstdname(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'add_offset',outoffset(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'scale_factor',outscalefac(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    if(outvartype(ivar).EQ.NF90_SHORT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',INT(outfillval(ivar),2))
    if(outvartype(ivar).EQ.NF90_INT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',INT(outfillval(ivar)))
    if(outvartype(ivar).EQ.NF90_FLOAT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',REAL(outfillval(ivar)))
    if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',DBLE(outfillval(ivar)))
    CALL CHECK_ERROR(IRET,__LINE__)
!    iret=nf90_put_att(ncido,varid(ivar),'valid_min',outvalidmin(ivar))
!    CALL CHECK_ERROR(IRET,__LINE__)
!    iret=nf90_put_att(ncido,varid(ivar),'valid_max',outvalidmax(ivar))
!    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,varid(ivar),'axis','XY')
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_enddef(ncido)
    CALL CHECK_ERROR(IRET,__LINE__)
  END DO



  ! Define refdate for an output in "days since 1900-01-01T00:00:00Z"
  CALL U2D('days since 1990-01-01 00:00:00',DATE,IERR)
  CALL D2J(DATE,OUTREFJULDAY,IERR)



!==========================================================
!
! 4. Loop on all trk files to find the time dimensions
!
!==========================================================

  ntimetrk=0
  WRITE(NDSO,'(/A)') 'Searching the time trk... '
  REWIND(NDSL)
  DO ITRK=1,NTRKFILE
!/T    WRITE(NDSO,'(/A,I6,A,I6)') 'Opening obs file number ', IOBS,' /',NOBSFILE
    READ (NDSL,'(A1024)',END=801,ERR=802) TRKFILE
!/T    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(TRKFILE)
    ! open file if exists, otherwise exit the loop
    iret = nf90_open(trim(TRKFILE), NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get the time dimension
    iret=nf90_inq_dimid(ncid,'time',indimid(1))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'mes',indimid(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,indimid(1),len=ntimetrklist(ITRK))
    ntimetrk=ntimetrk+ntimetrklist(ITRK)

    ! close obs file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)  

  END DO ! itrk=1,ntrkfile

  WRITE(NDSO,'(A,I12)') 'all time trk : ', ntimetrk



!==========================================================
!
! 4. Get the track variables
!
!==========================================================


  ALLOCATE(TRKJULDAY(ntimetrk))
  ALLOCATE(TRKVARVAL(ntimetrk,nvartrk))
  REWIND(NDSL)
  istart=1

  DO IFILE=1,NTRKFILE

    WRITE(NDSO,'(/A)') 'Open trk file : '
    READ (NDSL,'(A1024)',END=801,ERR=802) TRKFILE
    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(TRKFILE)
    iret = nf90_open(TRKFILE, NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get the time variable
    write(NDSO,'(/A)') 'Get the track variables : '
    write(NDSO,'(A)') '  variable : time '
    iret=nf90_inq_varid(ncid,'time', varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    write(*,*) 'array [',istart,':', istart+ntimetrklist(ifile)-1,']'
    iret=nf90_get_var(ncid, varidtmp, TRKJULDAY(istart:istart+ntimetrklist(ifile)-1))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get variables
    DO ivar=1,nvartrk
      write(NDSO,'(2A)') '  variable : ', trim(alltrkvarstr(ivar))
      iret=nf90_inq_varid(ncid, trim(alltrkvarstr(ivar)), varidtmp)
      CALL CHECK_ERROR(IRET,__LINE__)
      iret=nf90_get_var(ncid, varidtmp, TRKVARVAL(istart:istart+ntimetrklist(ifile)-1,ivar))
      CALL CHECK_ERROR(IRET,__LINE__)
    END DO

    ! close trk file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    istart=istart+ntimetrklist(ifile)

  END DO

  ! convert the julian date in calendar date
  IF (index(trktimeunits, "seconds").NE.0)    TRKJULDAY(:)=TRKJULDAY(:)/86400.
  IF (index(trktimeunits, "minutes").NE.0)   TRKJULDAY(:)=TRKJULDAY(:)/1440.
  IF (index(trktimeunits, "hours").NE.0)     TRKJULDAY(:)=TRKJULDAY(:)/24.
  TRKJULDAY(:)=TRKREFJULDAY+TRKJULDAY(:)


!==========================================================
!
! 5. Loop on track measurements
!
!==========================================================



  ALLOCATE(OUTVARVAL(NLON,NLAT,nvarout))
  ALLOCATE(OUTLATVAL(NLAT))
  ALLOCATE(OUTLONVAL(NLON))
  ALLOCATE(TMPSCATMOD(NLON,NLAT,nvarout))
  ALLOCATE(TMPSCATOBS(NLON,NLAT,nvarout))

  ! set lat/lon grid

  J=1
  write(NDSO,'(/A)') 'Get the lat/lon grid'
  DO WHILE ((LAT_MIN+DY*(J-1)).LE.(LAT_MAX+0.5*DY))
!/T write(*,*) 'nlat: ', nlat, j, LAT_MIN+DY*(J-1), LAT_MAX
    OUTLATVAL(j)=LAT_MIN+DY*(J-1)
    J=J+1
  END DO
  I=1
  DO WHILE ((LON_MIN+DX*(I-1)).LE.(LON_MAX+0.5*DX))
!/T write(*,*) 'nlon: ', nlon, i, LON_MIN+DX*(I-1), LON_MAX
    OUTLONVAL(i)=LON_MIN+DX*(I-1)
    I=I+1
  END DO


  itime=1
  FIRST=.TRUE.
  OUTVARVAL(:,:,:)=0.
  TMPSCATMOD(:,:,:)=0.
  TMPSCATOBS(:,:,:)=0.

  !
  ! compute time statistics (obs and mod variables)
  !
  WRITE(NDSO,*) 'Compute time statistics'
  TIME : DO itime=1,ntimetrk

    ! compute latitude and longitude indexes
    indlat=FLOOR(TRKVARVAL(itime,ilat)/dy)-FLOOR(LAT_MIN/dy)+1 ! +1 fortran index
    indlon=FLOOR(TRKVARVAL(itime,ilon)/dx)-FLOOR(LON_MIN/dx)+1 ! +1 fortran index
    ! force indice to 1 if global stats on 1D array
    IF (NLAT.EQ.1) INDLAT=1
    IF(NLON.EQ.1) INDLON=1

!/T write(*,*) ''
!/T write(*,*) TRKVARVAL(itime,ilat), TRKVARVAL(itime,ilon)
!/T write(*,*) indlat, indlon
!/T write(*,*) ''
!/T write(*,*) TRKJULDAY(itime), ' - ', STARTJULDAY, '/', STOPJULDAY

    ! skip if time before start time
    IF (TRKJULDAY(itime).LT.STARTJULDAY ) CYCLE TIME

    ! skip if time after stop time
    IF (TRKJULDAY(itime).GE.STOPJULDAY ) CYCLE TIME

    ! skip if one of out variable is fillvalue
    DO ivar=1,nvartrkout
      IF (TRKVARVAL(itime,ivar).EQ.trkfillval(ivar) ) CYCLE TIME
    END DO

    ! apply scale_factor and offset
    DO ivar=1,nvartrkout
     TRKVARVAL(itime,ivar)=TRKVARVAL(itime,ivar)*trkscalefac(ivar)+trkoffset(ivar)
    END DO

    ! compute 'mod CROSS obs' maximum - must be done before 'trk' time statistics
    IF (nvarmod.GT.0) THEN
      DO ivar=1,nvarmod
      ! valmax : model value for max obs value
        IF(TRKVARVAL(itime,ivar).GT.OUTVARVAL(indlon,indlat,indmax(ivar))) THEN
          OUTVARVAL(indlon,indlat,indvalmax(ivar))=TRKVARVAL(itime,nvarobs+ivar)
        END IF
      END DO
    END IF

    ! compute 'trk' time statistics (obs and mod variables)
    DO ivar=1,nvartrkout
      ! mean : sum[trk]
      OUTVARVAL(indlon,indlat,indmean(ivar))=OUTVARVAL(indlon,indlat,indmean(ivar))+TRKVARVAL(itime,ivar)
      ! count : i++
      OUTVARVAL(indlon,indlat,indcount(ivar))=OUTVARVAL(indlon,indlat,indcount(ivar))+1
      ! min : min(trk)
      IF (OUTVARVAL(indlon,indlat,indcount(ivar)).EQ.1) THEN
        OUTVARVAL(indlon,indlat,indmin(ivar))=TRKVARVAL(itime,ivar)
      ELSE
        OUTVARVAL(indlon,indlat,indmin(ivar))=MIN(OUTVARVAL(indlon,indlat,indmin(ivar)),TRKVARVAL(itime,ivar))
      END IF
      ! max : max(trk)
      OUTVARVAL(indlon,indlat,indmax(ivar))=MAX(OUTVARVAL(indlon,indlat,indmax(ivar)),TRKVARVAL(itime,ivar))
      ! rms : sum[(trk)**2]
      OUTVARVAL(indlon,indlat,indrms(ivar))=OUTVARVAL(indlon,indlat,indrms(ivar))+TRKVARVAL(itime,ivar)**2
    END DO


    ! compute 'obs VS mod' time statistics
    IF (nvarmod.GT.0) THEN
      DO ivar=1,nvarmod
        ! rmse : sum[(mod-obs)**2]
        OUTVARVAL(indlon,indlat,indrmse(ivar))=OUTVARVAL(indlon,indlat,indrmse(ivar))+(TRKVARVAL(itime,nvarobs+ivar)-TRKVARVAL(itime,ivar))**2
        IF (INDEX(NORM, "OBS").NE.0) THEN
          ! bias : sum[(mod-obs)]
          OUTVARVAL(indlon,indlat,indbias(ivar))=OUTVARVAL(indlon,indlat,indbias(ivar))+(TRKVARVAL(itime,nvarobs+ivar)-TRKVARVAL(itime,ivar))
          ! nbias : sum[obs]
          OUTVARVAL(indlon,indlat,indnbias(ivar))=OUTVARVAL(indlon,indlat,indnbias(ivar))+ TRKVARVAL(itime,ivar)
          ! nrmse : sum[obs**2]
          OUTVARVAL(indlon,indlat,indnrmse(ivar))=OUTVARVAL(indlon,indlat,indnrmse(ivar))+ TRKVARVAL(itime,ivar)**2
        ELSE IF (INDEX(NORM, "MOD").NE.0) THEN
          ! bias : sum[(obs-mod)]
          OUTVARVAL(indlon,indlat,indbias(ivar))=OUTVARVAL(indlon,indlat,indbias(ivar))+(TRKVARVAL(itime,ivar)-TRKVARVAL(itime,nvarobs+ivar))
          ! nbias : sum[mod]
          OUTVARVAL(indlon,indlat,indnbias(ivar))=OUTVARVAL(indlon,indlat,indnbias(ivar))+ TRKVARVAL(itime,nvarobs+ivar)
          ! nrmse : sum[mod**2]
          OUTVARVAL(indlon,indlat,indnrmse(ivar))=OUTVARVAL(indlon,indlat,indnrmse(ivar))+ TRKVARVAL(itime,nvarobs+ivar)**2
        END IF
        ! hh : sum[mod*obs]
        OUTVARVAL(indlon,indlat,indhh(ivar))=OUTVARVAL(indlon,indlat,indhh(ivar))+ (TRKVARVAL(itime,nvarobs+ivar)*TRKVARVAL(itime,ivar))
      END DO
    END IF
  END DO TIME

  
  

  !
  ! Check arrays size for allocation
  !
  WRITE(NDSO,'(A)') 'Check arrays size'
  maxcellcount=MAXVAL(OUTVARVAL(:,:,indcount(:)))
  IF (maxcellcount .EQ. 0) THEN
    DO ivar=1,nvartrkout
      WRITE(NDSO,*) 'sum of count for ', trim(alltrkvarstr(ivar)), ' : ', SUM(OUTVARVAL(:,:,indcount(ivar)))
    END DO
    ! remove file if no data
    WRITE(NDSO,'(/A)') '[INFO] No matching data recorded in output file. It will be deleted'
    OPEN(30, file=trim(outncfile), form='unformatted')
    CLOSE(30, status='delete')
    GOTO 888
  ELSE
  ! allocate arrays over maxcellcount
    WRITE(NDSO,*) 'Allocate arrays over maxcellcount : ', maxcellcount
    IF (nvarmod.GT.0) THEN
      ALLOCATE(ORIGVARVAL(2,maxcellcount))
      ALLOCATE(SORTVARVAL(2,maxcellcount))
      ALLOCATE(indexorig(2,maxcellcount))
      ALLOCATE(countsort(2))
    ELSE
      ALLOCATE(ORIGVARVAL(1,maxcellcount))
      ALLOCATE(SORTVARVAL(1,maxcellcount))
      ALLOCATE(indexorig(1,maxcellcount))
      ALLOCATE(countsort(1))
    END IF
  END IF


  !
  ! Store time index for each lat/lon cell
  !
  WRITE(NDSO,*) 'Store time indexes per lat/lon cell : ', NLON, '*', NLAT, '*', maxcellcount
  ALLOCATE(TIMEINDEX(NLON,NLAT,maxcellcount))
  ALLOCATE(COUNTTIMEINDEX(NLON,NLAT))
  TIMEINDEX=-1
  COUNTTIMEINDEX=0
  TIMEIND: DO itime=1,ntimetrk
    ! skip if time before start time
    IF (TRKJULDAY(itime).LT.STARTJULDAY ) CYCLE TIMEIND

    ! skip if time after stop time
    IF (TRKJULDAY(itime).GE.STOPJULDAY ) CYCLE TIMEIND

    ! skip if one of out variable is fillvalue
    DO ivartmp=1,nvartrkout
      IF (TRKVARVAL(itime,ivartmp).EQ.trkfillval(ivartmp) ) CYCLE TIMEIND
    END DO

    indlat=FLOOR(TRKVARVAL(itime,ilat)/dy)-FLOOR(LAT_MIN/dy)+1 ! +1 fortran index
    indlon=FLOOR(TRKVARVAL(itime,ilon)/dx)-FLOOR(LON_MIN/dx)+1 ! +1 fortran index  
    ! force indice to 1 if global stats on 1D array
    IF (NLAT.EQ.1) INDLAT=1
    IF(NLON.EQ.1) INDLON=1

    counttimeindex(indlon,indlat)=counttimeindex(indlon,indlat)+1
    TIMEINDEX(indlon,indlat,counttimeindex(indlon,indlat))=itime
  END DO TIMEIND


  !
  ! Compute percentile
  !
  WRITE(NDSO,'(A)') 'Compute percentile98 and model value for obs percentile98 value'
  DO ivar=1,nvarobs
    WRITE(NDSO,'(2A)') '  variable : ', trim(allobsvarstr(ivar))
    DO i=1,NLON
      DO j=1,NLAT
        
!/T        WRITE(NDSO,*) 'computing ', ((i-1)*NLAT+j)/(NLON*NLAT)*100, ' %'  

        maxcellcount=MAXVAL(OUTVARVAL(i,j,indcount(:)))
        IF (maxcellcount.EQ.0) THEN
!/T          WRITE(NDSO,*) 'No data at this location'
          CYCLE
        END IF

!/T        WRITE(NDSO,*) 'Initialize array'
        ORIGVARVAL=0.
        SORTVARVAL=0.
        indexorig=0
        countsort=0

!/T        WRITE(NDSO,*) 'Store array'
        TIME2 : DO it=1,maxcellcount
          ! store obs values in array at given lat/lon/var
          countsort(1)=countsort(1)+1
          indexorig(1,countsort(1))=countsort(1)
          ORIGVARVAL(1,countsort(1))=TRKVARVAL(TIMEINDEX(i,j,it),ivar)

          ! store mod values in array at given lat/lon/var
          IF (nvarmod.GT.0) THEN
            countsort(2)=countsort(2)+1
            indexorig(2,countsort(2))=countsort(2)
            ORIGVARVAL(2,countsort(2))=TRKVARVAL(TIMEINDEX(i,j,it),nvarobs+ivar)
          END IF
        END DO TIME2


        !
        ! sort array
        !
!/T        WRITE(NDSO,*) 'Sort array'
        SORTVARVAL=ORIGVARVAL
        IF (countsort(1).GT.0) THEN
          ! sort obs array over only the effective count at given lat/lon/var
          CALL SORT(countsort(1),SORTVARVAL(1,1:countsort(1)), &
                    indexorig(1,1:countsort(1)))
        END IF
        IF (countsort(2).GT.0) THEN
          ! sort mod array over only the effective count at given lat/lon/var
          CALL SORT(countsort(2),SORTVARVAL(2,1:countsort(2)), &
                    indexorig(2,1:countsort(2)))
        END IF


        !
        ! compute 'trk' percentile (obs and mod variables)
        ! and compute 'mod CROSS obs' percentile
        !
!/T        WRITE(NDSO,*) 'Compute percentile98'
        IF (countsort(1).GT.0) THEN
          ! p98 : trk[round(0.98*count(trk_sorted))]
          OUTVARVAL(i,j,indp98(ivar))=SORTVARVAL(1,NINT(0.98*countsort(1)))
        END IF
        IF (countsort(2).GT.0) THEN
          ! p98 : trk[round(0.98*count(trk_sorted))]
          OUTVARVAL(i,j,indp98(nvarobs+ivar))=SORTVARVAL(2,NINT(0.98*countsort(2)))
          ! valp98 : model value for p98 obs value
          OUTVARVAL(i,j,indvalp98(ivar))=ORIGVARVAL(2,indexorig(1,NINT(0.98*countsort(1))))
        END IF

      END DO ! NLAT
    END DO ! NLON
  END DO ! nvarobs

  DEALLOCATE(SORTVARVAL)
  DEALLOCATE(COUNTSORT)
  DEALLOCATE(ORIGVARVAL)
  DEALLOCATE(INDEXORIG)
  DEALLOCATE(TIMEINDEX)
  DEALLOCATE(COUNTTIMEINDEX)


  !
  ! compute 'trk' averaging (obs and mod variables)
  !
  WRITE(NDSO,*) 'Compute mean and rms'
  DO i=1,NLON
    DO j=1,NLAT
      DO ivar=1,nvartrkout
        ! skip on minimum threshold
        IF (OUTVARVAL(i,j,indcount(ivar)).GT.NAVGMIN) THEN
          ! mean : sum[trk]/count
          OUTVARVAL(i,j,indmean(ivar))=OUTVARVAL(i,j,indmean(ivar))/OUTVARVAL(i,j,indcount(ivar)) 
          ! rms : sqrt{ sum[(trk)**2]/count }
          OUTVARVAL(i,j,indrms(ivar))=SQRT(OUTVARVAL(i,j,indrms(ivar))/OUTVARVAL(i,j,indcount(ivar))) 
        ELSE
          OUTVARVAL(i,j,indmean(ivar))=outfillval(indmean(ivar))
          OUTVARVAL(i,j,indrms(ivar))=outfillval(indrms(ivar))
        END IF
      END DO
    END DO
  END DO


  !
  ! compute 'trk' time scatter (obs and mod variables)
  !
  WRITE(NDSO,*) 'Compute time scatter'
  TIME3 : DO itime=1,ntimetrk

    ! compute latitude and longitude indexes
    indlat=FLOOR(TRKVARVAL(itime,ilat)/dy)-FLOOR(LAT_MIN/dy)+1 ! +1 fortran index
    indlon=FLOOR(TRKVARVAL(itime,ilon)/dx)-FLOOR(LON_MIN/dx)+1 ! +1 fortran index
    ! force indice to 1 if global stats on 1D array
    IF (NLAT.EQ.1) INDLAT=1
    IF(NLON.EQ.1) INDLON=1

    ! skip if time before start time
    IF (TRKJULDAY(itime).LT.STARTJULDAY ) CYCLE TIME3

    ! skip if time after stop time
    IF (TRKJULDAY(itime).GE.STOPJULDAY ) CYCLE TIME3

    ! skip if one of out variable is fillvalue
    DO ivar=1,nvartrkout
      IF (TRKVARVAL(itime,ivar).EQ.trkfillval(ivar) ) CYCLE TIME3
    END DO

    ! compute 'obs VS mod' time scatter
    IF (nvarmod.GT.0) THEN
      DO ivar=1,nvarmod
        ! skip on minimum threshold
        IF (OUTVARVAL(indlon,indlat,indcount(ivar)).GT.NAVGMIN) THEN
          ! temporary standard deviation for mod
          TMPSTDMOD=TRKVARVAL(itime,nvarobs+ivar)-OUTVARVAL(indlon,indlat,indmean(nvarobs+ivar))
          ! temporary standard deviation for obs
          TMPSTDOBS=TRKVARVAL(itime,ivar)-OUTVARVAL(indlon,indlat,indmean(ivar))
          ! si : sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2]
          OUTVARVAL(indlon,indlat,indsi(ivar))=OUTVARVAL(indlon,indlat,indsi(ivar)) + ( TMPSTDMOD - TMPSTDOBS )**2
          ! scatmod : sum[(mod-mean(mod))**2]
          TMPSCATMOD(indlon,indlat,ivar)=TMPSCATMOD(indlon,indlat,ivar) + TMPSTDMOD**2
          ! scatobs : sum[(obs-mean(obs))**2]
          TMPSCATOBS(indlon,indlat,ivar)=TMPSCATOBS(indlon,indlat,ivar) + TMPSTDOBS**2
          ! cor : sum[ (mod-mean(mod)) * (obs-mean(obs)) ]
          OUTVARVAL(indlon,indlat,indcor(ivar))=OUTVARVAL(indlon,indlat,indcor(ivar)) + TMPSTDMOD*TMPSTDOBS
        END IF
      END DO
    END IF
  END DO TIME3


  !
  ! compute 'obs VS mod' averaging
  !
  WRITE(NDSO,*) 'Compute sdd, si, nrmse, hh, rmse, bias, nbias and cor'
  IF (nvarmod.GT.0) THEN
    DO i=1,NLON
      DO j=1,NLAT
        DO ivar=1,nvarmod
          ! skip on minimum threshold
          IF (OUTVARVAL(i,j,indcount(ivar)).GT.NAVGMIN) THEN
            ! sdd : sqrt { sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / count }
            OUTVARVAL(i,j,indsdd(ivar))=SQRT(OUTVARVAL(i,j,indsi(ivar))/OUTVARVAL(i,j,indcount(ivar)))
            ! si : sqrt { sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / sum[norm**2] } * 100
            OUTVARVAL(i,j,indsi(ivar))=SQRT(OUTVARVAL(i,j,indsi(ivar))/OUTVARVAL(i,j,indnrmse(ivar)))*100
            ! nrmse : sqrt{ sum[(mod-obs)**2] / sum[norm**2] } * 100
            OUTVARVAL(i,j,indnrmse(ivar))=SQRT(OUTVARVAL(i,j,indrmse(ivar))/(OUTVARVAL(i,j,indnrmse(ivar))))*100
            ! hh : sqrt{ sum[(mod-obs)**2]/sum[mod*obs] } *100
            OUTVARVAL(i,j,indhh(ivar))=SQRT(OUTVARVAL(i,j,indrmse(ivar))/OUTVARVAL(i,j,indhh(ivar)))*100
            ! rmse : sqrt{ sum[(mod-obs)**2]/count }
            OUTVARVAL(i,j,indrmse(ivar))=SQRT(OUTVARVAL(i,j,indrmse(ivar))/OUTVARVAL(i,j,indcount(ivar)))
            ! nbias : (sum(mod-obs)/sum(norm)) *100 .OR. (sum(obs-mod)/sum(norm)) *100
            OUTVARVAL(i,j,indnbias(ivar))=(OUTVARVAL(i,j,indbias(ivar))/OUTVARVAL(i,j,indnbias(ivar)))*100
            ! bias : sum(mod-obs)/count .OR. sum(obs-mod)/count
            OUTVARVAL(i,j,indbias(ivar))=OUTVARVAL(i,j,indbias(ivar))/OUTVARVAL(i,j,indcount(ivar))
            ! cor : sum[ (mod-mean(mod)) * (obs-mean(obs)) ] / (sqrt{ sum[(mod-mean(mod))**2]}*sqrt{ sum[(obs-mean(obs))**2]} })
            OUTVARVAL(i,j,indcor(ivar))=OUTVARVAL(i,j,indcor(ivar)) / ( SQRT(TMPSCATMOD(i,j,ivar))*SQRT(TMPSCATOBS(i,j,ivar)) )
          ELSE
            OUTVARVAL(i,j,indsdd(ivar))=outfillval(indsdd(ivar))
            OUTVARVAL(i,j,indsi(ivar))=outfillval(indsi(ivar))
            OUTVARVAL(i,j,indnrmse(ivar))=outfillval(indnrmse(ivar))
            OUTVARVAL(i,j,indhh(ivar))=outfillval(indhh(ivar))
            OUTVARVAL(i,j,indrmse(ivar))=outfillval(indrmse(ivar))
            OUTVARVAL(i,j,indbias(ivar))=outfillval(indbias(ivar))
            OUTVARVAL(i,j,indnbias(ivar))=outfillval(indnbias(ivar))
            OUTVARVAL(i,j,indcor(ivar))=outfillval(indcor(ivar))
          END IF
        END DO
      END DO
    END DO
  END IF


  !
  ! apply scale factor and offset
  !
  DO i=1,NLON
    DO j=1,NLAT
      DO ivarout=ncoords+1,nvarout
        ! skip on minimum threshold
        IF (OUTVARVAL(i,j,ivarout).NE.outfillval(ivarout)) THEN
          OUTVARVAL(i,j,ivarout)=(OUTVARVAL(i,j,ivarout)-outoffset(ivarout))/outscalefac(ivarout)
        END IF
!/T        ! test to set at NaN values which are above the max value allowed by the var type
!/T        IF(outvartype(ivarout).EQ.NF90_SHORT) THEN
!/T        !  IF (ivarout.eq.18) WRITE(NDSE,*) 'short : ', OUTVARVAL(i,j,ivarout), HUGE(varshort)
!/T          IF (OUTVARVAL(i,j,ivarout).GT.HUGE(varshort)) THEN
!/T            WRITE(NDSE,*) trim(outvarstr(ivarout)),' is out of bound : ', OUTVARVAL(i,j,ivarout)
!/T            OUTVARVAL(i,j,ivarout)=outfillval(ivarout)
!/T          END IF
!/T        END IF
      END DO
    END DO
  END DO


  !
  ! write variable in output file along sat track
  !
  WRITE(NDSO,'(/A)') 'Writing data in track file...'
  WRITE(NDSO,'(A)') 'latitude'
  iret=nf90_inq_varid(ncido, 'latitude', varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_put_var(ncido,varidtmp,outlatval(:))
  CALL CHECK_ERROR(IRET,__LINE__)
  WRITE(NDSO,'(A)') 'longitude'
  iret=nf90_inq_varid(ncido, 'longitude', varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_put_var(ncido,varidtmp,outlonval(:))
  CALL CHECK_ERROR(IRET,__LINE__)
!
  DO ivar=3,nvarout ! skip latitude, longitude
    WRITE(NDSO,'(A)') trim(outvarstr(ivar))
    iret=nf90_inq_varid(ncido, trim(outvarstr(ivar)), varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    if(outvartype(ivar).EQ.NF90_SHORT)  iret=nf90_put_var(ncido,varidtmp,NINT(outvarval(:,:,ivar),2)) !,start=(/1,1/),count=(/NLON,NLAT/))
    if(outvartype(ivar).EQ.NF90_INT)    iret=nf90_put_var(ncido,varidtmp,NINT(outvarval(:,:,ivar))) !,start=(/1,1/),count=(/NLON,NLAT/))
    if(outvartype(ivar).EQ.NF90_FLOAT)  iret=nf90_put_var(ncido,varidtmp,REAL(outvarval(:,:,ivar))) !,start=(/1,1/),count=(/NLON,NLAT/))
    if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_var(ncido,varidtmp,DBLE(outvarval(:,:,ivar))) !,start=(/1,1/),count=(/NLON,NLAT/))
    CALL CHECK_ERROR(IRET,__LINE__)
  END DO

  iret=nf90_close(ncido)
  CALL CHECK_ERROR(IRET,__LINE__)


!==========================================================
!
! 8. Deallocate arrays
!
!==========================================================

  ! deallocate attributes arrays
  deallocate(trkvartype)
  deallocate(trklongname)
  deallocate(trkstdname)
  deallocate(trkoffset)
  deallocate(trkscalefac)
  deallocate(trkunits)
  deallocate(trkfillval)
  deallocate(trkvalidmin)
  deallocate(trkvalidmax)

  DEALLOCATE(TRKJULDAY)
  DEALLOCATE(TRKVARVAL)

  DEALLOCATE(OUTVARVAL)
  DEALLOCATE(TMPSCATMOD)
  DEALLOCATE(TMPSCATOBS)

  DEALLOCATE(outvartype)
  DEALLOCATE(outoffset)
  DEALLOCATE(outscalefac)
  DEALLOCATE(outunits)
  DEALLOCATE(outlongname)
  DEALLOCATE(outstdname)
  DEALLOCATE(outvalidmin)
  DEALLOCATE(outvalidmax)
  DEALLOCATE(outfillval)
  DEALLOCATE(outvarstr)





  GOTO 888
!
! Error escape locations
!
 801 CONTINUE
      WRITE (NDSE,1001)
      CALL EXTCDE ( 61 )
!
 802 CONTINUE
      WRITE (NDSE,1002) IERR
      CALL EXTCDE ( 62 )
!
  804 CONTINUE
      WRITE (NDSE,1004)
      CALL EXTCDE ( 44 )
!
  805 CONTINUE
      WRITE (NDSE,1005) IERR
      CALL EXTCDE ( 45 )
!
 809 CONTINUE
      WRITE (NDSE,1009) IERR
      CALL EXTCDE ( 69 )
!

  888 CONTINUE
  WRITE (NDSO,999)

!
! Formats
!
!
  999 FORMAT(//'  End of program '/                                   &
               ' ========================================='/          &
               '         WAVEWATCH III Stat'/)
!
 1001 FORMAT (/' *** WAVEWATCH-III ERROR IN W3STAT : '/              &
               '     PREMATURE END OF INPUT FILE'/)
!
 1002 FORMAT (/' *** WAVEWATCH III ERROR IN W3STAT : '/               &
               '     ERROR IN READING ',A,' FROM INPUT FILE'/         &
               '     IOSTAT =',I5/)
!
 1004 FORMAT (/' *** WAVEWATCH III ERROR IN W3STAT : '/               &
               '     PREMATURE END OF TIME ATTRIBUTE '/               &
               '     FROM INPUT DATA FILE'/)
!
 1005 FORMAT (/' *** WAVEWATCH III ERROR IN W3STAT : '/               &
               '     ERROR IN READING OF TIME ATTRIBUTE '/            &
               '     FROM INPUT DATA FILE'/                           &
               '     IOSTAT =',I5/)
!
 1009 FORMAT (/' *** WAVEWATCH III ERROR IN W3STAT : '/              &
               '     ERROR IN OPENING TRK FILE'/                     &
               '     IOSTAT =',I5/)
!
 1028 FORMAT (/' *** WAVEWATCH III WARNING IN W3STAT : '/             &
               '     CALENDAR ATTRIBUTE NOT DEFINED'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')
 1029 FORMAT (/' *** WAVEWATCH III ERROR IN W3STAT : '/             &
               '     CALENDAR ATTRIBUTE NOT MATCH'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')


END PROGRAM W3STAT

!/ ------------------------------------------------------------------- /


SUBROUTINE W3CRNC (ncfile, NCTYPE, NORM, dimln, dimid, varid, ncid)

  USE NETCDF

  implicit none


  CHARACTER, INTENT(IN)             :: ncfile*(*), NORM*(*)
  INTEGER, INTENT(IN)               :: NCTYPE, dimln(2)
  INTEGER, INTENT(OUT)              :: dimid(2),varid(4),ncid
  INTEGER                           :: iret, deflate=1

  ! Creation in netCDF3 or netCDF4
  IF(NCTYPE.EQ.3)  iret = nf90_create(ncfile, NF90_CLOBBER, ncid)
  IF(NCTYPE.EQ.4)  iret = nf90_create(ncfile, NF90_NETCDF4, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)


  ! Define dimensions
  iret = nf90_def_dim(ncid, 'longitude', dimln(1), dimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)
  iret = nf90_def_dim(ncid, 'latitude', dimln(2), dimid(2))
  CALL CHECK_ERROR(IRET,__LINE__)


  ! Define variables

  !  longitude
  iret=nf90_def_var(ncid, 'longitude', NF90_DOUBLE, dimid(1), varid(1))
  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(1), 1, 1, deflate)
  iret=nf90_put_att(ncid,varid(1),'units','degrees_east')
  iret=nf90_put_att(ncid,varid(1),'long_name','longitude')
  iret=nf90_put_att(ncid,varid(1),'standard_name','longitude')
  iret=nf90_put_att(ncid,varid(1),'valid_min',-180.0)
  iret=nf90_put_att(ncid,varid(1),'valid_max',180.)
  iret=nf90_put_att(ncid,varid(1),'axis','X')

  !  latitude
  iret=nf90_def_var(ncid, 'latitude', NF90_DOUBLE, dimid(2), varid(2))
  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(2), 1, 1, deflate)
  iret=nf90_put_att(ncid,varid(2),'units','degrees_north')
  iret=nf90_put_att(ncid,varid(2),'long_name','latitude')
  iret=nf90_put_att(ncid,varid(2),'standard_name','latitude')
  iret=nf90_put_att(ncid,varid(2),'valid_min',-90.0)
  iret=nf90_put_att(ncid,varid(2),'valid_max',90.)
  iret=nf90_put_att(ncid,varid(2),'axis','Y')

  ! global attributes
  IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'NORM',TRIM(NORM))
  
  IF (INDEX(NORM, "OBS").NE.0) THEN
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'p98','var[round(0.98*count(var_sorted))]')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'mean','sum(var)/count(var)')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'scat','var-mean(var)')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'bias','sum(mod-obs)/count')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'nbias','(sum(mod-obs)/sum(obs)) * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'rms','sqrt{ sum[(var)**2]/count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'rmse','sqrt{ sum[(mod-obs)**2]/count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'nrmse','sqrt{ sum[(mod-obs)**2] / sum[obs**2] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'hh','sqrt{ sum[(mod-obs)**2] / sum[mod*obs] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'sdd','sqrt{ sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'si','sqrt{ sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / sum[obs**2] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'cor','sum[ (mod-mean(mod)) * (obs-mean(obs)) ] / (sqrt{ sum[(mod-mean(mod))**2]}*sqrt{ sum[(obs-mean(obs))**2]} }) * 100')
  ELSE IF (INDEX(NORM, "MOD").NE.0) THEN
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'p98','var[round(0.98*count(var_sorted))]')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'mean','sum(var)/count(var)')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'scat','var-mean(var)')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'bias','sum(obs-mod)/count')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'nbias','(sum(obs-mod)/sum(mod)) * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'rms','sqrt{ sum[(var)**2]/count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'rmse','sqrt{ sum[(mod-obs)**2]/count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'nrmse','sqrt{ sum[(mod-obs)**2] / sum[mod**2] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'hh','sqrt{ sum[(mod-obs)**2] / sum[mod*obs] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'sdd','sqrt{ sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / count }')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'si','sqrt{ sum[ ( (mod-mean(mod)) - (obs-mean(obs)) )**2] / sum[mod**2] } * 100')
    IRET=NF90_PUT_ATT(NCID,NF90_GLOBAL,'cor','sum[ (mod-mean(mod)) * (obs-mean(obs)) ] / (sqrt{ sum[(mod-mean(mod))**2]}*sqrt{ sum[(obs-mean(obs))**2]} }) * 100')
  END IF

  CALL CHECK_ERROR(IRET,__LINE__)

 
  IRET=NF90_ENDDEF(NCID)
  CALL CHECK_ERROR(IRET,__LINE__)
 

  RETURN


END SUBROUTINE W3CRNC

!/ ------------------------------------------------------------------- /

  SUBROUTINE CHECK_ERROR(IRET, ILINE)

  USE NETCDF
  USE W3SERVMD, ONLY: EXTCDE

  IMPLICIT NONE

  INTEGER IRET, ILINE, NDSE


  NDSE=6

  IF (IRET .NE. NF90_NOERR) THEN
    WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN WW3_STAT :'
    WRITE(NDSE,*) ' LINE NUMBER ', ILINE
    WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
    WRITE(NDSE,*) NF90_STRERROR(IRET)
    CALL EXTCDE ( 59 )
  END IF
  RETURN

  END SUBROUTINE CHECK_ERROR

!/ ------------------------------------------------------------------- /


!*****************************************************
!* Sorts an array ARR of length N in ascending order *
!*            by the Shell-Mezgar method             *
!* ------------------------------------------------- *
!* INPUTS:                                           *
!*	    N	  size of table ARR                  *
!*          ARR	  table to be sorted                 *
!*          IND	  index to be sorted                 *
!* OUTPUT:                                           *
!*	    ARR   table sorted in ascending order    *
!*	    IND   index sorted in ascending order    *
!*                                                   *
!* NOTE: The Shell method is a N^3/2 routine and can *
!*       be used for relatively large arrays.        *
!*****************************************************
SUBROUTINE SORT(N,ARR,IND)

  INTEGER, INTENT(IN)               :: N
  DOUBLE PRECISION, INTENT(INOUT)   :: ARR(N)
  INTEGER, INTENT(INOUT)            :: IND(N)
!
  DOUBLE PRECISION, PARAMETER       :: ALN2I=1./0.69314718,TINY=1.E-5
  DOUBLE PRECISION                  :: tmpval,LOGNB2
  INTEGER                           :: i,j,k,l,m,nn,tmpind

  LOGNB2=INT(ALOG(FLOAT(N))*ALN2I+TINY)
  m=n
  do nn=1,LOGNB2
    m=m/2; k=n-m
    do j=1,k
      i=j
      do while (i.GE.1)
      l=i+m
      if(ARR(l).GE.ARR(i)) then
        EXIT 
      else
        tmpval=ARR(i)
        tmpind=IND(i)
        ARR(i)=ARR(l)
        IND(i)=IND(l)
        ARR(l)=tmpval
        IND(l)=tmpind
        i=i-m
        end if
      end do
    end do
  end do
  return
END SUBROUTINE SORT

!/ ------------------------------------------------------------------- /













