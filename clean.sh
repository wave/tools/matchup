#!/usr/bin/sh

curdir=$(pwd)

rm -f $curdir/*.mod
rm -f $curdir/*.o
rm -f $curdir/*__*.f90
rm -f $curdir/*~

