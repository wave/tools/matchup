!/ ------------------------------------------------------------------- /
      MODULE W3SERVMD
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         01-Mar-2018 |
!/                  +-----------------------------------+
!/
!/    For update log see individual subroutines.
!/    12-Jun-2012 : Add /RTD option or rotated grid option. 
!/                  (Jian-Guo Li)                       ( version 4.06 )
!/    11-Nov-2013 : SMC and rotated grid incorporated in the main 
!/                  trunk                               ( version 4.13 )
!/    18-Aug-2016 : Add dist_sphere: angular distance   ( version 5.11 )
!/    01-Mar-2016 : Added W3THRTN and W3XYRTN for post  ( version 6.02 )
!/                  processing rotated grid data
!/
!/    Copyright 2009-2012 National Weather Service (NWS),
!/       National Oceanic and Atmospheric Administration.  All rights
!/       reserved.  WAVEWATCH III is a trademark of the NWS. 
!/       No unauthorized use without permission.
!/
!  1. Purpose :
!
!     In this module all WAVEWATCH specific service routines have
!     been gathered.
!
!  2. Variables and types :
!
!      Name      Type  Scope    Description
!     ----------------------------------------------------------------
!      NDSTRC    Int.  Private  Data set number for output of STRACE
!                               (set in ITRACE).
!      NTRACE    Int.  Private  Maximum number of trace prints in
!                               strace (set in ITRACE).
!     ----------------------------------------------------------------
!
!  3. Subroutines and functions :
!
!      Name      Type  Scope    Description
!     ----------------------------------------------------------------
!      ITRACE    Subr. Public   (Re-) Initialization for STRACE.
!      STRACE    Subr. Public   Enable subroutine tracing, usually
!                               activated with the !/S switch.   
!      NEXTLN    Subr. Public   Get to next line in input command file.
!      W3S2XY    Subr. Public   Grid conversion routine.
!      EJ5P      R.F.  Public   Five parameter JONSWAP spectrum.
!      WWDATE    Subr. Public   Get system date.
!      WWTIME    Subr. Public   Get system time.
!      EXTCDE    Subr. Public   Abort program with exit code.
!     Four subs for rotated grid are appended to this module.  As they 
!     are shared with SMC grid, they are not quoted by option /RTD but 
!     are available for general use.     JGLi12Jun2012
!     W3SPECTN       turns wave spectrum anti-clockwise by AnglD 
!     W3ACTURN       turns wave action(k,nth) anti-clockwise by AnglD.
!     W3LLTOEQ       convert standard into rotated lat/lon, plus AnglD
!     W3EQTOLL       revers of the LLTOEQ, but AnglD unchanged.
!     W3THTRN        turns direction value anti-clockwise by AnglD
!     W3XYTRN        turns 2D vectors anti-clockwise by AnglD
!
!     ----------------------------------------------------------------
!
!  4. Subroutines and functions used :
!
!     None.
!
!  5. Remarks :
!
!  6. Switches
!
!       !/S    Enable subroutine tracing using STRACE in this module.
!
!       !/F90  FORTRAN 90 specific switches.
!
!  7. Source code :
!
!/ ------------------------------------------------------------------- /
      PUBLIC
!
  INTEGER, PRIVATE        :: NDSTRC = 6, NTRACE = 0
!
CONTAINS
!/ ------------------------------------------------------------------- /
SUBROUTINE NEXTLN ( CHCKC , NDSI , NDSE )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         10-Dec-2014 |
!/                  +-----------------------------------+
!/
!/    15-Jan-1999 : Final FORTRAN 77                    ( version 1.18 )
!/    18-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/    10-Dec-2014 : Skip blank lines and leading blanks ( version 5.04 )
!/
!  1. Purpose :
!
!     Sets file pointer to next active line of input file, by skipping
!     blank lines and lines starting with the character CHCKC. Leading
!     white space is allowed before the character CHCKC.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       CHCKC   C*1   I  Check character for defining comment line.
!       NDSI    Int.  I  Input dataset number.
!       NDSE    Int.  I  Error output dataset number.
!                        (No output if NDSE < 0).
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!       STRACE ( !/S switch )
!
!  5. Called by :
!
!       Any routine.
!
!  6. Error messages :
!
!     - On EOF or error in input file.
!
!  9. Switches :
!
!     !/S  Enable subroutine tracing.
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
      IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: NDSI, NDSE
      CHARACTER, INTENT(IN)   :: CHCKC*1
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
!/S      INTEGER, SAVE           :: IENT = 0
      INTEGER                 :: IERR
      CHARACTER(128)          :: MSG
      CHARACTER(256)          :: LINE, TEST
!/
!/ ------------------------------------------------------------------- /
!/
!/S      CALL STRACE (IENT, 'NEXTLN')
!
  100 CONTINUE
      ! read line
      READ ( NDSI, 900, END=800, ERR=801, IOSTAT=IERR, IOMSG=MSG ) LINE
      ! leading blanks removed and placed on the right
      TEST = ADJUSTL ( LINE )
      IF ( TEST(1:1).EQ.CHCKC .OR. LEN_TRIM(TEST).EQ.0 ) THEN
        ! if comment or blank line, then skip
          GOTO 100
        ELSE
        ! otherwise, backup to beginning of line
          BACKSPACE ( NDSI, ERR=802, IOSTAT=IERR, IOMSG=MSG )
        ENDIF
      RETURN
!
  800 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,910)
      CALL EXTCDE ( 1 )
!
  801 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,911) IERR, TRIM(MSG)
      CALL EXTCDE ( 2 )
!
  802 CONTINUE
      IF ( NDSE .GE. 0 ) WRITE (NDSE,912) IERR, TRIM(MSG)
      CALL EXTCDE ( 3 )
!
! Formats
!
  900 FORMAT (A)
  910 FORMAT (/' *** WAVEWATCH III ERROR IN NEXTLN : '/         &
               '     PREMATURE END OF INPUT FILE'/)
  911 FORMAT (/' *** WAVEWATCH III ERROR IN NEXTLN : '/         &
               '     ERROR IN READING FROM FILE'/               &
               '     IOSTAT =',I5,/                             &
               '     IOMSG = ',A/)
  912 FORMAT (/' *** WAVEWATCH III ERROR IN NEXTLN : '/         &
               '     ERROR ON BACKSPACE'/                       &
               '     IOSTAT =',I5,/                             &
               '     IOMSG = ',A/)
!/
!/ End of NEXTLN ----------------------------------------------------- /
!/
      END SUBROUTINE NEXTLN
!/ ------------------------------------------------------------------- /
      SUBROUTINE EXTCDE ( IEXIT, UNIT, MSG, FILE, LINE, COMM )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         06-Jun-2018 |
!/                  +-----------------------------------+
!/
!/    06-Jan-1998 : Final FORTRAN 77                    ( version 1.18 )
!/    23-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/    10-Dec-2014 : Add checks for allocate status      ( version 5.04 )
!/    11-Mar-2015 : Allow non-error exit (iexit=0)      ( version 5.04 )
!/    20-Jan-2017 : Add optional MPI communicator arg   ( version 6.02 )
!/    06-Jun-2018 : Add optional MPI                    ( version 6.04 )
!/
!  1. Purpose :
!
!     Perform a program stop with an exit code.
!
!     If exit code IEXIT=0, then it is not an error, but 
!     a stop has been requested by the calling routine:
!     wait for other processes in communicator to catch up.
!     
!     If exit code IEXIT.ne.0, then abort program w/out
!     waiting for other processes to catch up (important for example
!     when not all processes are used by WW3).
!
!  2. Method :
!
!     Machine dependent.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IEXIT   Int.   I   Exit code to be used.
!       UNIT    Int.   I   (optional) file unit to write error message
!       MSG     Str.   I   (optional) error message
!       FILE    Str.   I   (optional) name of source code file
!       LINE    Int.   I   (optional) line number in source code file
!       COMM    Int.   I   (optional) MPI communicator
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!  5. Called by :
!
!     Any.
!
!  9. Switches :
!
!     !/MPI  MPI finalize interface if active
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
      IMPLICIT NONE
!
!/MPI      INCLUDE "mpif.h"
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN) :: IEXIT
      INTEGER,      INTENT(IN), OPTIONAL :: UNIT
      CHARACTER(*), INTENT(IN), OPTIONAL :: MSG
      CHARACTER(*), INTENT(IN), OPTIONAL :: FILE
      INTEGER,      INTENT(IN), OPTIONAL :: LINE
      INTEGER,      INTENT(IN), OPTIONAL :: COMM
!/
!/ ------------------------------------------------------------------- /
!/
!/MPI      INTEGER                 :: IERR_MPI
!/MPI      LOGICAL                 :: RUN
      INTEGER                 :: IUN
      CHARACTER(256)          :: LMSG = ""
      CHARACTER(6)            :: LSTR
      CHARACTER(10)           :: PREFIX = "WW3 ERROR:"
!/
!/ Set file unit for error output
!/
      IUN = 0
      IF (PRESENT(UNIT)) IUN = UNIT
!/
!/ Report error message
!/
      IF (PRESENT(MSG)) THEN
          WRITE (IUN,"(A)") PREFIX//" "//TRIM(MSG)
        END IF
!/
!/ Report context
!/
      IF ( PRESENT(FILE) ) THEN
          LMSG = TRIM(LMSG)//" FILE="//TRIM(FILE)
        END IF
      IF ( PRESENT(LINE) ) THEN
          WRITE (LSTR,'(I0)') LINE
          LMSG = TRIM(LMSG)//" LINE="//TRIM(LSTR)
        END IF
      IF ( LEN_TRIM(LMSG).GT.0 ) THEN
          WRITE (IUN,"(A)") PREFIX//TRIM(LMSG)
        END IF
!/
!/ Handle MPI exit
!/
!/MPI      CALL MPI_INITIALIZED ( RUN, IERR_MPI )
!/MPI      IF ( RUN ) THEN
!/MPI        IF ( IEXIT.EQ.0 ) THEN ! non-error state
!/MPI          IF ( PRESENT(COMM) ) CALL MPI_BARRIER ( COMM, IERR_MPI )
!/MPI          CALL MPI_FINALIZE (IERR_MPI )
!/MPI        ELSE ! error state
!/MPI          WRITE(*,'(/A,I6/)') 'EXTCDE MPI_ABORT, IEXIT=', IEXIT
!/MPI          IF (PRESENT(UNIT)) THEN
!/MPI            WRITE(*,'(/A,I6/)') 'EXTCDE UNIT=', UNIT
!!/MPI          ELSE
!!/MPI            WRITE(*,'(A)') 'EXTCDE UNIT missing'
!/MPI          END IF
!/MPI          IF (PRESENT(MSG)) THEN
!/MPI            WRITE(*,'(/2A/)') 'EXTCDE MSG=', MSG
!!/MPI          ELSE
!!/MPI            WRITE(*,'(A)') 'EXTCDE MSG missing'
!/MPI          END IF
!/MPI          IF (PRESENT(FILE)) THEN
!/MPI            WRITE(*,'(/2A/)') 'EXTCDE FILE=', FILE
!!/MPI          ELSE
!!/MPI            WRITE(*,'(A)') 'EXTCDE FILE missing'
!/MPI          END IF
!/MPI          IF (PRESENT(LINE)) THEN
!/MPI            WRITE(*,'(/A,I8/)') 'EXTCDE LINE=', LINE
!!/MPI          ELSE
!!/MPI            WRITE(*,'(A)') 'EXTCDE LINE missing'
!/MPI          END IF
!/MPI          IF (PRESENT(COMM)) THEN
!/MPI            WRITE(*,'(/A,I6/)') 'EXTCDE COMM=', COMM
!!/MPI          ELSE
!!/MPI            WRITE(*,'(A)') 'EXTCDE COMM missing'
!/MPI          END IF
!/MPI          CALL MPI_ABORT ( MPI_COMM_WORLD, IEXIT, IERR_MPI )
!/MPI        END IF
!/MPI      END IF
!/
!/ Handle non-MPI exit
!/
      CALL EXIT ( IEXIT )
!/
!/ End of EXTCDE ----------------------------------------------------- /
!/
      END SUBROUTINE EXTCDE
!
!/ ------------------------------------------------------------------- /
!/ ------------------------------------------------------------------- /
!/
      SUBROUTINE STRSPLIT(STRING,TAB,CNT)
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |          M. Accensi               |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         29-Apr-2013 !
!/                  +-----------------------------------+
!/
!/    29-Mar-2013 : Origination.                        ( version 4.10 )
!/
!  1. Purpose :
!
!     Splits string into words
!
!  2. Method :
!
!     finds spaces and loops
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       STRING   Str   I    String to be splitted
!       TAB      Str   IO   Array of string
!       CNT      Int   O    Count of string
!     ----------------------------------------------------------------
!

      IMPLICIT NONE



      CHARACTER(LEN=*), intent(IN)         :: STRING
      CHARACTER(LEN=128), intent(INOUT)    :: TAB(*)
      INTEGER, intent(OUT), optional       :: CNT
      INTEGER                              :: I
      CHARACTER(LEN=1024)                  :: tmp_str, ori_str

! initializes arrays
      ori_str=ADJUSTL(TRIM(STRING))
      tmp_str=ori_str
      cnt=0

! counts the number of substrings
      DO WHILE ((INDEX(tmp_str,' ').NE.0) .AND. (len_trim(tmp_str).NE.0))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        cnt=cnt+1
        ENDDO
!
! reinitializes arrays
!
      tmp_str=ori_str
! loops on each substring
      DO I=1,cnt
        TAB(I)=tmp_str(:INDEX(tmp_str,' '))
        tmp_str=ADJUSTL(tmp_str(INDEX(tmp_str,' ')+1:))
        END DO

      RETURN
!/
!/ End of STRSPLIT ----------------------------------------------------- /
!/
      END SUBROUTINE STRSPLIT
!/

!/ ------------------------------------------------------------------- /
END MODULE W3SERVMD
