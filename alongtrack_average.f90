!/ ------------------------------------------------------------------- /
      PROGRAM ALONGTRACK_AVERAGE
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |     M. Accensi                    |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         13-Mar-2020 |
!/                  +-----------------------------------+
!/
!
!/    13-Mar-2020 : Creation
!
!  1. Purpose :
!
!     Average all variables alongtrack per 1 degree latitude
!
!  2. Method :
!
!/ ------------------------------------------------------------------- /
  USE NETCDF
!
  IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
  INTEGER                          :: iret, ncid, ncido, nctype, ntime
  INTEGER                          :: NDSI, NDSE, NDSO, NDSL, NDSM, IERR
  INTEGER                          :: i, ivar, varidtime, varidlat, varidlon, minlat
  INTEGER                          :: nglobalatt, unlimdimid, nvarobs, ndimobs, ivarout, nvarout
  INTEGER                          :: varidtmp, timefirst, timelast
  INTEGER                          :: ITIMEOBS, startout
  INTEGER                          :: itime, validtimes
  INTEGER                          :: outdimid(1)
  INTEGER                          :: DATE(8)

  INTEGER, ALLOCATABLE             :: varid(:), outvarid(:)
  INTEGER, ALLOCATABLE             :: obsvartype(:), outvartype(:)

  REAL                             :: res, diff

  REAL, ALLOCATABLE                :: outscalefac(:)
  REAL, ALLOCATABLE                :: obsoffset(:), obsscalefac(:), outoffset(:)
  REAL, ALLOCATABLE                :: obsvalidmin(:), obsvalidmax(:)
  REAL, ALLOCATABLE                :: outvalidmin(:), outvalidmax(:)

  DOUBLE PRECISION                 :: MEAN

  DOUBLE PRECISION, ALLOCATABLE    :: timevarval(:), latvarval(:), obsvarval(:,:)
  DOUBLE PRECISION, ALLOCATABLE    :: obsfillval(:),  outfillval(:)

  CHARACTER*1024                   :: INFILE, OUTFILE
  CHARACTER*50                     :: calendar
  CHARACTER*1024                   :: arg, cmd

  CHARACTER*128, ALLOCATABLE       :: outunits(:), outlongname(:), outstdname(:) 
  CHARACTER*128, ALLOCATABLE       :: obsunits(:), obslongname(:), obsstdname(:)
  CHARACTER*128, ALLOCATABLE       :: obsvarstr(:), outvarstr(:)


  LOGICAL                          :: filefound


!/
!/ ------------------------------------------------------------------- /
!/


!==========================================================
! 1. Process input
!==========================================================


!---------------------------------------------------------------
! 1.a IO set-up
!---------------------------------------------------------------

  NDSI=10
  NDSL=12
  NDSM=13
  NDSE=6
  NDSO=6

!---------------------------------------------------------------
! 1.b Read namelist
!---------------------------------------------------------------

  RES=1.
  NCTYPE=4
  CALL get_command_argument(1,arg)
  INFILE=trim(arg)
!  INFILE='/scratch/tmp/GLOBAL_2018/SAT/saral/test.nc'
  WRITE(OUTFILE,'(2A)')TRIM(INFILE),'_1d'


!==========================================================
!
! 2. Read the input file
!
!==========================================================

  WRITE(NDSO,'(/A)') 'Open input file : '
  WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(INFILE)
  iret = nf90_open(INFILE, NF90_NOWRITE, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)


  ! get the obs reference time
  DATE(:)=0
  iret=nf90_inq_varid(ncid,'time', varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_att(ncid,varidtmp,"calendar",calendar)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(NDSE,1028)
  ELSE IF ((INDEX(calendar, "standard").EQ.0) .AND. &
           (INDEX(calendar, "gregorian").EQ.0)) THEN
    WRITE(NDSE,1029)
    STOP
  END IF

  iret=nf90_inquire(ncid, ndimobs, nvarobs, nglobalatt, unlimdimid)
  CALL CHECK_ERROR(IRET,__LINE__)
  allocate(varid(nvarobs))
  allocate(outvarid(nvarobs))
  iret=nf90_inq_varids(ncid,nvarobs,varid)
  CALL CHECK_ERROR(IRET,__LINE__)

  write(*,*) 'ndim : ', ndimobs
  write(*,*) 'nvar : ', nvarobs

  ! allocate attributes arrays
  allocate(obsvarstr(nvarobs))
  allocate(obsvartype(nvarobs))
  allocate(obslongname(nvarobs))
  allocate(obsstdname(nvarobs))
  allocate(obsoffset(nvarobs))
  allocate(obsscalefac(nvarobs))
  allocate(obsunits(nvarobs))
  allocate(obsfillval(nvarobs))
  allocate(obsvalidmin(nvarobs))
  allocate(obsvalidmax(nvarobs))

  ! output index
  nvarout=nvarobs
  ivarout=0

  ! Allocate attributes arrays
  allocate(outvarstr(nvarout))
  ALLOCATE(outvartype(nvarout))
  ALLOCATE(outoffset(nvarout))
  ALLOCATE(outscalefac(nvarout))
  ALLOCATE(outunits(nvarout))
  ALLOCATE(outlongname(nvarout))
  ALLOCATE(outstdname(nvarout))
  ALLOCATE(outvalidmin(nvarout))
  ALLOCATE(outvalidmax(nvarout))
  ALLOCATE(outfillval(nvarout))



  DO ivar=1,nvarobs

    ! inq variable name
    iret=nf90_inquire_variable(ncid, varid(ivar), obsvarstr(ivar),xtype=obsvartype(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    write(NDSO,'(2A)') 'variable : ', trim(obsvarstr(ivar))


    ! save time lat lon varid
    if ((index(trim(obsvarstr(ivar)),'time').NE.0)  .OR. &
        (index(trim(obsvarstr(ivar)),'TIME').NE.0)) THEN
      varidtime=varid(ivar)
    END IF
    if ((index(trim(obsvarstr(ivar)),'lat').NE.0)       .OR. &
        (index(trim(obsvarstr(ivar)),'latitude').NE.0)  .OR. &
        (index(trim(obsvarstr(ivar)),'LATITUDE').NE.0)) THEN
      varidlat=varid(ivar)
    END IF
    if ((index(trim(obsvarstr(ivar)),'lon').NE.0)        .OR. &
        (index(trim(obsvarstr(ivar)),'longitude').NE.0)  .OR. &
        (index(trim(obsvarstr(ivar)),'LONGITUDE').NE.0)) THEN
      varidlon=varid(ivar)
    END IF

    ! skip time variable
    !if (index(trim(obsvarstr(ivar)),'time').NE.0) THEN
    !  WRITE(NDSO,'(A)') 'skip time variable'
    !  CYCLE
    !END IF

    ! process attributes 
    iret=nf90_get_att(ncid,varid(ivar),'add_offset',obsoffset(ivar))
    if (iret.NE.0) obsoffset(ivar)=0.0
    iret=nf90_get_att(ncid,varid(ivar),'scale_factor',obsscalefac(ivar))
    if (iret.NE.0) obsscalefac(ivar)=1.0
    iret=nf90_get_att(ncid,varid(ivar),'_FillValue',obsfillval(ivar))
    if (iret.NE.0) THEN
      if(obsvartype(ivar).EQ.NF90_SHORT) obsfillval(ivar)=NF90_FILL_SHORT
      if(obsvartype(ivar).EQ.NF90_INT) obsfillval(ivar)=NF90_FILL_INT
      if(obsvartype(ivar).EQ.NF90_FLOAT) obsfillval(ivar)=NF90_FILL_FLOAT
      if(obsvartype(ivar).EQ.NF90_DOUBLE) obsfillval(ivar)=NF90_FILL_DOUBLE
    END IF
    iret=nf90_get_att(ncid,varid(ivar),'units',obsunits(ivar))
    if (iret.NE.0) obsunits(ivar)='unknown'
    iret=nf90_get_att(ncid,varid(ivar),'long_name',obslongname(ivar))
    if (iret.NE.0) obslongname(ivar)=obsvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'standard_name',obsstdname(ivar))
    if (iret.NE.0) obsstdname(ivar)=obsvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'valid_min',obsvalidmin(ivar))
    if (iret.NE.0) obsvalidmin(ivar)=-1000
    iret=nf90_get_att(ncid,varid(ivar),'valid_max',obsvalidmax(ivar))
    if (iret.NE.0) obsvalidmin(ivar)=1000


    ! increment output variable index
    ivarout=ivarout+1

    ! output attributes
    outvartype(ivarout)=obsvartype(ivar)
    outoffset(ivarout)=obsoffset(ivar)
    outscalefac(ivarout)=obsscalefac(ivar)
    outfillval(ivarout)=obsfillval(ivar)
    outunits(ivarout)=obsunits(ivar)
    outlongname(ivarout)=obslongname(ivar)
    outstdname(ivarout)=obsstdname(ivar)
    outvalidmin(ivarout)=obsvalidmin(ivar)
    outvalidmax(ivarout)=obsvalidmax(ivar)


    ! output var name and std/long name atts
    outvarstr(ivarout)=TRIM(obsvarstr(ivar))
    outlongname(ivarout)=obslongname(ivar)
    outstdname(ivarout)=obsstdname(ivar)

  END DO



!==========================================================
!
! 8. Create output file
!
!==========================================================

  ! Check if corresponding track file exists
  WRITE(6,'(/A)') "Defining output file :"
  WRITE(6,'(A)') trim(adjustl(adjustr(outfile)))
  inquire(file=trim(outfile), exist=filefound)
  IF(filefound) THEN
    WRITE (cmd, '("/bin/rm ", A)') trim(outfile)
    CALL system(cmd)
    WRITE(NDSO,'(3A)') '[WARNING] There was already an output file ', TRIM(OUTFILE), ': it has been deleted.'
  END IF


  ! create output file 
  write(NDSO,'(/A)') 'Creating output file : '
  WRITE(NDSO,'(A)') trim(outfile)

  ! Creation in netCDF3 or netCDF4
  IF(NCTYPE.EQ.3)  iret = nf90_create(outfile, NF90_CLOBBER, ncido)
  IF(NCTYPE.EQ.4)  iret = nf90_create(outfile, NF90_NETCDF4, ncido)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! Define dimensions
  iret = nf90_def_dim(ncido, 'time', NF90_UNLIMITED, outdimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)

  ! define the variables in track file (skip time)
  write(NDSO,'(/A)') 'Defining the output variables : '
  DO ivar=1,nvarout

    write(NDSO,'(A)') trim(outvarstr(ivar))

    ! use outvarstr to define the name of variables in track file
    iret=nf90_def_var(ncido, trim(outvarstr(ivar)), outvartype(ivar), outdimid(1), outvarid(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'units',outunits(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'long_name',outlongname(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'standard_name',outstdname(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'add_offset',outoffset(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'scale_factor',outscalefac(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
    if(outvartype(ivar).EQ.NF90_SHORT) iret=nf90_put_att(ncido,outvarid(ivar),'_FillValue',INT(outfillval(ivar),2))
    if(outvartype(ivar).EQ.NF90_INT) iret=nf90_put_att(ncido,outvarid(ivar),'_FillValue',INT(outfillval(ivar)))
    if(outvartype(ivar).EQ.NF90_FLOAT) iret=nf90_put_att(ncido,outvarid(ivar),'_FillValue',REAL(outfillval(ivar)))
    if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_att(ncido,outvarid(ivar),'_FillValue',DBLE(outfillval(ivar)))
    CALL CHECK_ERROR(IRET,__LINE__)
!    iret=nf90_put_att(ncido,outvarid(ivar),'valid_min',outvalidmin(ivar))
!    CALL CHECK_ERROR(IRET,__LINE__)
!    iret=nf90_put_att(ncido,outvarid(ivar),'valid_max',outvalidmax(ivar))
!    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_put_att(ncido,outvarid(ivar),'axis','T')
    CALL CHECK_ERROR(IRET,__LINE__)
  END DO

  iret=nf90_enddef(ncido)
  CALL CHECK_ERROR(IRET,__LINE__)



!==========================================================
!
! 10. Loop on time
!
!==========================================================

  ! get time dimension
  iret=nf90_inquire_dimension(ncid,outdimid(1),len=ntime)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! allocate variable
  ALLOCATE(timevarval(ntime))
  ALLOCATE(latvarval(ntime))
  ALLOCATE(obsvarval(ntime,nvarobs))

  ! get variable
  DO ivar=1,nvarobs
    WRITE(NDSO,*) 'get ', trim(obsvarstr(ivar))
    IF(varid(ivar).EQ.varidtime) THEN
      iret=nf90_get_var(ncid, varid(ivar), timevarval(1:ntime))
      CALL CHECK_ERROR(IRET,__LINE__)
    ELSE IF(varid(ivar).EQ.varidlat) THEN
      iret=nf90_get_var(ncid, varid(ivar), latvarval(1:ntime))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF
    iret=nf90_get_var(ncid, varid(ivar), obsvarval(1:ntime,ivar))
    CALL CHECK_ERROR(IRET,__LINE__)
  END DO

  minlat=int(latvarval(1))
!  write(NDSO,*) 'minlat : ', minlat
  timefirst=1

  ! loop on time
  startout=1
  DO ITIMEOBS=1,NTIME

!    WRITE(NDSO,*) 'time : ', timevarval(itimeobs)
!    WRITE(NDSO,*) 'lat : ', latvarval(itimeobs)

    diff=abs(int(latvarval(itimeobs))-minlat)
    IF(diff.GE.res) THEN
      timelast=itimeobs-1
      DO ivar=1,nvarobs
        validtimes=0
        mean=0
        DO itime=timefirst,timelast
          IF (obsvarval(itime,ivar).NE.obsfillval(ivar)) THEN
            mean=mean+obsvarval(itime,ivar)
            validtimes=validtimes+1
          END IF
        END DO
        mean=mean / validtimes
!        write(*,*) 'timefirst, timelast : ', timefirst, timelast
!        write(*,*) obsvarval(timefirst:timelast,ivar)
!        write(*,*) 'mean : ', mean
!        write(*,*) 'varid / outvarid : ', varid(ivar), outvarid(ivar)
!        write(*,*) 'varstr / outvarstr : ', trim(obsvarstr(ivar)), trim(outvarstr(ivar))
!        write(*,*) 'outvartype : ', outvartype(ivar)
        if(outvartype(ivar).EQ.NF90_BYTE)   iret=nf90_put_var(ncido,outvarid(ivar),NINT(mean,1),start=(/startout/))!,count=(/1/))
        if(outvartype(ivar).EQ.NF90_UBYTE)  iret=nf90_put_var(ncido,outvarid(ivar),NINT(mean,1),start=(/startout/))!,count=(/1/))
        if(outvartype(ivar).EQ.NF90_SHORT)  iret=nf90_put_var(ncido,outvarid(ivar),NINT(mean,2),start=(/startout/))!,count=(/1/))
        if(outvartype(ivar).EQ.NF90_INT)    iret=nf90_put_var(ncido,outvarid(ivar),NINT(mean),start=(/startout/))!,count=(/1/))
        if(outvartype(ivar).EQ.NF90_FLOAT)  iret=nf90_put_var(ncido,outvarid(ivar),REAL(mean),start=(/startout/))!,count=(/1/))
        if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_var(ncido,outvarid(ivar),DBLE(mean),start=(/startout/))!,count=(/1/))
        CALL CHECK_ERROR(IRET,__LINE__)
      END DO
     startout=startout+1

!      WRITE(NDSO,*) latvarval(itimeobs), int(latvarval(itimeobs))
      minlat=int(latvarval(itimeobs))
      timefirst=itimeobs
    END IF

    


  END DO

!==========================================================
!
! 10. Finalize
!
!==========================================================

  ! close input file
  iret=nf90_close(ncid)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! close output file
  iret=nf90_close(ncido)
  CALL CHECK_ERROR(IRET,__LINE__)


  GOTO 888
!
! Error escape locations
!
 801 CONTINUE
      WRITE (NDSE,1001)
      CALL EXTCDE ( 61 )
!
 802 CONTINUE
      WRITE (NDSE,1002)
      CALL EXTCDE ( 62 )
!
  804 CONTINUE
      WRITE (NDSE,1004)
      CALL EXTCDE ( 44 )
!
  805 CONTINUE
      WRITE (NDSE,1005) IERR
      CALL EXTCDE ( 45 )
!
 809 CONTINUE
      WRITE (NDSE,1009) IERR
      CALL EXTCDE ( 69 )
!

  888 CONTINUE
  WRITE (NDSO,999)

!
! Formats
!
!
  999 FORMAT(//'  End of program '/                                   &
               ' ========================================='/          &
               '         WAVEWATCH III Matching '/)
!
 1001 FORMAT (/' *** WAVEWATCH-III ERROR IN W3MATCH : '/              &
               '     PREMATURE END OF INPUT FILE'/)
!
 1002 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     ERROR IN READING ',A,' FROM INPUT FILE'/         &
               '     IOSTAT =',I5/)
!
 1004 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     PREMATURE END OF TIME ATTRIBUTE '/               &
               '     FROM INPUT DATA FILE'/)
!
 1005 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     ERROR IN READING OF TIME ATTRIBUTE '/            &
               '     FROM INPUT DATA FILE'/                           &
               '     IOSTAT =',I5/)
!
 1009 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/              &
               '     ERROR IN OPENING OBS FILE'/                     &
               '     IOSTAT =',I5/)
!
 1028 FORMAT (/' *** WAVEWATCH III WARNING IN W3MATCH : '/             &
               '     CALENDAR ATTRIBUTE NOT DEFINED'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')
 1029 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/             &
               '     CALENDAR ATTRIBUTE NOT MATCH'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')




END PROGRAM ALONGTRACK_AVERAGE


!/ ------------------------------------------------------------------- /

  SUBROUTINE CHECK_ERROR(IRET, ILINE)

  USE NETCDF

  IMPLICIT NONE

  INTEGER IRET, ILINE, NDSE


  NDSE=6

  IF (IRET .NE. NF90_NOERR) THEN
    WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN ALONGTRACK_AVERAGE :'
    WRITE(NDSE,*) ' LINE NUMBER ', ILINE
    WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
    WRITE(NDSE,*) NF90_STRERROR(IRET)
    CALL EXTCDE ( 59 )
  END IF
  RETURN

  END SUBROUTINE CHECK_ERROR

!/ ------------------------------------------------------------------- /

SUBROUTINE EXTCDE ( IEXIT )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |           H. L. Tolman            |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         06-Jan-1999 |
!/                  +-----------------------------------+
!/
!/    06-Jan-1998 : Final FORTRAN 77                    ( version 1.18 )
!/    23-Nov-1999 : Upgrade to FORTRAN 90               ( version 2.00 )
!/
!  1. Purpose :
!
!     Perfor a program stop with an exit code.
!
!  2. Method :
!
!     Machine dependent.
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IEXIT   Int.   I   Exit code to be used.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!  5. Called by :
!
!     Any.
!
!  9. Switches :
!
!     !/MPI  MPI finalize interface if active
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
IMPLICIT NONE
!
!/MPI      INCLUDE "mpif.h"
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: IEXIT
!/
!/ ------------------------------------------------------------------- /
!/
!/MPI      INTEGER                 :: IERR_MPI
!/MPI      LOGICAL                 :: RUN
!/
!/ Test if MPI needs to be closed
!/
!/MPI      CALL MPI_INITIALIZED ( RUN, IERR_MPI )
!/MPI      IF ( RUN ) THEN
!/MPI          CALL MPI_BARRIER ( MPI_COMM_WORLD, IERR_MPI )
!/MPI          CALL MPI_FINALIZE (IERR_MPI )
!/MPI        END IF
!
      CALL EXIT ( IEXIT )
!/DUM      STOP

END SUBROUTINE EXTCDE

!/ ------------------------------------------------------------------- /
