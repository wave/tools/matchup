!/ ------------------------------------------------------------------- /

MODULE W3GDATMD

PUBLIC

   INTEGER,DIMENSION(:,:),ALLOCATABLE            :: TRIGP
   DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE   :: XYB
   INTEGER                                       :: NX
   INTEGER                                       :: NTRI, COUNTRI, COUNTOT, NNZ

   INTEGER, DIMENSION(:),ALLOCATABLE             :: JAA,IAA, CCON, &
                                                    INDEX_CELL, IE_CELL, POS_CELL
   INTEGER, DIMENSION(:,:),ALLOCATABLE           :: POSI
   REAL(8), DIMENSION(:), ALLOCATABLE            :: TRIA, SI
   REAL(8), DIMENSION(:,:), ALLOCATABLE          :: LLEN, IEN
   REAL                                          :: MAXX, MAXY, DXYMAX
   REAL                                          :: SX, SY, X0, Y0
   LOGICAL                                       :: GUGINIT
   LOGICAL                                       :: FSNIMP = .FALSE.

CONTAINS

!/ ------------------------------------------------------------------- /   
      SUBROUTINE W3DIMUG  ( IMOD, MTRI, MX, COUNTOTA, NNZ, NDSE, NDST )
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH-III           NOAA/NCEP |
!/                  |             F.ardhuin             |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         15-Mar-2007 !
!/                  +-----------------------------------+
!/
!/    15-Mar-2007 : Origination.                        ( version 3.14 )
!/    11-May-2015 : Updates to 2-ways nestings for UG   ( version 5.08 )
!/
!  1. Purpose :
!
!     Initialize an individual spatial grid at the proper dimensions.
!
!  2. Method :
!
!     Allocate directly into the structure array GRIDS. Note that
!     this cannot be done through the pointer alias!
!
!  3. Parameters :
!
!     Parameter list
!     ----------------------------------------------------------------
!       IMOD    Int.   I   Model number to point to.
!       NDSE    Int.   I   Error output unit number.
!       NDST    Int.   I   Test output unit number.
!       MX, MTRI, MSEA       Like NX, NTRI, NSEA in data structure.
!     ----------------------------------------------------------------
!
!  4. Subroutines used :
!
!       See module documentation.
!
!  5. Called by :
!
!      Name      Type  Module   Description
!     ----------------------------------------------------------------
!      W3IOGR    Subr. W3IOGRMD Model definition file IO program.
!      WW3_GRID  Prog.   N/A    Model set up program.
!     ----------------------------------------------------------------
!
!  6. Error messages :
!
!     - Check on input parameters.
!     - Check on previous allocation.
!
!  7. Remarks :
!
!     - Grid dimensions apre passed through parameter list and then 
!       locally stored to assure consistency between allocation and
!       data in structure.
!     - W3SETG needs to be called after allocation to point to 
!       proper allocated arrays.
!
!  8. Structure :
!
!     See source code.
!
!  9. Switches :
!
!     !/S    Enable subroutine tracing.
!
! 10. Source code :
!
!/ ------------------------------------------------------------------- /
      USE W3SERVMD, ONLY: EXTCDE
!/MEMCHECK      USE MallocInfo_m
!/S      USE W3SERVMD, ONLY: STRACE
!
      IMPLICIT NONE
!
!/
!/ ------------------------------------------------------------------- /
!/ Parameter list
!/
      INTEGER, INTENT(IN)     :: IMOD, MTRI, MX, COUNTOTA, NNZ, NDSE, NDST
!/MEMCHECK      type(MallInfo_t)        :: mallinfos
      INTEGER                 :: IAPROC = 1
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/
!/S      INTEGER, SAVE           :: IENT = 0
!/
!/S      CALL STRACE (IENT, 'W3DIMUG')
!
! -------------------------------------------------------------------- /

! -------------------------------------------------------------------- /
! 2.  Allocate arrays
!
      ALLOCATE ( TRIGP(MTRI,3),                                       &
                 XYB(MX,3),                                           &
                 SI(MX),                                              &
                 TRIA(MTRI),                                          &
                 IEN(MTRI,6),                                         &
                 LLEN(MTRI,3),                                        &
                 CCON(MX),                                            &
                 INDEX_CELL(MX+1),                                    &
                 IE_CELL(COUNTOTA),                                   &
                 POS_CELL(COUNTOTA),                                  &
                 IAA(NX+1),                                           &
                 JAA(NNZ),                                            &
                 POSI(3,COUNTOTA)                                     &
               )
! -------------------------------------------------------------------- /
! 4.  Update counters in grid
!     Note that in the case of lon/lat grids, these quantities do not
!     include the spherical coordinate metric (SPHERE=.FALSE.).
!
      NTRI   = MTRI
      COUNTOT=COUNTOTA
      GUGINIT  = .TRUE.

      RETURN

!/
!/ End of W3DIMUG ----------------------------------------------------- /
!/
      END SUBROUTINE W3DIMUG
!/ ------------------------------------------------------------------- /


END MODULE W3GDATMD

!/ ------------------------------------------------------------------- /
