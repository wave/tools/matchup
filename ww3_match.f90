!/ ------------------------------------------------------------------- /
      PROGRAM W3MATCH
!/
!/                  +-----------------------------------+
!/                  | WAVEWATCH III           NOAA/NCEP |
!/                  |     M. Accensi & F. Ardhuin       |
!/                  |                        FORTRAN 90 |
!/                  | Last update :         02-Sep-2021 |
!/                  +-----------------------------------+
!/
!
!/    16-Oct-2014 : Creation
!/    02-Nov-2014 : Debugged to work with gnu fortran
!/    29-Jul-2015 : Implement interpolation on unst and reg grid with ww3 routines
!/    06-Aug-2015 : add curvilinear interpolation
!/    18-Nov-2016 : Moved modules c_caldat and w3triamd out of file
!/    18-Nov-2016 : Added time interpolation 
!/    12-Dec-2018 : Use of namelist
!/    21-Feb-2019 : Fully rebuilt
!/    14-May-2020 : Add obs and mod flag operators
!/    15-Jun-2020 : Update modules with WW3 v7
!/    26-Jun-2020 : unpack obs coord variables if needed
!/    26-Jun-2020 : skip obs file with time dimension=0
!/    29-Oct-2020 : remove output file if no data
!/    30-Oct-2020 : skip 2nd dimension in obs file
!/    26-Nov-2020 : set MAPSTA as wet points if missing variable
!/    03-May-2021 : force to convert obs longitude range to mod longitude range
!/    02-Sep-2021 : add option to filter on number of land points as neighbors 
!
!  1. Purpose :
!
!     Gets the wave model output on observation tracks for comparisons
!
!  2. Method :
!
!/ ------------------------------------------------------------------- /
  USE W3SERVMD
  USE W3GDATMD
  USE W3TRIAMD
  USE W3GSRUMD
  USE W3TIMEMD
  USE W3NMLMATCHMD
  USE NETCDF
!
  IMPLICIT NONE
!/
!/ ------------------------------------------------------------------- /
!/ Local parameters
!/

!
! Parameters from GSRUMD
!
!/
!/ Public index closure types (for lat/lon grids only)
!/   ICLO_NONE : no closure in index space
!/   ICLO_SMPL : closure in i-index at i=NX+1: (NX+1,j) => (1,j)
!/   ICLO_TRPL : tripole grid closure: (NX+1,j<=NY) => (1,j) and
!/                                     (i,NY+1) => (MOD(NX-i+1,NX)+1,NY)
!/
!      INTEGER, PARAMETER :: ICLO_NONE = 0
!      INTEGER, PARAMETER :: ICLO_SMPL = 1 -> global
!      INTEGER, PARAMETER :: ICLO_TRPL = 2
!
! Parameters from GDATMD
!
      INTEGER, PARAMETER      :: RLGTYPE = 1
      INTEGER, PARAMETER      :: CLGTYPE = 2
      INTEGER, PARAMETER      :: UNGTYPE = 3   

      INTEGER, PARAMETER      :: ICLOSE_NONE = ICLO_NONE
      INTEGER, PARAMETER      :: ICLOSE_SMPL = ICLO_SMPL
      INTEGER, PARAMETER      :: ICLOSE_TRPL = ICLO_TRPL
!
  TYPE(T_GSU)                      :: GSI      ! Grid search utility object
  TYPE(NML_MATCH_T)                :: NML_MATCH

  INTEGER                          :: iret, ncid, ncido, nctype, varndims
  INTEGER                          :: NDSI, NDSE, NDSO, NDSL, NDSM, IERR
  INTEGER                          :: ivar, maxntimeobs, maxntimemod
  INTEGER                          :: nlatmod, nlonmod, nvarmod, nvarmodout, ivarmodout
  INTEGER                          :: nvarobs, nvarobsout, ivarobsout, nvarout, nflagobs, nflagmod
  INTEGER                          :: nnodemod, nelemmod, nnoelmod, nfilevarmod, nfilevarobs
  INTEGER                          :: i, varidtmp, IMOD, IOBS
  INTEGER                          :: GTYPE, GRIDDIMS, ICLO !, TIMESPLIT
  INTEGER                          :: ITOUT    ! Triangle index in unstructured grids
  INTEGER                          :: NOBSFILE, NMODFILE, TMOD1, TMOD2
  INTEGER                          :: ITIMEMOD, ITIMEOBS, ITIMEOUT, startout
  INTEGER                          :: LANDPOINTS, MAXLANDPOINTS, NS

  INTEGER                          :: dimln(4), dimid(4), varid(20)
  INTEGER                          :: TIMESTART(2), TIMESTOP(2)
  INTEGER                          :: DATE(8), STARTDATE(8), STOPDATE(8), MODREFDATE(8), OBSREFDATE(8)
  INTEGER                          :: MODSTARTDATE(8), MODSTOPDATE(8), OBSSTARTDATE(8), OBSSTOPDATE(8)

  INTEGER, ALLOCATABLE             :: modvartype(:), obsvartype(:), obsndims(:), outvartype(:)
  INTEGER, ALLOCATABLE             :: modtri(:,:), vartmp(:,:)
  INTEGER, ALLOCATABLE             :: ntimemod(:), ntimeobs(:), indextimeobs(:), indextimemod(:), modmap(:,:) 

  INTEGER, POINTER                 :: II(:), JJ(:)

  REAL                             :: lon_min, lon_max, lat_min, lat_max
  REAL                             :: latmin, latmax !, SX
  REAL                             :: timestride, timestridetmp, latstride, lonstride
  REAL                             :: SW, RATIO1, RATIO2
  REAL                             :: XPOS, YPOS

  REAL, ALLOCATABLE                :: RW(:)
  REAL, ALLOCATABLE                :: modoffset(:), modscalefac(:), outscalefac(:)
  REAL, ALLOCATABLE                :: obsoffset(:), obsscalefac(:), outoffset(:)
  REAL, ALLOCATABLE                :: modvalidmin(:), modvalidmax(:)
  REAL, ALLOCATABLE                :: obsvalidmin(:), obsvalidmax(:)
  REAL, ALLOCATABLE                :: outvalidmin(:), outvalidmax(:)

!  REAL, ALLOCATABLE                :: alontmp(:,:)

  REAL, POINTER                    :: alat(:,:), alon(:,:)
  REAL(4), POINTER                 :: CS(:) => NULL()

  DOUBLE PRECISION                 :: MODREFJULDAY, OBSREFJULDAY, TDIF
  DOUBLE PRECISION                 :: STARTJULDAY, STOPJULDAY, OUTREFJULDAY

  DOUBLE PRECISION, ALLOCATABLE    :: obsvarval(:,:), OBSJULDAY(:,:), MODJULDAY(:,:)
  DOUBLE PRECISION, ALLOCATABLE    :: modvarval(:,:,:,:), interpvarval(:,:), modlat(:), modlon(:), obslontmp(:)
 ! DOUBLE PRECISION, ALLOCATABLE    :: modlontmp(:)
  DOUBLE PRECISION, ALLOCATABLE    :: outvarval(:,:), obsfillval(:), modfillval(:), outfillval(:)
  DOUBLE PRECISION, ALLOCATABLE    :: modfillvalcalc(:), obsfillvalcalc(:), obsflagval(:), modflagval(:)

  CHARACTER*1024                   :: DIR_OUT, outncfile
  CHARACTER*1024                   :: OBSVARLIST, OBSTIME, OBSCOORDLIST, OBSFLAGLIST, OBSFLAGCOMP, OBSFLAGVALUE
  CHARACTER*1024                   :: MODVARLIST, MODTIME, MODFLAGLIST, MODFLAGCOMP, MODFLAGVALUE, TMPLIST
  CHARACTER*1024                   :: OBSFILE1, MODFILE1, OBSFILELIST, MODFILELIST, BASENAME
  CHARACTER*4                      :: CSTRG
  CHARACTER*50                     :: calendar
  CHARACTER*128                    :: allmodvarstr(70), allmodflagstr(20), allmodflagcompstr(20), allmodflagvaluestr(20)
  CHARACTER*128                    :: allobsvarstr(20), allobsflagstr(20), allobsflagcompstr(20), allobsflagvaluestr(20)
  CHARACTER*128                    :: modtimeunits, obstimeunits
  CHARACTER*256                    :: OUTPREFIX
  CHARACTER*3                      :: FILESPLIT, OUTTIMEUNITS
  CHARACTER*1024                   :: cmd
  CHARACTER*6                      :: INTERPSPACE

  CHARACTER*128, ALLOCATABLE       :: outunits(:), outlongname(:), outstdname(:) 
  CHARACTER*128, ALLOCATABLE       :: obsunits(:), obslongname(:), obsstdname(:)
  CHARACTER*128, ALLOCATABLE       :: modunits(:), modlongname(:), modstdname(:)
  CHARACTER*128, ALLOCATABLE       :: outvarstr(:)
  CHARACTER*1024, ALLOCATABLE      :: MODFILE(:), OBSFILE(:)

  LOGICAL                          :: INGRID, FLAGLL, filefound, MODSAMEFILE, OBSSAMEFILE, INTERPTIME
  LOGICAL                          :: NEWFILEMOD, NEWFILEOBS, NORM, OBSLON360, MODLON360, FIRSTFILEMOD, FIRSTFILEOBS, NEWOPENMOD, NEWOPENOBS, NEWFILEOUT, FIRSTFILEOUT, NEWFILEOBSFROMMOD
  LOGICAL                          :: ALLTIMEINDEX

!/
!/ ------------------------------------------------------------------- /
!/


!==========================================================
! 1. Process input
!==========================================================


!---------------------------------------------------------------
! 1.a IO set-up
!---------------------------------------------------------------

  NDSI=10
  NDSL=12
  NDSM=13
  NDSE=6
  NDSO=6


!---------------------------------------------------------------
! 1.b Read namelist
!---------------------------------------------------------------

  CALL W3NMLMATCH (NDSI, 'ww3_match.nml', NML_MATCH, IERR)
  OBSFILELIST = NML_MATCH%OBSFILELIST
  OBSTIME     = NML_MATCH%OBSTIME
  OBSCOORDLIST= NML_MATCH%OBSCOORDLIST
  OBSVARLIST  = NML_MATCH%OBSVARLIST
  OBSFLAGLIST = NML_MATCH%OBSFLAGLIST
  OBSFLAGCOMP = NML_MATCH%OBSFLAGCOMP
  OBSFLAGVALUE= NML_MATCH%OBSFLAGVALUE
  OBSSAMEFILE = NML_MATCH%OBSSAMEFILE
  OBSLON360   = NML_MATCH%OBSLON360

  MODFILELIST = NML_MATCH%MODFILELIST
  MODTIME     = NML_MATCH%MODTIME
  MODVARLIST  = NML_MATCH%MODVARLIST
  MODFLAGLIST = NML_MATCH%MODFLAGLIST
  MODFLAGCOMP = NML_MATCH%MODFLAGCOMP
  MODFLAGVALUE= NML_MATCH%MODFLAGVALUE
  MODSAMEFILE = NML_MATCH%MODSAMEFILE
  MODLON360   = NML_MATCH%MODLON360

  DIR_OUT     = NML_MATCH%DIR_OUT
  OUTPREFIX   = NML_MATCH%OUTPREFIX
  OUTTIMEUNITS= NML_MATCH%OUTTIMEUNITS
  FILESPLIT   = NML_MATCH%FILESPLIT
  NCTYPE      = NML_MATCH%NCTYPE

  READ (NML_MATCH%TIMESTART,*) TIMESTART
  READ (NML_MATCH%TIMESTOP,*) TIMESTOP
!  TIMESPLIT   = NML_MATCH%TIMESPLIT
  INTERPSPACE = NML_MATCH%INTERPSPACE
  INTERPTIME  = NML_MATCH%INTERPTIME
  MAXLANDPOINTS = NML_MATCH%MAXLANDPOINTS
  ALLTIMEINDEX  = NML_MATCH%ALLTIMEINDEX
  
  CSTRG       = NML_MATCH%CLOS
  LON_MIN     = NML_MATCH%LON_MIN
  LON_MAX     = NML_MATCH%LON_MAX
  LAT_MIN     = NML_MATCH%LAT_MIN
  LAT_MAX     = NML_MATCH%LAT_MAX
  
  ! number of input sat file
  NOBSFILE = 0
  OPEN(NDSL,FILE=TRIM(OBSFILELIST),STATUS='OLD',ERR=809,IOSTAT=IERR)
  REWIND (NDSL)
  DO
    READ (NDSL,*,END=400,ERR=802)
    NOBSFILE = NOBSFILE + 1
  END DO
  400 CONTINUE
  REWIND (NDSL)
  ALLOCATE(ntimeobs(NOBSFILE))
  ntimeobs(:)=0
  ALLOCATE(OBSFILE(NOBSFILE))

  ! number of input mod file
  NMODFILE = 0
  OPEN(NDSM,FILE=TRIM(MODFILELIST),STATUS='OLD',ERR=809,IOSTAT=IERR)
  REWIND (NDSM)
  DO
    READ (NDSM,*,END=401,ERR=802)
    NMODFILE = NMODFILE + 1
  END DO
  401 CONTINUE
  REWIND (NDSM)
  ALLOCATE(ntimemod(NMODFILE))
  ALLOCATE(MODFILE(NMODFILE))


  ! obs arrays (variables and flags)
  allobsflagcompstr(:)='.LT.'
  WRITE(TMPLIST,'(5A)') TRIM(OBSCOORDLIST), ' ', TRIM(OBSVARLIST), ' ', TRIM(OBSFLAGLIST)
  OBSVARLIST=TMPLIST
  CALL STRSPLIT(OBSVARLIST,allobsvarstr,nvarobs)
  CALL STRSPLIT(OBSFLAGLIST,allobsflagstr,nflagobs)
  CALL STRSPLIT(OBSFLAGCOMP,allobsflagcompstr,nflagobs)
  CALL STRSPLIT(OBSFLAGVALUE,allobsflagvaluestr,nflagobs)
  ALLOCATE(OBSFLAGVAL(NFLAGOBS))
  DO I=1,NFLAGOBS
    READ(allobsflagvaluestr(I),*) OBSFLAGVAL(I)
    WRITE(NDSO,*) '[INFO] flag ', TRIM(allobsflagstr(I)), ' will skip values ', TRIM(allobsflagcompstr(I)), OBSFLAGVAL(I) 
  END DO

  ! mod arrays (variables and flags)
  allmodflagcompstr(:)='.LT.'
  WRITE(TMPLIST,'(3A)') TRIM(MODVARLIST), ' ', TRIM(MODFLAGLIST)
  MODVARLIST=TMPLIST
  CALL STRSPLIT(MODVARLIST,allmodvarstr,nvarmod)
  CALL STRSPLIT(MODFLAGLIST,allmodflagstr,nflagmod)
  CALL STRSPLIT(MODFLAGCOMP,allmodflagcompstr,nflagmod)
  CALL STRSPLIT(MODFLAGVALUE,allmodflagvaluestr,nflagmod)
  ALLOCATE(MODFLAGVAL(NFLAGMOD))
  DO I=1,NFLAGMOD
    READ(allmodflagvaluestr(I),*) MODFLAGVAL(I)
    WRITE(NDSO,*) TRIM(allmodflagstr(I)), TRIM(allmodflagcompstr(I)), MODFLAGVAL(I) 
  END DO

  ! output variable names
  nvarout=2 ! time, index

  ! false to calculate norm
  NORM=.FALSE.

  ! flag for output file creation
  NEWFILEOUT=.FALSE.
  FIRSTFILEOUT=.TRUE.

  ! Allocate attributes arrays
  ALLOCATE(outvartype(nvarout+nvarmod+nvarobs))
  ALLOCATE(outoffset(nvarout+nvarmod+nvarobs))
  ALLOCATE(outscalefac(nvarout+nvarmod+nvarobs))
  ALLOCATE(outunits(nvarout+nvarmod+nvarobs))
  ALLOCATE(outlongname(nvarout+nvarmod+nvarobs))
  ALLOCATE(outstdname(nvarout+nvarmod+nvarobs))
  ALLOCATE(outvalidmin(nvarout+nvarmod+nvarobs))
  ALLOCATE(outvalidmax(nvarout+nvarmod+nvarobs))
  ALLOCATE(outfillval(nvarout+nvarmod+nvarobs))
  ALLOCATE(outvarstr(nvarout+nvarmod+nvarobs))

  ! time lat lon
  outvarstr(1)='time'
  outvartype(1)=NF90_DOUBLE
  outvarstr(2)='index'
  outvartype(2)=NF90_INT


  ! TIMESPLIT defines the number of characters in the date for the filename
!  IF (TIMESPLIT.LT.4) TIMESPLIT=4 
!  IF (TIMESPLIT.GT.10) TIMESPLIT=10 
!  WRITE(6,'(A,I2)') 'Number of characters used to define the date : ',TIMESPLIT

  ! start and stop time
  CALL T2D(TIMESTART,STARTDATE,IERR)
  CALL D2J(STARTDATE,STARTJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)') 'input file start time : ', STARTDATE(:)
  CALL T2D(TIMESTOP,STOPDATE,IERR)
  CALL D2J(STOPDATE,STOPJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)') 'input file stop  time : ', STOPDATE(:)
  





!==========================================================
!
! 5. Read the first obs file to setup the refdate & attributes
!
!==========================================================

  WRITE(NDSO,'(/A)') 'Open first obs file : '
  READ (NDSL,'(A1024)',END=801,ERR=802) OBSFILE1
  WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(OBSFILE1)
  iret = nf90_open(OBSFILE1, NF90_NOWRITE, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)



  ! get the obs reference time
  DATE(:)=0
  iret=nf90_inq_varid(ncid,TRIM(OBSTIME), varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_att(ncid,varidtmp,"calendar",calendar)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(NDSE,1028)
  ELSE IF ((INDEX(calendar, "standard").EQ.0) .AND. &
           (INDEX(calendar, "gregorian").EQ.0)) THEN
    WRITE(NDSE,1029)
    STOP
  END IF

  IRET=NF90_GET_ATT(NCID,VARIDTMP,'units',OBSTIMEUNITS)
  CALL U2D(OBSTIMEUNITS,OBSREFDATE,IERR)
  CALL D2J(OBSREFDATE,OBSREFJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)')  'obs reference time : ', OBSREFDATE(:)

  ! allocate attributes arrays
  allocate(obsvartype(nvarobs))
  allocate(obsndims(nvarobs))
  allocate(obslongname(nvarobs))
  allocate(obsstdname(nvarobs))
  allocate(obsoffset(nvarobs))
  allocate(obsscalefac(nvarobs))
  allocate(obsunits(nvarobs))
  allocate(obsfillval(nvarobs))
  allocate(obsfillvalcalc(nvarobs))
  allocate(obsvalidmin(nvarobs))
  allocate(obsvalidmax(nvarobs))
  
  ! get attributes
  nvarobsout=-2 ! substract lat and lon

  ! number of files for the selected variables
  nfilevarobs=1

  DO ivar=1,nvarobs

    ! inq variable
    write(NDSO,'(2A)') 'variable : ', trim(allobsvarstr(ivar))
    iret=nf90_inq_varid(ncid, trim(allobsvarstr(ivar)), varid(ivar))
    IF (iret.NE.0) THEN
      IF (OBSSAMEFILE.EQV..TRUE.) THEN
        WRITE(*,*) 'ERROR MISSING VARIABLE FROM OBS FILE'
        STOP
      ELSE
!/T        WRITE(NDSO,'(/A,A)') 'Closing ', TRIM(OBSFILE1)
        iret=NF90_CLOSE(NCID)
        CALL CHECK_ERROR(IRET,__LINE__)
        READ (NDSL,'(A1024)',END=801,ERR=802) OBSFILE1
!/T        WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(OBSFILE1)
        iret = nf90_open(trim(OBSFILE1), NF90_NOWRITE, ncid)
        CALL CHECK_ERROR(IRET,__LINE__)
        nfilevarobs=nfilevarobs+1
        iret=nf90_inq_varid(ncid, trim(allobsvarstr(ivar)), varid(ivar))
        CALL CHECK_ERROR(IRET,__LINE__)
      END IF
    END IF

    iret=nf90_inquire_variable(ncid,varid(ivar),xtype=obsvartype(ivar),ndims=obsndims(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)


    ! process attributes 
    iret=nf90_get_att(ncid,varid(ivar),'add_offset',obsoffset(ivar))
    if (iret.NE.0) obsoffset(ivar)=0.0
    iret=nf90_get_att(ncid,varid(ivar),'scale_factor',obsscalefac(ivar))
    if (iret.NE.0) obsscalefac(ivar)=1.0
    iret=nf90_get_att(ncid,varid(ivar),'_FillValue',obsfillval(ivar))
    if (iret.NE.0) THEN
      if(obsvartype(ivar).EQ.NF90_SHORT) obsfillval(ivar)=NF90_FILL_SHORT
      if(obsvartype(ivar).EQ.NF90_INT) obsfillval(ivar)=NF90_FILL_INT
      if(obsvartype(ivar).EQ.NF90_FLOAT) obsfillval(ivar)=NF90_FILL_FLOAT
      if(obsvartype(ivar).EQ.NF90_DOUBLE) obsfillval(ivar)=NF90_FILL_DOUBLE
    END IF
    iret=nf90_get_att(ncid,varid(ivar),'units',obsunits(ivar))
    if (iret.NE.0) obsunits(ivar)='unknown'
    iret=nf90_get_att(ncid,varid(ivar),'long_name',obslongname(ivar))
    if (iret.NE.0) obslongname(ivar)=allobsvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'standard_name',obsstdname(ivar))
    if (iret.NE.0) obsstdname(ivar)=allobsvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'valid_min',obsvalidmin(ivar))
    if (iret.NE.0) obsvalidmin(ivar)=-1000
    iret=nf90_get_att(ncid,varid(ivar),'valid_max',obsvalidmax(ivar))
    if (iret.NE.0) obsvalidmin(ivar)=1000

    ! Calculate "FillValue after computation of scale factor and offset"
!    obsfillvalcalc(ivar) = obsfillval(ivar) * obsscalefac(ivar) + obsoffset(ivar)

    ! increment output variable index
    nvarout=nvarout+1
    nvarobsout=nvarobsout+1

    ! output attributes
    outvartype(nvarout)=obsvartype(ivar)
    outoffset(nvarout)=obsoffset(ivar)
    outscalefac(nvarout)=obsscalefac(ivar)
    outfillval(nvarout)=obsfillval(ivar)
    outunits(nvarout)=obsunits(ivar)
    outlongname(nvarout)=obslongname(ivar)
    outstdname(nvarout)=obsstdname(ivar)
    outvalidmin(nvarout)=obsvalidmin(ivar)
    outvalidmax(nvarout)=obsvalidmax(ivar)

    ! only unpack coord variables if needed
    IF ((ivar.EQ.1).OR.(ivar.EQ.2)) THEN
      IF (obsscalefac(ivar).NE.1.0) THEN
        outscalefac(nvarout)=1.0
        outvartype(nvarout)=NF90_FLOAT
        outfillval(nvarout)=NF90_FILL_FLOAT
      END IF 
    END IF

    ! output var name and std/long name atts
    outvarstr(nvarout)=TRIM(allobsvarstr(ivar))
    outlongname(nvarout)=obslongname(ivar)
    outstdname(nvarout)=obsstdname(ivar)

  END DO

  ! close obs file
  iret=nf90_close(ncid)
  CALL CHECK_ERROR(IRET,__LINE__)






!==========================================================
!
! 6. Loop on all obs files to find the maximum time dimension
!
!==========================================================

!  WRITE(NDSO,'(/A)') 'Searching the max time obs in first 10 files... '
  WRITE(NDSO,'(/A)') 'Searching the max time obs... '
  REWIND(NDSL)
  !DO IOBS=1,MIN(10,NOBSFILE)
  DO IOBS=1,NOBSFILE
!/T    WRITE(NDSO,'(/A,I6,A,I6)') 'Opening obs file number ', IOBS,' /',NOBSFILE
    READ (NDSL,'(A1024)',END=801,ERR=802) OBSFILE(IOBS)
!/T    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(OBSFILE(IOBS))
    ! open file if exists, otherwise exit the loop
    iret = nf90_open(trim(obsfile(IOBS)), NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get the time dimension
    iret=nf90_inq_dimid(ncid,TRIM(OBSTIME),dimid(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(1),len=dimln(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    ntimeobs(iobs)=dimln(1)

    ! close obs file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)  

  END DO ! iobs=1,nobsfile

  !maxntimeobs=maxval(ntimeobs)+1.2*maxval(ntimeobs)
  maxntimeobs=maxval(ntimeobs)
  ALLOCATE(OBSJULDAY(NOBSFILE,maxntimeobs))
  WRITE(NDSO,'(A,I12)') 'max time obs : ', maxntimeobs


!==========================================================
!
! 7. Loop on all obs files to get all the time values
!
!==========================================================

  WRITE(NDSO,'(/A)') 'Storing time obs... '
  REWIND(NDSL)
  DO IOBS=1,NOBSFILE
!/T    WRITE(NDSO,'(/A,I6,A,I6)') 'Opening obs file number ', IOBS,' /',NOBSFILE
    READ (NDSL,'(A1024)',END=801,ERR=802) OBSFILE(IOBS)
!/T    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(OBSFILE(IOBS))
    ! open file if exists, otherwise exit the loop
    iret = nf90_open(trim(OBSFILE(IOBS)), NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get time dimension
    !iret=nf90_inquire_dimension(ncid,dimid(1),len=dimln(1))
    !CALL CHECK_ERROR(IRET,__LINE__)
    !ntimeobs(iobs)=dimln(1)
    !IF (maxntimeobs.LT.ntimeobs(iobs)) THEN
    !  WRITE(NDSO,*) '[WARNING] estimation maxntimeobs is smaller than real ntimeobs(iobs)'
    !  WRITE(NDSO,*) '          consider increasing the 20% of security'
    !  STOP
    !END IF

    ! get the time variable
    iret=nf90_inq_varid(ncid,TRIM(OBSTIME), varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, OBSJULDAY(IOBS,1:ntimeobs(iobs)))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! convert the julian date in calendar date
    IF (index(obstimeunits, "seconds").NE.0)    OBSJULDAY(IOBS,1:ntimeobs(iobs))=OBSJULDAY(IOBS,1:ntimeobs(iobs))/86400.
    IF (index(obstimeunits, "minutes").NE.0)   OBSJULDAY(IOBS,1:ntimeobs(iobs))=OBSJULDAY(IOBS,1:ntimeobs(iobs))/1440.
    IF (index(obstimeunits, "hours").NE.0)     OBSJULDAY(IOBS,1:ntimeobs(iobs))=OBSJULDAY(IOBS,1:ntimeobs(iobs))/24.
    OBSJULDAY(IOBS,1:ntimeobs(iobs))=OBSREFJULDAY+OBSJULDAY(IOBS,1:ntimeobs(iobs))

    ! close obs file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)  

  END DO ! iobs=1,nobsfile

  ! Print obs start/stop time, steps and stride
  CALL J2D(OBSJULDAY(1,1),OBSSTARTDATE,IERR)
  WRITE(NDSO,'(A,8I4)')  'obs first time : ', OBSSTARTDATE(:)
  CALL J2D(OBSJULDAY(NOBSFILE,NTIMEOBS(NOBSFILE)),OBSSTOPDATE,IERR)
  WRITE(NDSO,'(A,8I4)')  'obs last time : ', OBSSTOPDATE(:)



!==========================================================
!
! 2. Read the first model file to setup the refdate, grid, attributes and more
!
!==========================================================

  WRITE(NDSO,'(/A)') 'Open first mod file : '
  READ (NDSM,'(A1024)',END=801,ERR=802) MODFILE1
  WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(MODFILE1)
  iret = nf90_open(MODFILE1, NF90_NOWRITE, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)

  ! define the grid type
  iret=nf90_inq_dimid(ncid,'node',dimid(1))
  IF(iret.NE.0) THEN
    iret=nf90_inq_varid(ncid, 'longitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid, 'LONGITUDE', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid, 'lon', varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_variable(ncid,varidtmp,ndims=varndims)
    CALL CHECK_ERROR(IRET,__LINE__)
    IF (varndims.EQ.2) THEN
      WRITE(NDSO,'(A)') 'The grid type is CURVILINEAR'
      GTYPE=CLGTYPE
      GRIDDIMS=3
    ELSE  
      WRITE(NDSO,'(A)') 'The grid type is REGULAR'
      GTYPE=RLGTYPE
      GRIDDIMS=3
    END IF
  ELSE
    WRITE(NDSO,'(A)') 'The grid type is UNSTRUCTURED'
    GTYPE=UNGTYPE
    GRIDDIMS=2
  END IF

  ! initialize lat lon for first model file
  ! For regular grid
  IF (GTYPE.EQ.RLGTYPE) THEN
    ! lat lon dimensions
    WRITE(NDSO,'(A)') 'dimension : latitude|LATITUDE|lat'
    iret=nf90_inq_dimid(ncid,'latitude',dimid(2))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'LATITUDE',dimid(2))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'lat',dimid(2))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(2),len=dimln(2))
    nlatmod=dimln(2)
    WRITE(NDSO,'(A)') 'dimension : longitude|LONGITUDE|lon'
    iret=nf90_inq_dimid(ncid,'longitude',dimid(3))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'LONGITUDE',dimid(3))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'lon',dimid(3))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(3),len=dimln(3))
    nlonmod=dimln(3)

    ! model variable latitude
    IF(ALLOCATED(modlat)) deallocate(modlat)
    allocate(modlat(nlatmod))
    WRITE(NDSO,'(A)') 'variable : latitude|LATITUDE|lat'
    iret=nf90_inq_varid(ncid,'latitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LATITUDE',varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lat',varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, modlat(:))
    CALL CHECK_ERROR(IRET,__LINE__)
    latmin=minval(modlat)
    latmax=maxval(modlat)
    latstride=(latmax-latmin)/real(nlatmod-1)
    IF(latmin.GT.lat_min) lat_min=latmin
    IF(latmax.LT.lat_max) lat_max=latmax

    ! model variable longitude
    IF(ALLOCATED(modlon)) deallocate(modlon)
    allocate(modlon(nlonmod))
    WRITE(NDSO,'(A)') 'variable : longitude|LONGITUDE|lon'
    iret=nf90_inq_varid(ncid,'longitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LONGITUDE',varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lon',varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, modlon(:))
    CALL CHECK_ERROR(IRET,__LINE__)
    lonstride=(maxval(modlon)-minval(modlon))/real(nlonmod-1)

    ! convert model longitude to [0.;360.] or [-180.;180.]
    !IF(ALLOCATED(modlontmp)) deallocate(modlontmp)
    !allocate(modlontmp(nlonmod))
    !IF (OBSLON360 .AND. .NOT. MODLON360) THEN
    !  DO I=1,nlonmod      
    !      modlontmp(I)=mod(modlon(I)+360,360.)
    !  END DO
    !ELSE IF (.NOT. OBSLON360 .AND. MODLON360) THEN
    !  DO I=1,dimln(3)
    !    IF((modlon(I).GE.179.98) .AND. (modlon(I).LT.359.98)) THEN
    !      modlontmp(I)=modlon(I)-360.
    !    ELSE
    !      modlontmp(I)=modlon(I)
    !    END IF
    !  END DO
    !END IF 
    !modlon=modlontmp
    !DEALLOCATE(modlontmp)

    ! model variable map status
    IF(ALLOCATED(modmap)) deallocate(modmap)
    allocate(modmap(nlonmod,nlatmod))
    WRITE(NDSO,'(A)') 'variable : MAPSTA'
    iret=nf90_inq_varid(ncid,'MAPSTA', varidtmp)
    IF (iret.NE.0) THEN
      WRITE(NDSO,'(A)') '[WARNING] no MAPSTA found. All points set as wet points'
      modmap(:,:)=1
    ELSE
      iret=nf90_get_var(ncid, varidtmp, modmap(:,:))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF

    ! model lat lon arrays for GSI
    IF(ASSOCIATED(alat)) deallocate(alat)
    allocate(alat(nlonmod,nlatmod))
    IF(ASSOCIATED(alon)) deallocate(alon)
    allocate(alon(nlonmod,nlatmod))
    DO i=1,nlonmod
      alat(i,:)=modlat(:)
    END DO
    DO i=1,nlatmod
      alon(:,i)=modlon(:)
    END DO

  ! For curvilinear grid
  ELSE IF (GTYPE.EQ.CLGTYPE) THEN

    ! lat lon dimensions
    WRITE(NDSO,'(A)') 'dimension : latitude|LATITUDE|lat'
    iret=nf90_inq_dimid(ncid,'latitude',dimid(2))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'LATITUDE',dimid(2))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'lat',dimid(2))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(2),len=dimln(2))
    nlatmod=dimln(2)
    WRITE(NDSO,'(A)') 'dimension : longitude|LONGITUDE|lon'
    iret=nf90_inq_dimid(ncid,'longitude',dimid(3))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'LONGITUDE',dimid(3))
    IF(IRET.NE.0) iret=nf90_inq_dimid(ncid,'lon',dimid(3))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(3),len=dimln(3))
    nlonmod=dimln(3)

    ! model variable latitude
    IF(ASSOCIATED(alat)) deallocate(alat)
    allocate(alat(nlonmod,nlatmod))
    WRITE(NDSO,'(A)') 'variable : latitude|LATITUDE|lat'
    iret=nf90_inq_varid(ncid,'latitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LATITUDE', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lat', varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, alat(:,:), count=(/nlonmod,nlatmod/))
    CALL CHECK_ERROR(IRET,__LINE__)
    latmin=minval(alat)
    latmax=maxval(alat)
    IF(latmin.GT.lat_min) lat_min=latmin
    IF(latmax.LT.lat_max) lat_max=latmax

    ! model variable longitude
    IF(ASSOCIATED(alon)) deallocate(alon)
    allocate(alon(nlonmod,nlatmod))
    WRITE(NDSO,'(A)') 'variable : longitude|LONGITUDE|lon'
    iret=nf90_inq_varid(ncid,'longitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LONGITUDE', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lon', varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, alon(:,:), count=(/nlonmod,nlatmod/))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! convert model longitude to [0.;360.] or [-180.;180.]
    !IF(ALLOCATED(alontmp)) deallocate(alontmp)
    !allocate(alontmp(nlonmod,nlatmod))
    !IF (OBSLON360 .AND. .NOT. MODLON360) THEN
    !  DO I=1,nlonmod      
    !    DO J=1,nlatmod
    !      alontmp(I,J)=mod(alon(I,J)+360,360.)
    !    END DO
    !  END DO
    !ELSE IF (.NOT. OBSLON360 .AND. MODLON360) THEN
    !  DO I=1,nlonmod      
    !    DO J=1,nlatmod
    !      IF((alon(I,J).GE.179.98) .AND. (alon(I,J).LT.359.98)) THEN
    !        alontmp(I,J)=alon(I,J)-360.
    !      ELSE
    !        alontmp(I,J)=alon(I,J)
    !      END IF
    !    END DO
    !  END DO
    !END IF 
    !alon=alontmp
    !DEALLOCATE(alontmp)


    ! model variable map status
    IF(ALLOCATED(modmap)) deallocate(modmap)
    allocate(modmap(nlonmod,nlatmod))
    WRITE(NDSO,'(A)') 'variable : MAPSTA'
    iret=nf90_inq_varid(ncid,'MAPSTA', varidtmp)
    IF (iret.NE.0) THEN
      WRITE(NDSO,'(A)') '[WARNING] no MAPSTA found. All points set as wet points'
      modmap(:,:)=1
    ELSE
      iret=nf90_get_var(ncid, varidtmp, modmap(:,:))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF


  ! For unstructured mesh
  ELSE IF (GTYPE.EQ.UNGTYPE) THEN

    ! lat lon dimensions
    WRITE(NDSO,'(A)') 'dimension : node'
    iret=nf90_inq_dimid(ncid,'node',dimid(2))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(2),len=dimln(2))
    nnodemod=dimln(2)
    WRITE(NDSO,'(A)') 'dimension : element'
    iret=nf90_inq_dimid(ncid,'element',dimid(3))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(3),len=dimln(3))
    nelemmod=dimln(3)
    WRITE(NDSO,'(A)') 'dimension : noel'
    iret=nf90_inq_dimid(ncid,'noel',dimid(4))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(4),len=dimln(4))
    nnoelmod=dimln(4)

    nlonmod=nnodemod
    nlatmod=1

    ! model variable latitude
    IF(ALLOCATED(modlat)) deallocate(modlat)
    allocate(modlat(nnodemod))
    WRITE(NDSO,'(A)') 'variable : latitude|LATITUDE|lat'
    iret=nf90_inq_varid(ncid,'latitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LATITUDE',varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lat',varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, modlat(:))
    CALL CHECK_ERROR(IRET,__LINE__)
    latmin=minval(modlat)
    latmax=maxval(modlat)
    !n/a latstride=(latmax-latmin)/(nlatmod-1)
    IF(latmin.GT.lat_min) lat_min=latmin
    IF(latmax.LT.lat_max) lat_max=latmax

    ! model variable longitude
    IF(ALLOCATED(modlon)) deallocate(modlon)
    allocate(modlon(nnodemod))
    WRITE(NDSO,'(A)') 'variable : longitude|LONGITUDE|lon'
    iret=nf90_inq_varid(ncid,'longitude', varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'LONGITUDE',varidtmp)
    IF(IRET.NE.0) iret=nf90_inq_varid(ncid,'lon',varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, modlon(:))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! convert model longitude to [0.;360.] or [-180.;180.]
    !IF(ALLOCATED(modlontmp)) deallocate(modlontmp)
    !allocate(modlontmp(nlonmod))
    !IF (OBSLON360 .AND. .NOT. MODLON360) THEN
    !  DO I=1,nlonmod      
    !    modlontmp(I)=mod(modlon(I)+360,360.)
    !  END DO
    !ELSE IF (.NOT. OBSLON360 .AND. MODLON360) THEN
    !  DO I=1,dimln(2)
    !    IF((modlon(I).GE.179.98) .AND. (modlon(I).LT.359.98)) THEN
    !      modlontmp(I)=modlon(I)-360.
    !    ELSE
    !      modlontmp(I)=modlon(I)
    !    END IF
    !  END DO
    !END IF
    !modlon=modlontmp
    !DEALLOCATE(modlontmp)

    ! model variable tri
    IF(ALLOCATED(modtri)) deallocate(modtri)
    allocate(vartmp(nnoelmod, nelemmod))
    allocate(modtri(nelemmod, nnoelmod))
    WRITE(NDSO,'(A)') 'variable : tri'
    iret=nf90_inq_varid(ncid,'tri', varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, vartmp(:,:))
    CALL CHECK_ERROR(IRET,__LINE__)
    modtri=transpose(vartmp)
!    WRITE(6,*) 'shape(vartmp) :', shape(vartmp)
!    WRITE(6,*) 'shape(modtri) :', shape(modtri)

    ! model variable map status
    IF(ALLOCATED(modmap)) deallocate(modmap)
    allocate(modmap(nnodemod,1))
    WRITE(NDSO,'(A)') 'variable : MAPSTA'
    iret=nf90_inq_varid(ncid,'MAPSTA', varidtmp)
    IF (iret.NE.0) THEN
      WRITE(NDSO,'(A)') '[WARNING] no MAPSTA found. All points set as wet points'
      modmap(:,1)=1
    ELSE
      iret=nf90_get_var(ncid, varidtmp, modmap(:,1))
      CALL CHECK_ERROR(IRET,__LINE__)
    END IF

    ! model lat lon arrays for GSU
    IF(ASSOCIATED(alat)) deallocate(alat)
    allocate(alat(nlonmod,nlatmod))
    IF(ASSOCIATED(alon)) deallocate(alon)
    allocate(alon(nlonmod,nlatmod))
    DO i=1,nlonmod
      alat(i,:)=modlat(:)
    END DO
    DO i=1,nlatmod
      alon(:,i)=modlon(:)
    END DO

    ! Define variables for interpolation later
    IF(.NOT. ALLOCATED(XYB)) ALLOCATE(XYB(nnodemod,3))
    XYB(:,1)=modlon(:)
    XYB(:,2)=modlat(:)
    XYB(:,3)=modmap(:,1)
    NX=nnodemod
    NTRI=nelemmod

  END IF ! GTYPE


  ! setup coordinates
  FLAGLL=.TRUE.
  IF (CSTRG.EQ.'NONE') THEN
    ICLO = ICLOSE_NONE
  ELSE IF (CSTRG.EQ.'SMPL') THEN
    ICLO = ICLOSE_SMPL
  ELSE IF (CSTRG.EQ.'TRPL') THEN
    ICLO = ICLOSE_TRPL
  END IF
  !SX = (modlon(nlonmod)-modlon(1))/REAL(nlonmod-1)
  !IF ( ABS(ABS(REAL(nlonmod)*SX)-360.) .LT. 0.1*ABS(SX) ) THEN
  !  ICLO = ICLOSE_SMPL
  !END IF

  ! Create the grid-search-utility object    
  GSI = W3GSUC( .TRUE., FLAGLL, ICLO, alon, alat )

  ! get the model reference time
  DATE(:)=0
  WRITE(NDSO,'(2A)') 'variable : ', TRIM(MODTIME)
  iret=nf90_inq_varid(ncid,TRIM(MODTIME), varidtmp)
  CALL CHECK_ERROR(IRET,__LINE__)
  iret=nf90_get_att(ncid,varidtmp,"calendar",calendar)
  IF ( iret/=nf90_noerr ) THEN
    WRITE(NDSE,1028)
  ELSE IF ((INDEX(calendar, "standard").EQ.0) .AND. &
           (INDEX(calendar, "gregorian").EQ.0)) THEN
    WRITE(NDSE,1029)
    STOP
  END IF

  IRET=NF90_GET_ATT(NCID,VARIDTMP,'units',MODTIMEUNITS)
  CALL U2D(MODTIMEUNITS,MODREFDATE,IERR)
  CALL D2J(MODREFDATE,MODREFJULDAY,IERR)
  WRITE(NDSO,'(A,8I4)')  'model reference time : ', MODREFDATE(:)


  ! allocate attributes arrays
  allocate(modvartype(nvarmod))
  allocate(modlongname(nvarmod))
  allocate(modstdname(nvarmod))
  allocate(modoffset(nvarmod))
  allocate(modscalefac(nvarmod))
  allocate(modunits(nvarmod))
  allocate(modfillval(nvarmod))
  allocate(modfillvalcalc(nvarmod))
  allocate(modvalidmin(nvarmod))
  allocate(modvalidmax(nvarmod))
  
  ! get attributes
  nvarmodout=0

  ! number of files for the selected variables
  nfilevarmod=1

  DO ivar=1,nvarmod

    ! inq variable
    write(NDSO,'(2A)') 'variable : ', trim(allmodvarstr(ivar))
    iret=nf90_inq_varid(ncid, trim(allmodvarstr(ivar)), varid(ivar))
    IF (iret.NE.0) THEN
      IF (MODSAMEFILE.EQV..TRUE.) THEN
        WRITE(*,*) 'ERROR MISSING VARIABLE FROM MOD FILE'
        STOP
      ELSE
!/T        WRITE(NDSO,'(/A,A)') 'Closing ', TRIM(MODFILE1)
        iret=NF90_CLOSE(NCID)
        CALL CHECK_ERROR(IRET,__LINE__)
        READ (NDSM,'(A1024)',END=801,ERR=802) MODFILE1
!/T        WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(MODFILE1)
        iret = nf90_open(trim(MODFILE1), NF90_NOWRITE, ncid)
        CALL CHECK_ERROR(IRET,__LINE__)
        nfilevarmod=nfilevarmod+1
        iret=nf90_inq_varid(ncid, trim(allmodvarstr(ivar)), varid(ivar))
        CALL CHECK_ERROR(IRET,__LINE__)
      END IF
    END IF

    iret=nf90_inquire_variable(ncid,varid(ivar),xtype=modvartype(ivar))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! process attributes 
    iret=nf90_get_att(ncid,varid(ivar),'add_offset',modoffset(ivar))
    if (iret.NE.0) modoffset(ivar)=0.0
    iret=nf90_get_att(ncid,varid(ivar),'scale_factor',modscalefac(ivar))
    if (iret.NE.0) modscalefac(ivar)=1.0
    iret=nf90_get_att(ncid,varid(ivar),'_FillValue',modfillval(ivar))
    if (iret.NE.0) THEN
      if(modvartype(ivar).EQ.NF90_SHORT) modfillval(ivar)=NF90_FILL_SHORT
      if(modvartype(ivar).EQ.NF90_INT) modfillval(ivar)=NF90_FILL_INT
      if(modvartype(ivar).EQ.NF90_FLOAT) modfillval(ivar)=NF90_FILL_FLOAT
      if(modvartype(ivar).EQ.NF90_DOUBLE) modfillval(ivar)=NF90_FILL_DOUBLE
    END IF

    iret=nf90_get_att(ncid,varid(ivar),'units',modunits(ivar))
    if (iret.NE.0) modunits(ivar)='unknown'
    iret=nf90_get_att(ncid,varid(ivar),'long_name',modlongname(ivar))
    if (iret.NE.0) modlongname(ivar)=allmodvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'standard_name',modstdname(ivar))
    if (iret.NE.0) modstdname(ivar)=allmodvarstr(ivar)
    iret=nf90_get_att(ncid,varid(ivar),'valid_min',modvalidmin(ivar))
    if (iret.NE.0) modvalidmin(ivar)=-100000
    iret=nf90_get_att(ncid,varid(ivar),'valid_max',modvalidmax(ivar))
    if (iret.NE.0) modvalidmin(ivar)=100000

    ! increment output variable index
    nvarout=nvarout+1
    nvarmodout=nvarmodout+1

    ! output attributes
    outvartype(nvarout)=modvartype(ivar)
    outoffset(nvarout)=modoffset(ivar)
    outscalefac(nvarout)=modscalefac(ivar)
    outfillval(nvarout)=modfillval(ivar)
    outunits(nvarout)=modunits(ivar)
    outvalidmin(nvarout)=modvalidmin(ivar)
    outvalidmax(nvarout)=modvalidmax(ivar)

    ! output var name and std/long name atts
    outvarstr(nvarout)=TRIM(allmodvarstr(ivar))
    outlongname(nvarout)=modlongname(ivar)
    outstdname(nvarout)=modstdname(ivar)


  END DO

  
  ! close model file
  iret=nf90_close(ncid)
  CALL CHECK_ERROR(IRET,__LINE__)



!==========================================================
!
! 3. Loop on all model files to find the maximum time dimension
!
!==========================================================

  REWIND(NDSM)
  DO IMOD=1,NMODFILE

!/T    WRITE(NDSO,'(/A,I6,A,I6)') 'Opening model file number ', IMOD,' /',NMODFILE
    READ (NDSM,'(A1024)',END=801,ERR=802) MODFILE(IMOD)
!/T    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(MODFILE(IMOD))

    ! open file if exists, otherwise exit the loop
    iret = nf90_open(trim(MODFILE(IMOD)), NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get the time dimension
    iret=nf90_inq_dimid(ncid,TRIM(MODTIME),dimid(1))
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_inquire_dimension(ncid,dimid(1),len=dimln(1))
    ntimemod(imod)=dimln(1)

    ! close model file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)  

  END DO ! imod=1,nmodfile

  maxntimemod=maxval(ntimemod)
  ALLOCATE(MODJULDAY(IMOD,maxntimemod))
  WRITE(NDSO,'(A,I12)') 'max time mod : ', maxntimemod



!==========================================================
!
! 4. Loop on all model files to get all the time values
!
!==========================================================

  DO IMOD=1,NMODFILE
!/T    WRITE(NDSO,'(/A,I6,A,I6)') 'Opening model file number ', IMOD,' /',NMODFILE
!/T    WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(MODFILE(IMOD))

    ! open file if exists, otherwise exit the loop
    iret = nf90_open(trim(MODFILE(IMOD)), NF90_NOWRITE, ncid)
    CALL CHECK_ERROR(IRET,__LINE__)

    ! get the time variable
    iret=nf90_inq_varid(ncid,TRIM(MODTIME), varidtmp)
    CALL CHECK_ERROR(IRET,__LINE__)
    iret=nf90_get_var(ncid, varidtmp, MODJULDAY(IMOD,1:ntimemod(imod)))
    CALL CHECK_ERROR(IRET,__LINE__)

    ! convert the julian date in calendar date
    IF (index(modtimeunits, "seconds").NE.0)    MODJULDAY(IMOD,:)=MODJULDAY(IMOD,:)/86400.
    IF (index(modtimeunits, "minutes").NE.0)   MODJULDAY(IMOD,:)=MODJULDAY(IMOD,:)/1440.
    IF (index(modtimeunits, "hours").NE.0)     MODJULDAY(IMOD,:)=MODJULDAY(IMOD,:)/24.
    MODJULDAY(IMOD,:)=MODREFJULDAY+MODJULDAY(IMOD,:)

    ! set timestride and check consistency
    IF (ntimemod(IMOD) .GT. 1) THEN
      IF (IMOD.EQ.1) TIMESTRIDE = (MODJULDAY(IMOD,ntimemod(IMOD))-MODJULDAY(IMOD,1))/(ntimemod(IMOD)-1)
      TIMESTRIDETMP = (MODJULDAY(IMOD,ntimemod(IMOD))-MODJULDAY(IMOD,1))/(ntimemod(IMOD)-1)
      IF ( TIMESTRIDE .NE. TIMESTRIDETMP ) THEN
        WRITE(NDSO,*) 'WARNING : timestride changes between first model file and model file number ', IMOD
        WRITE(NDSO,*) TIMESTRIDETMP, '/=', TIMESTRIDE
        WRITE(NDSO,*) MODJULDAY(IMOD,ntimemod(IMOD)), MODJULDAY(IMOD,1), ntimemod(IMOD)
        STOP
      END IF
    ELSE
      IF (IMOD.GT.1) TIMESTRIDE = (MODJULDAY(IMOD,1)-MODJULDAY(IMOD-1,1))
    END IF

    ! close model file
    iret=nf90_close(ncid)
    CALL CHECK_ERROR(IRET,__LINE__)  

  END DO ! imod=1,nmodfile


  ! Print model start/stop time, steps and stride
  CALL J2D(MODJULDAY(1,1),MODSTARTDATE,IERR)
  WRITE(NDSO,'(A,8I4)')  'model first time : ', MODSTARTDATE(:)
  CALL J2D(MODJULDAY(NMODFILE,NTIMEMOD(NMODFILE)),MODSTOPDATE,IERR)
  WRITE(NDSO,'(A,8I4)')  'model last time : ', MODSTOPDATE(:)
  WRITE(NDSO,'(A,F8.4,A,F8.4,A)') 'model time stride : ', timestride, ' days (', timestride*24, ' hours )'






!==========================================================
!
! 9. Loop on both model and obs files
!
!==========================================================


  ! initialize mod variables
  REWIND(NDSM)
  IMOD=1
  ITIMEMOD=1
  FIRSTFILEMOD=.TRUE.
  NEWFILEMOD=.TRUE.
  NEWOPENMOD=.TRUE.

  ! initialize obs variables
  REWIND(NDSL)
  IOBS=1
  ITIMEOBS=1
  FIRSTFILEOBS=.TRUE.
  NEWFILEOBS=.TRUE.
  NEWOPENOBS=.TRUE.
  NEWFILEOBSFROMMOD=.FALSE.

  ! initialize out variables
  startout=1
  itimeout=0

  ! Define refdate for an output
  IF (INDEX(OUTTIMEUNITS,'OBS').NE.0) THEN
    CALL U2D(OBSTIMEUNITS,DATE,IERR)
  ELSE IF (INDEX(OUTTIMEUNITS,'MOD').NE.0) THEN
    CALL U2D(MODTIMEUNITS,DATE,IERR)
  END IF
  CALL D2J(DATE,OUTREFJULDAY,IERR)

  write(NDSO,'(A)') '                 | ---------------------------------------------------------------------------------------------------------- | '
  write(NDSO,'(A)') 'INFOS            | IMOD/NMODFILE |  ITIME/NTIMEMOD   |   IOBS/NOBSFILE   |   ITIME/NTIMEOBS   |  MODJULDAY   |    OBSJULDAY   | '
  write(NDSO,'(A)') '                 | ---------------------------------------------------------------------------------------------------------- | '



  TIME : DO WHILE ( (IMOD.LE.NMODFILE) .AND. (IOBS.LE.NOBSFILE) )
    


!---------------------------------------------------------------
! 9.a Increment obs file to reach start time
!---------------------------------------------------------------

    DO WHILE (STARTJULDAY.GT.OBSJULDAY(IOBS,ITIMEOBS))

      ! if last time step in current file
      IF  (ITIMEOBS.EQ.NTIMEOBS(IOBS)) THEN
        NEWFILEOBS=.TRUE.
        EXIT ! exit do while startjulday
      ! else just increment time
      ELSE
        ITIMEOBS=ITIMEOBS+1
!/T        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated obs time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
!/T              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)     
!/T        WRITE(NDSO,'(2A)') '                 :', &
!/T              ' ------------------------------------------------------------------------------------------------------------ '
      END IF ! ITIMEOBS

      IF (STARTJULDAY.LE.OBSJULDAY(IOBS,ITIMEOBS)) THEN
        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'reached start time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)     
        WRITE(NDSO,'(2A)') '                 :', &
              ' ------------------------------------------------------------------------------------------------------------ '
      END IF
    END DO


!---------------------------------------------------------------
! 9.b Increment model file to reach start time
!---------------------------------------------------------------

    DO WHILE (STARTJULDAY.GT.MODJULDAY(IMOD,ITIMEMOD))

!/T      CALL J2D(MODJULDAY(imod,itimemod)+(timestride/2.0),MODSTARTDATE,IERR)
!/T      WRITE(NDSO,'(A,8I4)')  'model time+timestride/2 : ', MODSTARTDATE(:)
!/T      write(*,*) 'modjulday+timestride : ', (MODJULDAY(IMOD,ITIMEMOD)+(timestride/2.0)) , IMOD, NMODFILE, ITIMEMOD, NTIMEMOD(IMOD)
!/T      CALL J2D(OBSJULDAY(iobs,itimeobs),MODSTARTDATE,IERR)
!/T      WRITE(NDSO,'(A,8I4)')  'obs time : ', MODSTARTDATE(:)
!/T      write(*,*) 'obsjulday            : ', OBSJULDAY(IOBS,ITIMEOBS )

      ! if last time step in current file
      IF  (ITIMEMOD.EQ.NTIMEMOD(IMOD)) THEN
        NEWFILEMOD=.TRUE.
        EXIT ! exit do while modjulday
      ! else just increment time
      ELSE
        ITIMEMOD=ITIMEMOD+1
!/T        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated mod time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
!/T              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)        
!/T        WRITE(NDSO,'(2A)') '                 :', &
!/T              ' ------------------------------------------------------------------------------------------------------------ '
      END IF ! ITIMEMOD
    END DO ! TIMESTRIDE



!---------------------------------------------------------------
! 9.c Increment obs file to the next valid time step
!---------------------------------------------------------------

    IF (.NOT. NEWFILEMOD) THEN
      ! if last time step in current file
      IF  (ITIMEOBS.EQ.NTIMEOBS(IOBS)) THEN
        NEWFILEOBS=.TRUE.
      ! else just increment time
      ELSE
        ! do not increment time obs if new obs file
        ! new obs file is at new index not collocated yet
        IF (.NOT. NEWFILEOBS) THEN
          IF (NEWOPENOBS) THEN
            NEWOPENOBS=.FALSE.
          ELSE
            ! do not increment time obs if new mod file
            ! need to increment new mod file to reach obs file
            IF (.NOT.NEWOPENMOD) THEN
              ITIMEOBS=ITIMEOBS+1
            END IF
          END IF
        END IF
!/T        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated obs time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
!/T              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)     
!/T      WRITE(NDSO,'(2A)') '                 :', &
!/T            ' ------------------------------------------------------------------------------------------------------------ '
      END IF ! ITIMEOBS
    END IF ! NOT NEWFILEMOD

!---------------------------------------------------------------
! 9.d Increment model file to the closest time before the obs file
!---------------------------------------------------------------

    IF (NEWOPENMOD) THEN
      NEWOPENMOD=.FALSE.
    END IF

    ! go to next model time step if time difference is greater than half the model timestride
    DO WHILE ( (MODJULDAY(IMOD,ITIMEMOD)+(timestride/2)) .LT. OBSJULDAY(IOBS,ITIMEOBS ) )

!/T      CALL J2D(MODJULDAY(imod,itimemod)+(timestride/2),MODSTARTDATE,IERR)
!/T      WRITE(NDSO,'(A,8I4)')  'model time+timestride : ', MODSTARTDATE(:)
!/T      write(*,*) 'modjulday+timestride : ', (MODJULDAY(IMOD,ITIMEMOD)+(timestride/2.0)) , IMOD, NMODFILE, ITIMEMOD, NTIMEMOD(IMOD)
!/T      CALL J2D(OBSJULDAY(iobs,itimeobs),MODSTARTDATE,IERR)
!/T      WRITE(NDSO,'(A,8I4)')  'obs time : ', MODSTARTDATE(:)
!/T      write(*,*) 'obsjulday            : ', OBSJULDAY(IOBS,ITIMEOBS )

      ! if last time step in current file
      IF  (ITIMEMOD.EQ.NTIMEMOD(IMOD)) THEN
        NEWFILEMOD=.TRUE.
        EXIT ! exit do while modjulday
      ! else just increment time
      ELSE
        ITIMEMOD=ITIMEMOD+1
!/T        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated mod time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
!/T              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)        
!/T        WRITE(NDSO,'(2A)') '                 :', &
!/T              ' ------------------------------------------------------------------------------------------------------------ '
      END IF ! ITIMEMOD
    END DO ! TIMESTRIDE
    
!---------------------------------------------------------------
! 9.e Increment to last mod file when stop time reached
!---------------------------------------------------------------

    IF (modjulday(imod,itimemod).GT.stopjulday .OR. obsjulday(iobs,itimeobs).GT.stopjulday) THEN
      WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'reached stop time |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
            IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)     
      WRITE(NDSO,'(2A)') '                 :', &
            ' ------------------------------------------------------------------------------------------------------------ '
      NEWFILEMOD=.TRUE.
      IMOD=NMODFILE-(nfilevarmod-1)
    END IF

!---------------------------------------------------------------
! 9.f Allocate arrays
!---------------------------------------------------------------

    IF (.NOT.NEWFILEMOD .AND. .NOT.NEWFILEOBS) THEN

      ! allocate indextimeobs variable array
      IF(.NOT.ALLOCATED(indextimeobs)) WRITE(*,*) 'ALLOCATE OVER ', max(ntimeobs(iobs),ntimemod(imod))
      IF(.NOT.ALLOCATED(indextimeobs)) allocate(indextimeobs(max(ntimeobs(iobs),ntimemod(imod))))
      IF(.NOT.ALLOCATED(indextimemod)) allocate(indextimemod(max(ntimeobs(iobs),ntimemod(imod))))
      IF(.NOT.ALLOCATED(interpvarval)) allocate(interpvarval(max(ntimeobs(iobs),ntimemod(imod)),nvarmod))

    END IF

!---------------------------------------------------------------
! 9.g Filter on time
!---------------------------------------------------------------

    IF (.NOT.NEWFILEMOD .AND. .NOT.NEWFILEOBS .AND. &
        abs(modjulday(imod,itimemod)-obsjulday(iobs,itimeobs)).GT.timestride) THEN
      WRITE(NDSO,'(A)') 'time obs too far from time mod, cycle'
      CALL J2D(MODJULDAY(imod,itimemod),MODSTARTDATE,IERR)
      WRITE(NDSO,'(A,8I4)')  'model time : ', MODSTARTDATE(:)
      CALL J2D(OBSJULDAY(iobs,itimeobs),MODSTARTDATE,IERR)
      WRITE(NDSO,'(A,8I4)')  'obs time : ', MODSTARTDATE(:)
      WRITE(NDSO,'(A,F8.4,A,F8.4,A)') 'model time stride : ', timestride, ' days (', timestride*24, ' hours )'
      IF (ALLTIMEINDEX) THEN
        ! update output count
        itimeout=itimeout+1
        ! save obs time index
        indextimeobs(itimeout)=itimeobs
        ! save mod time index
        indextimemod(itimeout)=itimemod
        DO ivar=1,nvarmod
          interpvarval(itimeout,ivar)=modfillval(ivar)
        END DO
      END IF
      CYCLE TIME
    END IF


!---------------------------------------------------------------
! 9.h Filter on obs flag
!---------------------------------------------------------------

    IF (.NOT.NEWFILEMOD .AND. .NOT.NEWFILEOBS) THEN
      I=1
      DO ivar=nvarobs-nflagobs+1,nvarobs
        SELECT CASE (TRIM(allobsflagcompstr(I)))
          CASE ('.LT.')
!            WRITE(NDSO,'(2A)') 'flag comp', TRIM(allobsflagcompstr(I))
            IF (OBSVARVAL(itimeobs,ivar).LT.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
          CASE ('.LE.')
            IF (OBSVARVAL(itimeobs,ivar).LE.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
          CASE ('.GT.')
            IF (OBSVARVAL(itimeobs,ivar).GT.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
          CASE ('.GE.')
            IF (OBSVARVAL(itimeobs,ivar).GE.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
          CASE ('.EQ.')
            IF (OBSVARVAL(itimeobs,ivar).EQ.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
          CASE ('.NE.')
            IF (OBSVARVAL(itimeobs,ivar).NE.OBSFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allobsflagstr(I)),' not good, cycle'
              CYCLE TIME
            END IF
        END SELECT
        I=I+1
      END DO
    END IF

!---------------------------------------------------------------
! 9.i Filter on space
!---------------------------------------------------------------

    IF (.NOT.NEWFILEMOD .AND. .NOT.NEWFILEOBS) THEN

!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.h.1 Skip out of specified area
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      ! filter on min/max lon/lat
      IF ( (obsvarval(itimeobs,2).LT.lon_min) .OR. (obsvarval(itimeobs,2).GE.lon_max) .OR. &
           (obsvarval(itimeobs,1).LT.lat_min) .OR. (obsvarval(itimeobs,1).GE.lat_max) ) THEN

        WRITE(NDSO,'(A)') 'out of area, cycle'
        WRITE(NDSO,*) obsvarval(itimeobs,2), lon_min, lon_max 
        WRITE(NDSO,*) obsvarval(itimeobs,1), lat_min, lat_max 
        IF (ALLTIMEINDEX) THEN
          ! update output count
          itimeout=itimeout+1
          ! save obs time index
          indextimeobs(itimeout)=itimeobs
          ! save mod time index
          indextimemod(itimeout)=itimemod
          DO ivar=1,nvarmod
            interpvarval(itimeout,ivar)=modfillval(ivar)
          END DO
        END IF
        CYCLE TIME
      END IF


!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.i.2 Check if point within grid and compute interpolation weights
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      ! Initialize the weight
      RW(:)=0

      ! if within regular/curvilinear grid
      IF ((GTYPE.EQ.RLGTYPE).OR.(GTYPE.EQ.CLGTYPE)) THEN
        !INGRID = W3GRMP( GSI, REAL(obsvarval(itimeobs,2)), &
        !                 REAL(obsvarval(itimeobs,1)), II, JJ, W )
        INGRID = W3GRMC( GSI, REAL(obsvarval(itimeobs,2)), &
                         REAL(obsvarval(itimeobs,1)), INTERPSPACE, NS, II, JJ, CS )
        IF ( INGRID ) THEN
          IF (.NOT. ALLOCATED(RW)) ALLOCATE(RW(NS))
          RW(:)=CS(:)
          DEALLOCATE(CS)
        END IF

      ! if within unstructured mesh
      ELSE IF (GTYPE.EQ.UNGTYPE) THEN
        NS=4
        IF(.NOT. ASSOCIATED(II)) ALLOCATE(II(NS),JJ(NS))
        IF(.NOT. ALLOCATED(RW)) ALLOCATE(RW(NS))
        XPOS=REAL(obsvarval(itimeobs,2))
        YPOS=REAL(obsvarval(itimeobs,1))
        CALL IS_IN_UNGRID(modtri, XYB, NX, NTRI, XPOS, YPOS,  & 
                          ITOUT, II, JJ, RW)
        INGRID = (ITOUT.GT.0) 
      END IF ! GTYPE

!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.i.3 Skip out of range points
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      ! if point not inside the grid domain
      IF ( .NOT.INGRID ) THEN
        WRITE(NDSO,*) 'point not in grid domain, cycle'
        IF (ALLTIMEINDEX) THEN
          ! update output count
          itimeout=itimeout+1
          ! save obs time index
          indextimeobs(itimeout)=itimeobs
          ! save mod time index
          indextimemod(itimeout)=itimemod
          DO ivar=1,nvarmod
            interpvarval(itimeout,ivar)=modfillval(ivar)
          END DO
        END IF
        CYCLE TIME
      END IF

      ! if point on land
      LANDPOINTS=0
      DO i=1,NS
        IF ( modmap(II(i),JJ(i)) .EQ. 0 ) LANDPOINTS=LANDPOINTS+1
      END DO
      IF (LANDPOINTS .GT. MAXLANDPOINTS ) THEN
        WRITE(NDSO,*) LANDPOINTS,'/', NS, ' points on land, cycle'
        WRITE(NDSO,*) 'lon / lat : ', REAL(obsvarval(itimeobs,2)),' / ', REAL(obsvarval(itimeobs,1))
        IF (ALLTIMEINDEX) THEN
          ! update output count
          itimeout=itimeout+1
          ! save obs time index
          indextimeobs(itimeout)=itimeobs
          ! save mod time index
          indextimemod(itimeout)=itimemod
          DO ivar=1,nvarmod
            interpvarval(itimeout,ivar)=modfillval(ivar)
          END DO
        END IF
        CYCLE TIME
      END IF

      ! nullify interpolation weight if fillvalue
      DO ivar=1,nvarmod
        DO i=1,NS

          ! if fillvalue
          IF ( modvarval(II(i),JJ(i),itimemod,ivar) .EQ. modfillval(ivar) ) THEN
!/T            WRITE(NDSO,*) 'mod ', trim(allmodvarstr(ivar)), ' fillvalue, weight null'
            RW(i)=0
          END IF
        END DO
      END DO

      ! if sum of weights is null
      IF ( SUM(RW(1:NS)) .EQ. 0 ) THEN
        WRITE(NDSO,'(A)') 'sum of weights is null, cycle'
        IF (ALLTIMEINDEX) THEN
          ! update output count
          itimeout=itimeout+1
          ! save obs time index
          indextimeobs(itimeout)=itimeobs
          ! save mod time index
          indextimemod(itimeout)=itimemod
          DO ivar=1,nvarmod
            interpvarval(itimeout,ivar)=modfillval(ivar)
          END DO
        END IF
        CYCLE TIME
      END IF


!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.i.4 Increment output time if all tests passed
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      ! update output count
      itimeout=itimeout+1


!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.i.5 Compute linear interpolation on model time
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      tdif=obsjulday(iobs,itimeobs)-modjulday(imod,itimemod)
      ! if model time before obs time  
      IF (tdif.GE.0) THEN
        tmod1=itimemod
        tmod2=min(itimemod+1,ntimemod(imod))
      ! if model time after obs time  
      ELSE
        tmod1=max(itimemod-1,1)
        tmod2=itimemod
      END IF
      ratio1=ABS(tdif)/timestride
      ratio2=1-ratio1

!/T    write(*,*) 'modjulday(imod,tmod1) < obsjulday(iobs,itimeobs) < modjulday(imod,tmod2)'
!/T     write(*,*) modjulday(imod,tmod1) ,'<',ratio1,'<', obsjulday(iobs,itimeobs) ,'<',ratio2,'<', modjulday(imod,tmod2)
     

          !JJ
          !^
          !   |   |   |
          !---2---4-------
          !   |  x|   |
          !---1---3-------
          !   |   |   |
          !             ->II


!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.i.6 Compute bi-linear interpolation on model grid
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

      DO ivar=1,nvarmod
        SW=0.
        interpvarval(itimeout,ivar)=0.
        DO i=1,NS
          IF (INTERPTIME) THEN
            IF ( modvarval(II(i),JJ(i),TMOD2,ivar).NE.modfillval(ivar) ) THEN 
              interpvarval(itimeout,ivar) = interpvarval(itimeout,ivar) + RW(i) * modvarval(II(i),JJ(i),tmod1,ivar)*ratio1
              SW = SW + RW(I) * ratio1
              interpvarval(itimeout,ivar) = interpvarval(itimeout,ivar) + RW(i) * modvarval(II(i),JJ(i),tmod2,ivar)*ratio2
              SW = SW + RW(I) * ratio2
            ELSE
              interpvarval(itimeout,ivar) = interpvarval(itimeout,ivar) + RW(i) * modvarval(II(i),JJ(i),itimemod,ivar)
              SW = SW + RW(I)
            END IF
          ELSE
            interpvarval(itimeout,ivar) = interpvarval(itimeout,ivar) + RW(i) * modvarval(II(i),JJ(i),itimemod,ivar)
            SW = SW + RW(I)
          END IF
        END DO
        interpvarval(itimeout,ivar) = interpvarval(itimeout,ivar) / SW
      END DO


    END IF ! .not. newfilemod/newfileobs

!---------------------------------------------------------------
! 9.j Filter on mod flag
!---------------------------------------------------------------

    IF (.NOT.NEWFILEMOD .AND. .NOT.NEWFILEOBS) THEN
      I=1
      DO ivar=nvarmod-nflagmod+1,nvarmod
        SELECT CASE (TRIM(allmodflagcompstr(I)))
          CASE ('.LT.')
            IF (interpvarval(itimeout,ivar).LT.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1
              CYCLE TIME
            END IF
          CASE ('.LE.')
            IF (interpvarval(itimeout,ivar).LE.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1
              CYCLE TIME
            END IF
          CASE ('.GT.')
            IF (interpvarval(itimeout,ivar).GT.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1
              CYCLE TIME
            END IF
          CASE ('.GE.')
            IF (interpvarval(itimeout,ivar).GE.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1
              CYCLE TIME
            END IF
          CASE ('.EQ.')
            IF (interpvarval(itimeout,ivar).EQ.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1

              CYCLE TIME
            END IF
          CASE ('.NE.')
            IF (interpvarval(itimeout,ivar).NE.MODFLAGVAL(I) ) THEN
              WRITE(NDSO,'(3A)') 'flag ',TRIM(allmodflagstr(I)),' not good, cycle'
              ! revert output count
              itimeout=itimeout-1
              CYCLE TIME
            END IF
        END SELECT
        I=I+1
      END DO

!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .
! 9.j.2 Keep valid points
!. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .


      ! save obs time index
      indextimeobs(itimeout)=itimeobs

      ! save mod time index
      indextimemod(itimeout)=itimemod

    END IF

!---------------------------------------------------------------
! 9.k  Write data once a mod or obs file is fully read
!---------------------------------------------------------------

    IF ((ITIMEOUT.GT.0) .AND. (NEWFILEMOD.OR.NEWFILEOBS) ) THEN

      ! allocate out variable array
      IF(.NOT. ALLOCATED(outvarval)) allocate(outvarval(itimeout,nvarout))

      ! store time and index in output array
      WRITE(NDSO,'(/A)') 'Storing time,index in out array...'
      IF (INDEX(OUTTIMEUNITS,'OBS').NE.0) THEN
        IF (index(obstimeunits, "seconds").NE.0)    outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*86400)
        IF (index(obstimeunits, "minutes").NE.0)   outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*1440)
        IF (index(obstimeunits, "hours").NE.0)     outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*24)
        IF (index(obstimeunits, "days").NE.0)  outvarval(:,1)=obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday
      ELSE IF (INDEX(OUTTIMEUNITS,'MOD').NE.0) THEN
        IF (index(modtimeunits, "seconds").NE.0)    outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*86400)
        IF (index(modtimeunits, "minutes").NE.0)   outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*1440)
        IF (index(modtimeunits, "hours").NE.0)     outvarval(:,1)=nint((obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday)*24)
        IF (index(modtimeunits, "days").NE.0)  outvarval(:,1)=obsjulday(iobs,indextimeobs(1:itimeout))-outrefjulday
      END IF
      outvarval(:,2)=indextimeobs(1:itimeout)


      ! store obs variable in output array
      WRITE(NDSO,'(/A)') 'Storing obs in out array...'
      DO ivar=1,nvarobs
        outvarval(:,2+ivar)=obsvarval(indextimeobs(1:itimeout),ivar)
      END DO

      ! store mod variable in output array
      WRITE(NDSO,'(/A)') 'Storing mod in out array...'
      DO ivar=1,nvarmod
        outvarval(:,2+nvarobs+ivar)=interpvarval(1:itimeout,ivar)
      END DO


      ! split output file per mod/obs/not file format
      WRITE(NDSO,*) 'FILESPLIT=',FILESPLIT, ' NEWFILEOBS=',NEWFILEOBS,' NEWFILEMOD=',NEWFILEMOD, 'FIRSTFILEOUT=',FIRSTFILEOUT ,' NEWFILEOBSFROMMOD=',NEWFILEOBSFROMMOD
      IF (FILESPLIT=='OBS') THEN
        IF (NEWFILEMOD) THEN
          WRITE(NDSO,*) 'CREATE NEW OUTPUT WITH NEWFILEOBS : ', NEWFILEOBS      
          BASENAME=OBSFILE(IOBS)
          BASENAME=BASENAME(index(trim(OBSFILE(IOBS)),'/',back=.TRUE.)+1:len(trim(OBSFILE(IOBS))))
          WRITE(outncfile,'(5A)') trim(DIR_OUT),'/', TRIM(OUTPREFIX), TRIM(BASENAME), '_track.nc'
          NEWFILEOUT=.TRUE.
          IF (.NOT. FIRSTFILEOUT) THEN
            NEWFILEOBSFROMMOD=.TRUE.
            NEWOPENOBS=.TRUE.
          END IF
          FIRSTFILEOUT=.FALSE.
        ELSE IF (NEWFILEOBS) THEN
          IF (NEWFILEOBSFROMMOD) THEN
            WRITE(NDSO,*) 'APPEND TO EXISTING OUTPUT FILE'      
            NEWFILEOUT=.FALSE.
            NEWFILEOBSFROMMOD=.FALSE.
          ELSE
            WRITE(NDSO,*) 'CREATE NEW OUTPUT WITH NEWFILEOBS : ', NEWFILEOBS    
            BASENAME=OBSFILE(IOBS)
            BASENAME=BASENAME(index(trim(OBSFILE(IOBS)),'/',back=.TRUE.)+1:len(trim(OBSFILE(IOBS))))
            WRITE(outncfile,'(5A)') trim(DIR_OUT),'/', TRIM(OUTPREFIX), TRIM(BASENAME), '_track.nc'
            NEWFILEOUT=.TRUE.
            NEWFILEOBSFROMMOD=.FALSE.
            FIRSTFILEOUT=.FALSE.
          END IF
        END IF
      ELSE IF (FILESPLIT=='MOD') THEN
        IF (NEWFILEMOD .OR. (NEWFILEOBS .AND. FIRSTFILEOUT)) THEN
          WRITE(NDSO,*) 'CREATE NEW OUTPUT WITH NEWFILEMOD : ', NEWFILEMOD      
          BASENAME=MODFILE(IMOD)
          BASENAME=BASENAME(index(trim(MODFILE(IMOD)),'/',back=.TRUE.)+1:len(trim(MODFILE(IMOD))))
          WRITE(outncfile,'(5A)') trim(DIR_OUT),'/', TRIM(OUTPREFIX), TRIM(BASENAME), '_track.nc'
          NEWFILEOUT=.TRUE.
          FIRSTFILEOUT=.FALSE.
        END IF
      ELSE IF (FILESPLIT=='NOT' .AND. FIRSTFILEOUT) THEN
        WRITE(NDSO,*) 'CREATE NEW OUTPUT WITHOUT SPLITTING'      
        WRITE(outncfile,'(4A)') trim(DIR_OUT),'/', TRIM(OUTPREFIX), 'track.nc'
        NEWFILEOUT=.TRUE.
        FIRSTFILEOUT=.FALSE.
      ELSE ! append to exist output file
        WRITE(NDSO,*) 'APPEND TO EXISTING OUTPUT FILE'      
        NEWFILEOUT=.FALSE.
      END IF


!---------------------------------------------------------------
! 9.l. Create output file
!---------------------------------------------------------------
      IF (NEWFILEOUT) THEN

        ! initialize start index
        startout=1

        ! Check if corresponding track file exists
        WRITE(6,'(/A)') "Defining track file :"
        WRITE(6,'(A)') trim(adjustl(adjustr(outncfile)))
        inquire(file=trim(outncfile), exist=filefound)
        IF(filefound) THEN
          WRITE (cmd, '("/bin/rm ", A)') trim(outncfile)
          CALL system(cmd)
          WRITE(NDSO,'(3A)') '[WARNING] There was already a track file ', TRIM(OUTNCFILE), ' in the folder: it has been deleted.'
        END IF

        ! create output altimeter file 
        write(NDSO,'(/A)') 'Creating track file : '
        WRITE(NDSO,'(A)') trim(outncfile)
        dimln(1)=NF90_UNLIMITED
        IF (INDEX(OUTTIMEUNITS,'OBS').NE.0) THEN
          CALL W3CRNC(trim(outncfile), nctype, ncido, dimln, dimid, varid, OBSTIMEUNITS)
        ELSE IF (INDEX(OUTTIMEUNITS,'MOD').NE.0) THEN
          CALL W3CRNC(trim(outncfile), nctype, ncido, dimln, dimid, varid, MODTIMEUNITS)
        END IF

        ! define the variables in track file (skip time and index)
        write(NDSO,'(/A)') 'Defining the output variables : '
        DO ivar=3,nvarout
          write(NDSO,'(A)') trim(outvarstr(ivar))
          iret=nf90_redef(ncido)
          CALL CHECK_ERROR(IRET,__LINE__)

          ! use outvarstr to define the name of variables in track file
          iret=nf90_def_var(ncido, trim(outvarstr(ivar)), outvartype(ivar), dimid(1), varid(ivar))
          IF (iret.eq.-42) THEN
            WRITE(NDSO,'(3A)') '[INFO] ', trim(outvarstr(ivar)), ' is already defined.'
          ELSE
            CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'units',outunits(ivar))
            CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'long_name',outlongname(ivar))
            CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'standard_name',outstdname(ivar))
            CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'add_offset',outoffset(ivar))
            CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'scale_factor',outscalefac(ivar))
            CALL CHECK_ERROR(IRET,__LINE__)
            if(outvartype(ivar).EQ.NF90_SHORT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',INT(outfillval(ivar),2))
            if(outvartype(ivar).EQ.NF90_INT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',INT(outfillval(ivar)))
            if(outvartype(ivar).EQ.NF90_FLOAT) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',REAL(outfillval(ivar)))
            if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_att(ncido,varid(ivar),'_FillValue',DBLE(outfillval(ivar)))
            CALL CHECK_ERROR(IRET,__LINE__)
      !      iret=nf90_put_att(ncido,varid(ivar),'valid_min',outvalidmin(ivar))
      !      CALL CHECK_ERROR(IRET,__LINE__)
      !      iret=nf90_put_att(ncido,varid(ivar),'valid_max',outvalidmax(ivar))
      !      CALL CHECK_ERROR(IRET,__LINE__)
            iret=nf90_put_att(ncido,varid(ivar),'axis','T')
            CALL CHECK_ERROR(IRET,__LINE__)
          END IF
          iret=nf90_enddef(ncido)
          CALL CHECK_ERROR(IRET,__LINE__)
        END DO

        iret=nf90_close(ncido)
        CALL CHECK_ERROR(IRET,__LINE__)
      END IF ! create new output file



      ! open output track file
      WRITE(NDSO,'(/A)') 'Opening track file : '
      WRITE(NDSO,'(A)') trim(outncfile)
      iret=nf90_open(trim(outncfile),NF90_WRITE,ncido)
      CALL CHECK_ERROR(IRET,__LINE__)

      ! write variable in output file along sat track
      write(NDSO,'(/A)') 'Writing data in track file...'
      DO ivar=1,nvarout
        WRITE(NDSO,'(A)') trim(outvarstr(ivar))
        iret=nf90_inq_varid(ncido, trim(outvarstr(ivar)), varid(ivar))
        CALL CHECK_ERROR(IRET,__LINE__)
        if(outvartype(ivar).EQ.NF90_BYTE)   iret=nf90_put_var(ncido,varid(ivar),NINT(outvarval(:,ivar),1),start=(/startout/),count=(/itimeout/))
        if(outvartype(ivar).EQ.NF90_UBYTE)  iret=nf90_put_var(ncido,varid(ivar),NINT(outvarval(:,ivar),1),start=(/startout/),count=(/itimeout/))
        if(outvartype(ivar).EQ.NF90_SHORT)  iret=nf90_put_var(ncido,varid(ivar),NINT(outvarval(:,ivar),2),start=(/startout/),count=(/itimeout/))
        if(outvartype(ivar).EQ.NF90_INT)    iret=nf90_put_var(ncido,varid(ivar),NINT(outvarval(:,ivar)),start=(/startout/),count=(/itimeout/))
        if(outvartype(ivar).EQ.NF90_FLOAT)  iret=nf90_put_var(ncido,varid(ivar),REAL(outvarval(:,ivar)),start=(/startout/),count=(/itimeout/))
        if(outvartype(ivar).EQ.NF90_DOUBLE) iret=nf90_put_var(ncido,varid(ivar),DBLE(outvarval(:,ivar)),start=(/startout/),count=(/itimeout/))
        CALL CHECK_ERROR(IRET,__LINE__)
      END DO

      ! close output track file
      iret=nf90_close(ncido)
      CALL CHECK_ERROR(IRET,__LINE__)


      ! update output start and count
      startout=startout+itimeout
      itimeout=0

      ! deallocate out variable array
      IF(ALLOCATED(outvarval)) DEALLOCATE(outvarval)

    END IF



!---------------------------------------------------------------
! 9.m Open new mod file if necessary
!---------------------------------------------------------------

    IF (NEWFILEMOD) THEN

      ! Do not increment imod for the first file
      IF (FIRSTFILEMOD) THEN
        FIRSTFILEMOD=.FALSE.
      ELSE IF ((IMOD+nfilevarmod-1).LT.NMODFILE) THEN
        ITIMEMOD=1
        NEWOPENMOD=.TRUE.
        ! increment to next mod file
        IMOD=IMOD+nfilevarmod
        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated mod file |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)        
        WRITE(NDSO,'(2A)') '                 :', &
              ' ------------------------------------------------------------------------------------------------------------ '
      ! Stop time reached or last file reached
      ELSE
        EXIT
      END IF

      ! close previous file
      iret=nf90_close(ncid)
      NEWFILEMOD=.FALSE.

      WRITE(NDSO,'(/A,A)') 'Reading mod file  : ',TRIM(MODFILE(IMOD))
      WRITE(NDSO,'(2A)') '                 :', &
            ' ------------------------------------------------------------------------------------------------------------ '

      ! open next file
      iret = nf90_open(TRIM(MODFILE(IMOD)), NF90_NOWRITE, ncid)

      ! Last mod file reached or will be reached by ivarmodout
      IF(iret.NE.0 .OR. (IMOD+nfilevarmod-1).GT.NMODFILE) THEN
        IMOD=NMODFILE
        WRITE(NDSO,'(A)') 'last mod file    : '
        WRITE(NDSO,'(2A)') '                 :', &
              ' ------------------------------------------------------------------------------------------------------------ '
        EXIT
      END IF

      ! de/allocate model and time variable array
      IF(ALLOCATED(indextimeobs)) DEALLOCATE(indextimeobs)
      IF(ALLOCATED(indextimemod)) DEALLOCATE(indextimemod)
      IF(ALLOCATED(interpvarval)) DEALLOCATE(interpvarval)
      IF(ALLOCATED(modvarval)) DEALLOCATE(modvarval)
      ALLOCATE(modvarval(nlonmod,nlatmod,ntimemod(IMOD),nvarmod))

      ! get model data (hs, wnd, mss, ...)
      WRITE(NDSO,'(/A)') 'Getting model variables : '
      DO ivar=1,nvarmod
        WRITE(NDSO,'(2A)') 'variable : ', trim(allmodvarstr(ivar))

        ! get model variable in the same file
        IF (MODSAMEFILE.EQV..TRUE.) THEN
          iret=nf90_inq_varid(ncid, trim(allmodvarstr(ivar)), varid(ivar))
          CALL CHECK_ERROR(IRET,__LINE__)
          IF ((GTYPE.EQ.RLGTYPE).OR.(GTYPE.EQ.CLGTYPE)) THEN
            iret=nf90_get_var(ncid, varid(ivar), modvarval(:,:,:,ivar))
          ELSE IF (GTYPE.EQ.UNGTYPE) THEN
            iret=nf90_get_var(ncid, varid(ivar), modvarval(:,1,:,ivar))
          END IF ! gtype

        ! get model variable in a file par variable
        ELSE
          DO IVARMODOUT =1,NVARMODOUT
            iret=nf90_inq_varid(ncid, trim(allmodvarstr(ivar)), varid(ivar))
            IF (IRET.NE.0) THEN
              WRITE(NDSO,'(/A,A)') 'Closing ', TRIM(MODFILE(IMOD+IVARMODOUT-1))
              iret=NF90_CLOSE(NCID)
              CALL CHECK_ERROR(IRET,__LINE__)
              WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(MODFILE(IMOD+IVARMODOUT))
              iret = nf90_open(TRIM(MODFILE(IMOD+IVARMODOUT)), NF90_NOWRITE, ncid)
              CALL CHECK_ERROR(IRET,__LINE__)
              CYCLE
            ELSE
              IF ((GTYPE.EQ.RLGTYPE).OR.(GTYPE.EQ.CLGTYPE)) THEN
                iret=nf90_get_var(ncid, varid(ivar), modvarval(:,:,:,ivar))
                CALL CHECK_ERROR(IRET,__LINE__)
              ELSE IF (GTYPE.EQ.UNGTYPE) THEN
                iret=nf90_get_var(ncid, varid(ivar), modvarval(:,1,:,ivar))
                CALL CHECK_ERROR(IRET,__LINE__)
              END IF ! gtype
            END IF ! iret
          END DO ! ivarmodout
        END IF ! modsamefile
      END DO ! ivar    
    END IF ! newfilemod
 


!---------------------------------------------------------------
! 9.n Open new obs file if necessary
!---------------------------------------------------------------

    IF (NEWFILEOBS) THEN

      ! Do not increment iobs for the first file
      IF (FIRSTFILEOBS) THEN
        FIRSTFILEOBS=.FALSE.
      ELSE IF ((IOBS+nfilevarobs-1).LT.NOBSFILE) THEN
        ITIMEOBS=1
        NEWOPENOBS=.TRUE.
        ! increment to next obs file
        IOBS=IOBS+nfilevarobs
        ! go to next obs file if empty
        DO WHILE(NTIMEOBS(IOBS).EQ.0)
          IF ((IOBS+nfilevarobs-1).LT.NOBSFILE) THEN
            IOBS=IOBS+nfilevarobs
          ELSE
            EXIT TIME
          END IF
        END DO
        WRITE(NDSO,'(8(A,I8),2(A,F12.4))') 'updated obs file |M ', IMOD,'/',NMODFILE, ' |M ', ITIMEMOD, '/', NTIMEMOD(IMOD), ' |O  ', &
              IOBS,'/',NOBSFILE, ' |O ', ITIMEOBS, '/', NTIMEOBS(IOBS), ' |M ',MODJULDAY(IMOD,ITIMEMOD), ' |O ',OBSJULDAY(IOBS,ITIMEOBS)   
        WRITE(NDSO,'(2A)') '                 :', &
              ' ------------------------------------------------------------------------------------------------------------ '
      ! Stop time reached or last file reached
      ELSE
        EXIT
      END IF

      ! close previous file
      iret=nf90_close(ncid)
      NEWFILEOBS=.FALSE.

      WRITE(NDSO,'(/A,A)') 'Reading obs file  : ',TRIM(OBSFILE(IOBS))
      WRITE(NDSO,'(2A)') '                 :', &
            ' ------------------------------------------------------------------------------------------------------------ '

      ! open next file
      iret = nf90_open(TRIM(OBSFILE(IOBS)), NF90_NOWRITE, ncid)

      ! Last obs file reached or will be reached by ivarobsout
      IF(iret.NE.0 .OR. (IOBS+nfilevarobs-1).GT.NOBSFILE) THEN
        IOBS=NOBSFILE
        WRITE(NDSO,'(A)') 'last obs file    : '
        WRITE(NDSO,'(2A)') '                 :', &
              ' ------------------------------------------------------------------------------------------------------------ '
        EXIT
      END IF

      ! de/allocate obs and time variable array
      IF(ALLOCATED(indextimeobs)) DEALLOCATE(indextimeobs)
      IF(ALLOCATED(indextimemod)) DEALLOCATE(indextimemod)
      IF(ALLOCATED(interpvarval)) DEALLOCATE(interpvarval)
      IF(ALLOCATED(obsvarval)) DEALLOCATE(obsvarval)
      ALLOCATE(obsvarval(ntimeobs(IOBS),nvarobs))

      ! get obs data (swh, wnd, sigma0, ...)
      write(NDSO,'(/A)') 'Getting obs variables : '
      DO ivar=1,nvarobs
        write(NDSO,'(2A)') 'variable : ', trim(allobsvarstr(ivar))

        ! get obs variable in the same file
        IF (OBSSAMEFILE.EQV..TRUE.) THEN
          iret=nf90_inq_varid(ncid, trim(allobsvarstr(ivar)), varid(ivar))
          CALL CHECK_ERROR(IRET,__LINE__)
          WRITE(*,*) 'ndims : ', obsndims(ivar)
          IF (obsndims(ivar).EQ.0) THEN
            iret=nf90_get_var(ncid, varid(ivar), obsvarval(1,ivar))
            DO i=2,ntimeobs(IOBS)
              obsvarval(i,ivar)=obsvarval(1,ivar)
            END DO
          ELSE IF (obsndims(ivar).EQ.1) THEN
            iret=nf90_get_var(ncid, varid(ivar), obsvarval(:,ivar))
          ELSE IF (obsndims(ivar).EQ.2) THEN
            iret=nf90_get_var(ncid, varid(ivar), obsvarval(:,ivar), count=(/1,ntimeobs(IOBS)/) )
          ELSE
            WRITE(NDSO,'(A)') '[ERROR] number of dimensions not correct for this variable'
            EXIT
          END IF
          CALL CHECK_ERROR(IRET,__LINE__)

        ! get obs variable in a file par variable
        ELSE
          DO IVAROBSOUT =1,NVAROBSOUT
            iret=nf90_inq_varid(ncid, trim(allobsvarstr(ivar)), varid(ivar))
            IF (IRET.NE.0) THEN
              WRITE(NDSO,'(/A,A)') 'Closing ', TRIM(OBSFILE(IOBS+IVAROBSOUT-1))
              iret=NF90_CLOSE(NCID)
              CALL CHECK_ERROR(IRET,__LINE__)
              WRITE(NDSO,'(/A,A)') 'Reading ', TRIM(OBSFILE(IOBS+IVAROBSOUT))
              iret = nf90_open(TRIM(OBSFILE(IOBS+IVAROBSOUT)), NF90_NOWRITE, ncid)
              CALL CHECK_ERROR(IRET,__LINE__)
              CYCLE
            ELSE
              IF (obsndims(ivar).EQ.1) THEN
                iret=nf90_get_var(ncid, varid(ivar), obsvarval(:,ivar))
              ELSE
                iret=nf90_get_var(ncid, varid(ivar), obsvarval(:,ivar), count=(/1,ntimeobs(IOBS)/) )
              END IF
              CALL CHECK_ERROR(IRET,__LINE__)
            END IF ! iret
          END DO ! ivarobsout
        END IF ! obssamefile
      END DO ! ivar  


      ! only unpack coord variables if needed
      IF (obsscalefac(1).NE.1.0) THEN
        obsvarval(:,1)=obsvarval(:,1)*obsscalefac(1)
      END IF 
      IF (obsscalefac(2).NE.1.0) THEN
        obsvarval(:,2)=obsvarval(:,2)*obsscalefac(2)
      END IF 


      ! convert obs longitude from [-180;180] to [0;360]
      IF (.NOT. OBSLON360 .AND. MODLON360) THEN
        DO I=1,ntimeobs(IOBS)
          obsvarval(I,2)=mod(obsvarval(I,2)+360,360.)
        END DO
      END IF

      ! convert obs longitude from [0;360] to [-180;180]
      IF (OBSLON360 .AND. .NOT. MODLON360) THEN
        IF(ALLOCATED(obslontmp)) deallocate(obslontmp)
        allocate(obslontmp(ntimeobs(IOBS)))
        DO I=1,ntimeobs(IOBS)
          IF((obsvarval(I,2).GE.179.98) .AND. (obsvarval(I,2).LT.359.98)) THEN
            obslontmp(I)=obsvarval(I,2)-360.
          ELSE
            obslontmp(I)=obsvarval(I,2)
          END IF
        END DO
        obsvarval(:,2)=obslontmp
        DEALLOCATE(obslontmp)
      END IF

    END IF ! newfileobs



  END DO TIME ! imod .or. iobs




!==========================================================
!
! 10. Finalize
!
!==========================================================


  ! remove file if no data
  IF (startout.EQ.1) THEN
    WRITE(NDSO,'(/A)') '[INFO] No matching data recorded in output file. It will be deleted'
    inquire(file=trim(outncfile), exist=filefound)
    IF(filefound) THEN
      OPEN(30, file=trim(outncfile), form='unformatted')
      CLOSE(30, status='delete')
    END IF
  END IF

  ! Destroy grid-search-utility object
  CALL W3GSUD( GSI )


  GOTO 888
!
! Error escape locations
!
 801 CONTINUE
      WRITE (NDSE,1001)
      CALL EXTCDE ( 61 )
!
 802 CONTINUE
      WRITE (NDSE,1002) IERR
      CALL EXTCDE ( 62 )
!
 809 CONTINUE
      WRITE (NDSE,1009) IERR
      CALL EXTCDE ( 69 )
!

  888 CONTINUE
  WRITE (NDSO,999)

!
! Formats
!
!
  999 FORMAT(//'  End of program '/                                   &
               ' ========================================='/          &
               '         WAVEWATCH III Matching '/)
!
 1001 FORMAT (/' *** WAVEWATCH-III ERROR IN W3MATCH : '/              &
               '     PREMATURE END OF INPUT FILE'/)
!
 1002 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     ERROR IN READING ',A,' FROM INPUT FILE'/         &
               '     IOSTAT =',I5/)
!
 1004 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     PREMATURE END OF TIME ATTRIBUTE '/               &
               '     FROM INPUT DATA FILE'/)
!
 1005 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/               &
               '     ERROR IN READING OF TIME ATTRIBUTE '/            &
               '     FROM INPUT DATA FILE'/                           &
               '     IOSTAT =',I5/)
!
 1009 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/              &
               '     ERROR IN OPENING OBS FILE'/                     &
               '     IOSTAT =',I5/)
!
 1028 FORMAT (/' *** WAVEWATCH III WARNING IN W3MATCH : '/             &
               '     CALENDAR ATTRIBUTE NOT DEFINED'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')
 1029 FORMAT (/' *** WAVEWATCH III ERROR IN W3MATCH : '/             &
               '     CALENDAR ATTRIBUTE NOT MATCH'/                   &
               '     IT MUST RESPECT STANDARD OR GREGORIAN CALENDAR')


END PROGRAM W3MATCH

!/ ------------------------------------------------------------------- /


SUBROUTINE W3CRNC (ncfile, NCTYPE, ncid, dimln, dimid, varid, timeunits)

  USE NETCDF

  implicit none


  CHARACTER, INTENT(IN)             :: ncfile*(*)
  INTEGER, INTENT(IN)               :: nctype, dimln(1)
  CHARACTER, INTENT(IN)             :: timeunits*128
  INTEGER, INTENT(OUT)              :: dimid(1),varid(4),ncid
  INTEGER                           :: iret, deflate=1

  ! Creation in netCDF3 or netCDF4
  IF(NCTYPE.EQ.3)  iret = nf90_create(ncfile, NF90_CLOBBER, ncid)
  IF(NCTYPE.EQ.4)  iret = nf90_create(ncfile, NF90_NETCDF4, ncid)
  CALL CHECK_ERROR(IRET,__LINE__)


  ! Define dimensions
  iret = nf90_def_dim(ncid, 'time', dimln(1), dimid(1))
  CALL CHECK_ERROR(IRET,__LINE__)

  ! Define variables

  !  time
  iret=nf90_def_var(ncid, 'time', NF90_DOUBLE, dimid(1), varid(1))
  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(1), 1, 1, deflate)
  iret=nf90_put_att(ncid,varid(1),'long_name','julian day (UT)')
  iret=nf90_put_att(ncid,varid(1),'standard_name','time')
  iret=nf90_put_att(ncid,varid(1),'units',TRIM(TIMEUNITS))
  iret=nf90_put_att(ncid,varid(1),'calendar','standard') 
  iret=nf90_put_att(ncid,varid(1),'axis','T') 

  !  index
  iret=nf90_def_var(ncid, 'index', NF90_INT, dimid(1), varid(2))
  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(2), 1, 1, deflate)
  iret=nf90_put_att(ncid,varid(2),'units','1')
  iret=nf90_put_att(ncid,varid(2),'long_name','sat time index in original file')
  iret=nf90_put_att(ncid,varid(2),'standard_name','time index from sat')
  iret=nf90_put_att(ncid,varid(2),'_FillValue',NF90_FILL_INT)
  iret=nf90_put_att(ncid,varid(2),'axis','T')

  !  latitude
!  iret=nf90_def_var(ncid, 'latitude', NF90_DOUBLE, dimid(1), varid(2))
!  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(2), 1, 1, deflate)
!  iret=nf90_put_att(ncid,varid(2),'units','degree_north')
!  iret=nf90_put_att(ncid,varid(2),'long_name','latitude')
!  iret=nf90_put_att(ncid,varid(2),'standard_name','latitude')
!  iret=nf90_put_att(ncid,varid(2),'add_offset',0.)
!  iret=nf90_put_att(ncid,varid(2),'scale_factor',1.)
!  iret=nf90_put_att(ncid,varid(2),'_FillValue',NF90_FILL_DOUBLE)
!  iret=nf90_put_att(ncid,varid(2),'valid_min',-90.0)
!  iret=nf90_put_att(ncid,varid(2),'valid_max',90.)
!  iret=nf90_put_att(ncid,varid(2),'axis','T')

  !  longitude
!  iret=nf90_def_var(ncid, 'longitude', NF90_DOUBLE, dimid(1), varid(3))
!  IF (NCTYPE.EQ.4) iret=nf90_def_var_deflate(ncid, varid(3), 1, 1, deflate)
!  iret=nf90_put_att(ncid,varid(3),'units','degree_east')
!  iret=nf90_put_att(ncid,varid(3),'long_name','longitude')
!  iret=nf90_put_att(ncid,varid(3),'standard_name','longitude')
!  iret=nf90_put_att(ncid,varid(3),'add_offset',0.)
!  iret=nf90_put_att(ncid,varid(3),'scale_factor',1.)
!  iret=nf90_put_att(ncid,varid(3),'_FillValue',NF90_FILL_DOUBLE)
!  iret=nf90_put_att(ncid,varid(3),'valid_min',-180.0)
!  iret=nf90_put_att(ncid,varid(3),'valid_max',180.)
!  iret=nf90_put_att(ncid,varid(3),'axis','T')



 
  IRET=NF90_ENDDEF(NCID)
  CALL CHECK_ERROR(IRET,__LINE__)



  RETURN


END SUBROUTINE W3CRNC

!/ ------------------------------------------------------------------- /

  SUBROUTINE CHECK_ERROR(IRET, ILINE)

  USE NETCDF
  USE W3SERVMD, ONLY: EXTCDE

  IMPLICIT NONE

  INTEGER IRET, ILINE, NDSE


  NDSE=6

  IF (IRET .NE. NF90_NOERR) THEN
    WRITE(NDSE,*) ' *** WAVEWATCH III ERROR IN WW3_MATCH :'
    WRITE(NDSE,*) ' LINE NUMBER ', ILINE
    WRITE(NDSE,*) ' NETCDF ERROR MESSAGE: '
    WRITE(NDSE,*) NF90_STRERROR(IRET)
    CALL EXTCDE ( 59 )
  END IF
  RETURN

  END SUBROUTINE CHECK_ERROR

!/ ------------------------------------------------------------------- /
